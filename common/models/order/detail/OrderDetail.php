<?php

namespace common\models\order\detail;

use Yii;
use common\models\order\detail\base\OrderDetailBase;

class OrderDetail extends OrderDetailBase
{

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('yii', 'ID'),
            'status' => Yii::t('yii', '-1: đã xóa, 1: kích hoạt'),
            'inventory_id' => Yii::t('yii', 'Inventory ID'),
            'orders_id' => Yii::t('yii', 'Orders ID'),
            'price' => Yii::t('yii', 'Price'),
            'order_type_id' => Yii::t('yii', 'Order Type ID'),
            'date_in' => Yii::t('yii', 'Date In'),
        ];
    }

    /**
     * @inheritdoc
     * @return OrderDetailQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OrderDetailQuery(get_called_class());
    }
}
