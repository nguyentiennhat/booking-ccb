<?php
/**
 * Created by PhpStorm.
 * User: Team
 * Date: 3/31/2017
 * Time: 1:48 PM
 */
?>
<table id="table_room" class="table table-striped table-bordered nowrap" width="100%">
    <thead>
    <tr>
        <th width="10px">Ngày</th>
        <?php foreach ($prices as $key => $price): ?>
            <th><?= number_format($price) ?></th>
        <?php endforeach ?>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($datas as $key => $data):
        $subDatas = $data; ?>
        <tr>
            <td><?= date('d-m-Y', $data[0]['stay_date']) ?></td>
            <?php foreach ($subDatas as $subData): ?>
                <td><?= $subData['data'] ?></td>
            <?php endforeach ?>
        </tr>
    <?php endforeach ?>
    </tbody>
</table>
<script>
    $(function () {
        $('#table_room').DataTable({
            order: [],
            "iDisplayLength": 10,
            "lengthChange": false,
            paging: true,
            sorting: false,
            ordering: false
        });
    });
</script>