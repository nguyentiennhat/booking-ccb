<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\inventory\models;
use yii\base\Model;
/**
 * Description of Inventories
 *
 * @author theredlab
 */
class Inventories extends Model{
    public $fromDate;
    public $toDate;
    public $price;
    public $quantity;
    public $type;
    public $transaction_code;
    public $name_vi;
    public $name_en;
    
    public function rules() {
        return [
            [['fromDate','toDate','quantity','type'],'required'],
            [['transaction_code','price','name_en','name_vi'],'safe'],
            [['name_en','name_vi'],'string'],
            [['price','quantity','type'],'integer']
        ];
    }
}
