<?php

/* @var $this yii\web\View */

$this->title = 'Cocobay - ' . $name;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use himiklab\yii2\recaptcha\ReCaptcha;

$message = Yii::$app->session->getFlash('message');
?>
<div class="site-index row">
    <div class="col-lg-6 col-lg-offset-3 content">
        <h2><?=Yii::t('yii', 'Register')?> <?= Html::encode($name) ?></h2>
<?php if(isset($message)): ?>
        <p class="message">
            <?= $message ?>
        </p>
        <?php endif;?>
        <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($customer, 'fullname')->textInput(['maxlength' => true]) ?>

            <?= $form->field($customer, 'email')->textInput()->input('email'); ?>
            <?= $form->field($customer, 'phone')->textInput()->input('tel'); ?>
            <?php
            /**$form->field($customer, 'reCaptcha')->widget(
                    ReCaptcha::className(), []
            )->label('')*/
            ?>
            <div class="form-group">
            <?= Html::submitButton(Yii::t('yii', 'Continue'), ['class' => 'btn btn-success']) ?>
            </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
