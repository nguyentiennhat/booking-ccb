<?php

namespace common\models\order\base;

use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property int $id
 * @property int $customer_id
 * @property string $code
 * @property int $created_date
 * @property int $updated_date
 * @property int $updated_by
 * @property double $price
 * @property int $checkin
 * @property int $checkout
 * @property int $fee_payment
 * @property int $status -1: đã xóa, 1: chưa xóa
 * @property int $payment_status 0 Cho thanh toan; 1 Thanh toan thanh cong; 2 Giao dịch treo; 3 Thanh toan that bai ; 4 Thanh toán hết hạn; 5: Thanh toán hết hạn - Đã thanh toán
 * @property string $hash_code
 * @property int $saleable_id
 * @property int $quantity
 */
class OrderBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'created_date', 'checkin', 'checkout', 'fee_payment', 'saleable_id','quantity'], 'required'],
            [['customer_id', 'created_date', 'updated_date', 'updated_by', 'checkin', 'checkout', 'fee_payment', 'status', 'payment_status', 'saleable_id'], 'integer'],
            [['price','quantity'], 'number'],
            [['code'], 'string', 'max' => 50],
            [['hash_code'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Customer ID',
            'code' => 'Code',
            'created_date' => 'Created Date',
            'updated_date' => 'Updated Date',
            'updated_by' => 'Updated By',
            'price' => 'Price',
            'checkin' => 'Checkin',
            'checkout' => 'Checkout',
            'fee_payment' => 'Fee Payment',
            'status' => 'Status',
            'payment_status' => 'Payment Status',
            'hash_code' => 'Hash Code',
            'quantity' => 'Quantity'
        ];
    }
}
