<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;

AppAsset::register($this);
$language    = 'vi';

// get a session variable. The following usages are equivalent:
if (isset(Yii::$app->request->cookies['language']->value)) {
    $language = Yii::$app->request->cookies['language']->value;
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en-us">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
  <title>Cocobus</title>
  <link rel="shortcut icon" href="img/favicon.ico">

  <!-- GOOGLE FONTS-->
  <link href='http://fonts.googleapis.com/css?family=Roboto+Slab' rel='stylesheet' type='text/css'>
  <link href="https://fonts.googleapis.com/css?family=Space+Mono" rel="stylesheet">

  <!-- STYLESHEETS-->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href='css/owl.carousel.css' rel="stylesheet">
  <link href='css/style.css' rel="stylesheet">
  <link href='css/responsive.css' rel="stylesheet"> 
  <link rel="stylesheet" href="css/animate.css">
  <link rel="stylesheet" href="css/feast.css">
  <link rel="stylesheet" href="css/cocobos.css">
  <script src="js/jquery-1.9.1.min.js" type="text/javascript"></script> <script>
  ;(function() {
    'use strict';


    $(activate);


    function activate() {

      $('.nav-tabs').scrollingTabs();

    }
  }());
</script>

    <!--[if lte IE 9]>
      <script src="js/respond.min.js" type="text/javascript"></script>
      <![endif]-->
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    </head>
    <body class="enable-fixed-header">
<?php $this->beginBody() ?>
    <!-- Start Header -->
    <header id="header" class="triangle">

      <!-- Start Banner -->
      <div id="banner">
        <div class="banner-bg">
          <div class="banner-bg-item"><img src="img/cocobay/banner-feast.jpg" alt=""></div>
        </div>

        <!-- Start Header-Inner -->
        <div class="header-inner cleafix">
          <!-- Start Header-Logo -->
          <div class="header-logo">
            <a href="https://www.cocobay.vn">
              <img  style="margin-top: 60px;"  src="img/cocobay/logo-1.png" alt="logo" class="toplogo">
            </a>
          </div>
          <!-- End Header-Logo -->
          <!-- Start Header-Tool-Bar -->
          <div class="header-tool-bar tops">
            <div class="container">
              <div class="row">

                <!-- Start Header-Left -->
                <div class="col-lg-4 col-md-5 col-xs-12 header-left">

                  <!-- Start Header-Contact -->
                  <ul class="custom-list header-contact">
                    <li>
                      <i class="yellow-nav">Hotline</i>
                      0236 3966 988
                    </li>
                    <li>
                      <i  class="yellow-nav" style="padding-left: 25px;">Follow us</i>
                      
                      <ul class="header-social custom-list">
                        <li class="social-mobi"><a href="https://www.facebook.com/CocobayOfficial/" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                        <li class="social-mobi"><a href="https://www.youtube.com/channel/UCxjGy9lqQnHSyd50lVjsFCw" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
                        <li class="social-mobi"><a href="https://www.instagram.com/cocobayofficial/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                      </ul> 
                    </li>
                  </ul>
                  <!-- End Header-Contact -->

                </div>
                <!-- End Header-Left -->

                <!-- Start Header-Right -->
                <div class="col-lg-4 col-md-5 col-sm-12 col-xs-12 col-lg-offset-4 col-md-offset-2 header-right">

                  <ul class="custom-list header-contact">
                    <!-- Start Header-Language -->
                    <li><div class="header-language">
                      <button class="header-btn">
                        <i class="fa fa-globe"></i>
                        EN
                      </button>
                      <nav class="header-form">
                        <ul class="custom-list">
                          <li class="active"><a href="#">VI</a></li>
                        </ul>
                      </nav>
                    </div></li></ul>
                    <!-- End Header-Language -->

                  </div>
                  <!-- End Header-Right -->

                </div>
              </div>
            </div>
            <!-- End Header-tool-bar -->

            <!-- Start Header-Nav -->
            <div class="header-nav">
            </div>
            <!-- End Header-Nav -->

          </div>
          <!-- End Header-Inner -->

          <div class="css-table">
            <div class="css-table-cell cell-search">
            </div>
          </div>

        </div>
        <!-- End Banner -->

      </header>
      <!-- End Header -->

      <!-- Our stay4 -->
      <section class="feast2">
        <div class="container-fluid">
          <div class="row">
            <!-- Start stay4 -->
            <div class="col-sm-12 bg-stay4 ">
              <img src="img/cocobay/ccb-feast2/restaurant.png"/ class="wow fadeInDown" style="padding-top:60px;">
              <h1 class="title wow fadeInDown"><span style="background: -webkit-linear-gradient(#673AB7, #e91e63de);-webkit-background-clip: text;-webkit-text-fill-color: transparent;">COCOBUS</span> <span style="background: -webkit-linear-gradient(#673AB7, #e91e63de);-webkit-background-clip: text;-webkit-text-fill-color: transparent;">BOOKING</span></h1>
              <p class="wow fadeInDown">Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
              <img class="below-p wow fadeInDown" src="img/cocobay/gach-title.PNG"/>
            </div>
          </div>
        </div>
      </section>

      <!-- Start Banner -->

      <div>
       <div id="my-popup" style="margin-top: 0;" class="wow slideInDown" data-wow-duration="2s">
        <div class="row">
         <div class="col-sm-12" style="text-align: center;">
          <h3 class="title-projects">BOOK BUS</h3>
          <p class="sub-titlte-projects">Please fill your information into fields below</p>
          <img class="below-h1" src="img/cocobay/gach-title-white.png"/>
        </div>
        <div class="col-sm-6">
		<?= $content ?>
          <form id="contact-form" class="form" action="#" method="POST" role="form">
           <div class="form-group">
            <label class="form-label" for="name">Your Name</label>
            <input type="text" class="form-control" id="name" name="name" placeholder="John Doe" tabindex="1" required>
          </div>
          <div class="form-group">
          <label class="form-label" for="name">Your Email</label>
          <input type="email" class="form-control" id="email" name="email" placeholder="abc@gmail.com" tabindex="1" required>
         </div>
         <div class="form-group">
            <label class="form-label" for="pick">Local Residence</label>
            <select id="local" onchange="optionres();">
             <option value="no">No</option>
             <option value="yes">Yes</option>
           </select>
           <div id="optionres"></div>
           <script>
            function optionres() {
                if (document.getElementById("local").value == 1){
                document.getElementById("optionres").innerHTML = "* Please show your photo ID when using ticket" ;}
                else {
                document.getElementById("optionres").innerHTML = "" ;
                }
            }
            </script>
        </div>
      </div>
      <div class="col-sm-6 mob">
        <div class="form-group">
         <label class="form-label" for="phone">Your Phone</label>
         <input type="text" class="form-control" id="phone" name="phone" placeholder="0981856588" tabindex="1" required>
       </div>
        <div class="form-group">
       <div class="col-sm-4 col-xs-12 active-input marbtt" style="padding: 0px;">
            <label class="form-label" for="name">Adult</label>
            <input type="number" class="form-control" id="adult" name="adult" placeholder="1" tabindex="1" required>
         </div>
       <div class="col-sm-4 col-xs-12 active-input marbtt" style="padding: 0px;">
            <label class="form-label" for="name">Children</label>
            <input type="number" class="form-control" id="children" name="children" placeholder="1" tabindex="1" required>
         </div>
         <div class="col-sm-4 col-xs-12 active-input marbtt" style="padding: 0px;">
           <label class="form-label mob" for="note">Type of Ticket</label>
           <select id="ticket" style="border-color: #00bcb8!important;width: 100%;">
             <option>24h</option>
             <option>48h</option>
             <option>1 route</option>
           </select>
         </div>
       </div>                                 
       <div class="form-group">
         <label class="form-label" for="note">Note (Optional)</label>
         <input type="text" class="form-control" id="note" name="note" placeholder="" tabindex="2" required>
       </div>                            
                      
       </form>
       </div>
       <div class="col-sm-12 center">
        <button class="btn subcribtn" onclick="var magnificPopup = $.magnificPopup.instance; magnificPopup.close();">SUBMIT</button>
      </div>
    </div>
  </div>
</form>
</div>
</div>
<!-- End Banner -->



  <!-- Start Footer -->
  <footer id="footer">
          <!-- Start Footer-Menu -->
          <div class="footer-copyrights">
            <div class="container">
              <div class="row">
                <div class="col-lg-12">
                  <a href="#" class="link-footer">About Empire Group</a> | <a href="#" class="link-footer">Privacy Policy</a> | <a href="#" class="link-footer">Terms Of Use</a> | <a href="#" class="link-footer">Terms & Conditions</a>
                </div>
              </div>
            </div>
          </div>
          <!-- End Footer-Menu -->

        </footer>
        <!-- End Footer -->

        <a href="#" class="btn" id="back-to-top"><i class="fa fa-arrow-up"></i></a>

        <!-- SCRIPTS-->
        <script src="js/jquery-1.9.1.min.js" type="text/javascript"></script><script src="js/slick.min.js" type="text/javascript" charset="utf-8"></script>

        <script type="text/javascript">
          $(document).ready(function(){$(".customer-logos").slick({slidesToShow:6,slidesToScroll:6,autoplay:!0,autoplaySpeed:5000,arrows:!0,dots:!1,pauseOnHover:!1,responsive:[{breakpoint:768,settings:{slidesToShow:4, slidesToScroll:4,autoplaySpeed:1000}},{breakpoint:520,settings:{slidesToShow:2, slidesToScroll:2,autoplaySpeed:1000}}]})});
        </script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/jquery.vide.min.js"></script>
        <script src="js/jquery-ui-1.10.4.custom.min.js"></script>
        <script src="js/script.js"></script>
        <script src="js/jquery.ba-outside-events.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/jquery.placeholder.js"></script>
        <script type="text/javascript" src="js/jquery.flexisel.js"></script>

        <script type="text/javascript" src="js/scroll.js"></script>
        <script src="js/wow.js"></script>
        <script>
          wow = new WOW(
          {
            animateClass: 'animated',
            offset:       100,
            callback:     function(box) {
              console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
            }
          }
          );
          wow.init();
        </script> <script>$("#open-popup").magnificPopup({type:"inline",callbacks:{open:function(){$.magnificPopup.instance.close=function(){confirm("Are you sure?")&&$.magnificPopup.proto.close.call(this)}}}});</script>
		<script>$("#open-popup1").magnificPopup({type:"inline",callbacks:{open:function(){$.magnificPopup.instance.close=function(){confirm("Are you sure?")&&$.magnificPopup.proto.close.call(this)}}}});</script>
        <script>var nav=$("div.header-tool-bar.tops");$(window).scroll(function(){$(window).scrollTop()>50?nav.removeClass("tops"):nav.addClass("tops")});var nav1=$("img.toplogo");$(window).scroll(function(){$(window).scrollTop()>50?nav1.removeClass("toplogo"):nav1.addClass("toplogo martop")});</script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>