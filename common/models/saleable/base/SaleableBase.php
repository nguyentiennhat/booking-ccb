<?php

namespace common\models\saleable\base;

use Yii;

/**
 * This is the model class for table "saleable".
 *
 * @property int $id
 * @property string $name
 * @property string $name_en
 * @property string $name_vi
 * @property string $note_en
 * @property string $note_vi
 * @property string $term
 * @property string $term_vi
 * @property string $term_en
 * @property string $link
 * @property int $plan_id
 * @property int $status
 * @property int $created_by
 * @property int $created_at
 * @property int $updated_at
 * @property int $transaction_code
 * @property double $price
 * @property double $child_price
 * @property double $local_price
 * @property string $hash_code
 * @property string $code_name
 * @property int $is_dynamic_price
 * @property int $is_dynamic_checkout
 * @property int $is_check_inventory
 * @property int $allow_select_price
 * @property int $is_selectable_inventory
 * @property string $layout
 */
class SaleableBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'saleable';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'name_en', 'name_vi', 'link', 'plan_id', 'status', 'created_by', 'hash_code', 'code_name'], 'required'],
            [['term', 'term_vi', 'term_en', 'note_en', 'note_vi'], 'string'],
            [['plan_id', 'status', 'created_by', 'created_at', 'updated_at', 'transaction_code', 'is_dynamic_price', 'is_dynamic_checkout', 'is_check_inventory', 'allow_select_price', 'is_selectable_inventory'], 'integer'],
            [['price', 'child_price', 'local_price'], 'number'],
            [['name', 'name_en', 'name_vi', 'link', 'hash_code'], 'string', 'max' => 128],
			[['layout'], 'string', 'max' => 1024],
            [['code_name'], 'string', 'max' => 8],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'name_en' => 'Name En',
            'name_vi' => 'Name Vi',
            'term' => 'Term',
            'term_vi' => 'Term Vi',
            'term_en' => 'Term En',
            'link' => 'Link',
            'plan_id' => 'Plan ID',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'transaction_code' => 'Transaction Code',
            'price' => 'Price',
			'child_price' => 'Price for child',
			'local_price' => 'Price for local',
            'hash_code' => 'Hash Code',
            'code_name' => 'Code Name',
            'is_dynamic_price' => 'Is Dynamic Price',
            'is_dynamic_checkout' => 'Is Dynamic Checkout',
            'is_check_inventory' => 'Non Check Inventory',
            'allow_select_price' => 'Allow Select Price',
			'is_selectable_inventory' => 'Is Selectable Inventory Type',
			'layout' => 'Layout'
        ];
    }
}
