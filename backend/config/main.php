<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);
$baseUrl = '/';
return [
    'homeUrl'             => '/admin/',
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'timeZone'            => 'Asia/Ho_Chi_Minh',
    'bootstrap' => ['log'],
    'modules' => [
        'inventory' => [
            'class' => 'backend\modules\inventory\Module',
        ],
        'plan' => [
            'class' => 'backend\modules\plan\Module',
        ],
        'saleable' => [
            'class' => 'backend\modules\saleable\Module',
        ],
        'order' => [
            'class' => 'backend\modules\order\Module',
        ],
        'report' => [
            'class' => 'backend\modules\report\Report',
        ],
    ],
    'components' => [
        'request' => [
            'baseUrl'                => $baseUrl,
            'csrfParam' => '_csrf-backend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'baseUrl'                => $baseUrl,
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
				[
                    'class' => 'yii\web\GroupUrlRule',
                    'rules' => [
                        '/'               => 'site/index',
                        'login'           => 'site/login',
                    ],
                ],
                '<controller>'                                   => '<controller>/index',
                '<controller:[a-z-]+>/<action:[a-z-]+>/<id:\d+>' => '<controller>/<action>',
            ],
        ],
        'formatter'    => [
            'dateFormat'        => 'php:d-m-Y',
            'datetimeFormat'    => 'php:d-m-Y H:i:s',
            'timeFormat'        => 'H:i:s',
            'locale'            => 'vi',
            'defaultTimeZone'   => 'Asia/Ho_Chi_Minh',
            'decimalSeparator'  => '.',
            'thousandSeparator' => ',',
            'currencyCode'      => 'VND',
            'sizeFormatBase'    => 1000,
            'nullDisplay'       => ''
        ],
    ],
    'params' => $params,
    'language' => 'vi',
];
