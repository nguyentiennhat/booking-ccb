<?php

namespace backend\modules\inventory\controllers;

use Yii;
use common\models\inventory\Inventory;
use common\models\inventory\InventorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\modules\inventory\models\Inventories;

/**
 * DefaultController implements the CRUD actions for Inventory model.
 */
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Inventory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InventorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Inventory model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Inventory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Inventory();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Inventory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionBatch()
    {
        $model = new Inventories();

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            $dateFrom = date('Y-m-d', strtotime($model->fromDate));//date($model->fromDate);
            $dateTo = date('Y-m-d', strtotime($model->toDate));//date($model->toDate);

            $periods       = \backend\utilities\helpers\TimeHelper::getDatesFromRange($dateFrom, $dateTo,'m/d/Y');
            foreach ($periods as $date):
                for($i = 0; $i < $model->quantity; $i++){
                    $inventory = new Inventory();
                    $inventory->setAttributes([
                        'status' => Inventory::STATUS_AVAILABLE,
                        'price' => (!empty($model->price))?$model->price:0,
                        'transaction_code' => $model->transaction_code,
                        'inv_type_id' => $model->type,
                        'name_vi' => $model->name_vi,
                        'name_en' => $model->name_en,
                        'inv_type_id' => $model->type,
                        'stay_date' => strtotime($date . ' 00:00:00')
                    ]);
Yii::trace($inventory->stay_date);
                    $inventory->save();
                }
            endforeach;
            return $this->redirect(['index']);
        }

        return $this->render('batch_insert', [
            'model' => $model,
            'items' => \yii\helpers\ArrayHelper::map(\common\models\inventory\type\InventoryType::find()->createCommand()->queryAll(),'id','name')
        ]);
    }

    /**
     * Updates an existing Inventory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Inventory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Inventory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Inventory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Inventory::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('yii', 'The requested page does not exist.'));
    }
}
