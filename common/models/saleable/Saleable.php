<?php

namespace common\models\saleable;

use Yii;
use common\models\saleable\base\SaleableBase;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\SluggableBehavior;
use common\models\plan\Plan;
use common\models\inventory\type\InventoryType;
use yii\helpers\ArrayHelper;
use voskobovich\linker\LinkerBehavior;
use common\models\inventory\Inventory;
use backend\utilities\helpers\TimeHelper;

/**
 * @property Plan plan
 */
class Saleable extends SaleableBase
{
    public function rules()
    {
        $rules = [
                [['inventories'],'required'],
                [['inventory_types'],'safe'],
                [['inventory_type_ids'], 'each', 'rule' => ['integer']]
            ];
        return ArrayHelper::merge(parent::rules(), $rules);
    }
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className()
            ],
            [
                'class' => SluggableBehavior::className(),
                'attribute' => ['name'],
                'slugAttribute' => 'link',
            ],
            [
                'class' => LinkerBehavior::className(),
                'relations' => [
                    'inventory_type_ids' => [
                        'inventory_types',
                        'updater' => ['viaTableAttributesValue' => [
                                'quantity' => function($updater, $relatedPk, $rowCondition) {
                                    /**
                                     * $updater this is a object of current updater that implement UpdaterInterface.
                                     * $relatedPk this is a Primary Key of related object.
                                     * $rowCondition this is a object of current row state, that implement AssociativeRowCondition.
                                     */
                                    /**
                                     * How i can get the Primary Model?
                                     */
                                    $primaryModel = $updater->getBehavior()->owner;
                                    Yii::trace($relatedPk);


                                    /**
                                     * How i can get the Primary Key of Primery Model?
                                     */
                                    return $primaryModel->inventories[$relatedPk]['quantity'];
                                },
                                'period' => function($updater, $relatedPk, $rowCondition) {
                                    /**
                                     * $updater this is a object of current updater that implement UpdaterInterface.
                                     * $relatedPk this is a Primary Key of related object.
                                     * $rowCondition this is a object of current row state, that implement AssociativeRowCondition.
                                     */
                                    /**
                                     * How i can get the Primary Model?
                                     */
                                    $primaryModel = $updater->getBehavior()->owner;

                                    /**
                                     * How i can get the Primary Key of Primery Model?
                                     */
                                    return $primaryModel->inventories[$relatedPk]['period'];
                                },
                            ],
                        ]
                    ],
                ],
            ],            
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('yii', 'ID'),
            'name' => Yii::t('yii', 'Name'),
            'status' => Yii::t('yii', 'Status'),
            'created_by' => Yii::t('yii', 'Created By'),
            'created_at' => Yii::t('yii', 'Created At'),
        ];
    }

    public function getPlan(){
        return $this->hasOne(Plan::className(), ['id' => 'plan_id']);
    }
    
    public function getInventory_types(){
        return $this->hasMany(InventoryType::className(), ['id' => 'inventory_type_id'])->viaTable('saleable_inventory_type', ['saleable_id' => 'id']);
    }
    
    public function getInventoryJungleTable(){
        return Yii::$app->db->createCommand(
                'select * from saleable_inventory_type  WHERE `saleable_id`=' . $this->id)->queryAll();
    }
    public function getInventories(){
        $jungle_table_data = ($this->isNewRecord)?[]: $this->getInventoryJungleTable();
        Yii::trace($jungle_table_data);
        return $jungle_table_data;
    }
    
    public function setInventories($inventories){
        $id_list;
        foreach ($inventories as $key => $inventory){
            $id_list[] = $inventory['id'];
        }
        if(!isset($id_list)){
            $id_list = [];
        }
        $this->setAttributes(['inventory_type_ids' => $id_list]);
        $this->inventories = ArrayHelper::index($inventories, 'id') ;
    }
    
    public function getCheckoutDate($fromDate){
        $checkout_date = date('m/d/Y', 0);
        foreach($this->getInventoryJungleTable() as $inventory_jungle_table){
            $date_to = date('m/d/Y', strtotime('+' . $inventory_jungle_table['period'] . (($inventory_jungle_table['period'] > 1)?' days':' day'), strtotime($fromDate)));
            if($date_to > $checkout_date){
                $checkout_date = $date_to;
            }
        }
        return $checkout_date;
    }
    
    public function getInventoryIds($fromDate, $toDate, $quantity){
        $inventoryIds = [];
        
        foreach($this->getInventoryJungleTable() as $inventory_jungle_table){
            
            $date_to = date('m/d/Y', strtotime('+' . $inventory_jungle_table['period'] . (($inventory_jungle_table['period'] > 1)?' days':' day'), strtotime($fromDate)));
            
            $to_date = ($this->is_dynamic_checkout)?$toDate:$date_to;
            $periods = TimeHelper::getDatesFromRange($fromDate, $to_date, 'm/d/Y');

            $get_quantity = $inventory_jungle_table['quantity'] * $quantity;

            Yii::trace($periods);
            Yii::trace($date_to);
            Yii::trace('+' . $inventory_jungle_table['period'] . (($inventory_jungle_table['period'] > 1)?' days':' day'));
            if (count($periods) > 1) {
                array_splice($periods, 0, 1);
            }
            Yii::trace($periods);
            Yii::trace($fromDate . '-' . $toDate);
            $inventory_quantity = 0;
            foreach ($periods as $date) {
                $inventories = Inventory::find()->distinct()->select('id')->where([
                            'inv_type_id' => $inventory_jungle_table['inventory_type_id'],
                            'status' => Inventory::STATUS_AVAILABLE,
                            'stay_date' => strtotime($date . ' 00:00:00')
                        ])->limit($get_quantity)->orderBy(['price' => SORT_ASC])->createCommand()->queryAll();
                $inventory_quantity = count($inventories);
                if($inventory_quantity < $get_quantity){
                    return [];
                }
                $inventoryIds = ArrayHelper::merge(ArrayHelper::getColumn($inventories,'id'),$inventoryIds);
            }
        }
        
        return $inventoryIds;
    }
    
    /**
     * @inheritdoc
     * @return SaleableQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SaleableQuery(get_called_class());
    }
}
