<?php

namespace backend\assets;

use yii\web\AssetBundle;
use backend\assets\DatatablesAsset;
use backend\assets\Select2Asset;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'js/plugins/toastr/toastr.min.css',
        'js/plugins/datepicker/css/bootstrap-datepicker.css',
        'js/plugins/datetimepicker/bootstrap-datetimepicker.css',
    ];
    public $js = [
        'js/plugins/jquery.blockUI.min.js',
        'js/plugins/jquery.alphanum.min.js',
        'js/plugins/bootbox.min.js',
        'js/plugins/jquery.bt.min.js',
        'js/plugins/toastr/toastr.min.js',
        'js/plugins/numeral/numeral.min.js',
        'js/plugins/datetimepicker/moment.js',
        'js/plugins/datepicker/js/bootstrap-datepicker.min.js',
        'js/plugins/datetimepicker/bootstrap-datetimepicker.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        DatatablesAsset::class,
        Select2Asset::class,
    ];
}
