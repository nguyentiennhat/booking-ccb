<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\saleable\Saleable */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="saleable-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model, 'fromDate')->widget(DatePicker::className(),[
      'pluginOptions' => [
        'format' => 'd-m-yyyy',
         'todayHighlight' => true
      ]
    ]) ?>
    <?= $form->field($model, 'toDate')->widget(DatePicker::className(),[
      'pluginOptions' => [
        'format' => 'd-m-yyyy',
         'todayHighlight' => true
      ]
    ]) ?>
    <?= $form->field($model, 'type')->dropDownList($items) ?>
    <?= $form->field($model, 'name_vi')->textArea() ?>
    <?= $form->field($model, 'name_en')->textArea() ?>
    <?= $form->field($model, 'quantity')->textInput() ?>
    <?= $form->field($model, 'price')->textInput() ?>
    <?= $form->field($model, 'transaction_code')->textInput() ?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('yii', 'Generate'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
