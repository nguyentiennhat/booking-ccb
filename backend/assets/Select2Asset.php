<?php
/**
 * Created by PhpStorm.
 * User: Team
 * Date: 7/16/2016
 * Time: 11:14 AM
 */

namespace backend\assets;

use yii\web\AssetBundle;

class Select2Asset extends AssetBundle
{
    public $sourcePath = '@bower/select2/dist';
    public $css = [
        'css/select2.css'
    ];
    public $js = [
        'js/select2.js',
        'js/i18n/vi.js'
    ];
}