<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\models;

use yii\base\Model;
/**
 * Description of Booking
 *
 * @author theredlab
 */
class Booking extends Model{

	public $name;
	public $email;
	public $phone;
	public $adult;
	public $children;
	public $local;
	public $note;
	public $type;
	public $accept_term;
	public $payment_type;
	
	public function rules() {
		return [
			[['name', 'email', 'phone', 'payment_type'], 'required'],
			[['adult', 'children', 'local', 'note', 'type'], 'safe'],
			['email', 'email'],
			['adult', 'required', 'when' => function($model) {
				return $model->children <= 0 ;
			}, 'whenClient' => "function (attribute, value) {
			    return $('#children').val() <= 0;
			}"],
			['children', 'required', 'when' => function($model) {
				return $model->adult <= 0 ;
			}, 'whenClient' => "function (attribute, value) {
			    return $('#adult').val() <= 0;
			}"],
			[['local', 'accept_term'],'boolean'],
			['accept_term', 'compare', 'compareValue' => true, 'message' => 'You must agree to the terms and conditions'],
			[['adult', 'children'],'default', 'value' => 0],
			[['adult', 'children'],'number', 'min' => 0, 'max' => 100],
			[['name','email','note'],'string', 'max' => 50],
			[['name','email','note'],'filter','filter'=>'trim']
		];
	}
}
