<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\saleable\SaleableSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="saleable-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'name_vi') ?>

    <?= $form->field($model, 'link') ?>

    <?= $form->field($model, 'plan_id') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'transaction_code') ?>

    <?php // echo $form->field($model, 'price') ?>

    <?php // echo $form->field($model, 'hash_code') ?>

    <?php // echo $form->field($model, 'code_name') ?>

    <?php // echo $form->field($model, 'is_dynamic_price') ?>

    <?php // echo $form->field($model, 'is_dynamic_checkout') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('yii', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('yii', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
