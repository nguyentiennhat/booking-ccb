<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\saleable\Saleable */

$this->title = Yii::t('yii', 'Create Saleable');
$this->params['breadcrumbs'][] = ['label' => Yii::t('yii', 'Saleables'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="saleable-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
