<?php

use yii\helpers\Url;

$this->title                   = Yii::t('yii', 'Report');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Báo cáo tồn kho';
\backend\assets\HighChartsAsset::register($this);
/* @var $role backend\models\Role */
$url1 = Url::to(['generate-synthetic-report']);
$url2 = Url::to(['generate-by-date-report']);
$js =<<<EA
$(function () {
        $(".custom-datepicker").datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            orientation: 'bottom left',
            todayHighlight: true,
            startDate: '15-07-2017'
        });
        $("#btn_generate_synthetic").on('click', function () {
            //$.blockUI();
            $.post("$url1", function (result) {
                $("#report_synthetic_section").html(result);
            });
        });
        $("#btn_generate_room_by_price").on('click', function () {
            //$.blockUI();
            let checkAll = $("#chk_check_all").is(':checked') ? 1 : 0;
            $.post("$url2", {dateFrom: $("#txt_date_from").val(), dateTo: $("#txt_date_to").val(), checkAll: checkAll}, function (result) {
                if (result != 'empty') {
                    $("#report_by_price_section").show();
                    Highcharts.chart('report_by_price_chart_section', {
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: 'Báo cáo phòng theo giá tiền'
                        },
                        xAxis: {
                            type: 'category'
                        },
                        legend: {
                            enabled: false
                        },
                        plotOptions: {
                            series: {
                                borderWidth: 0,
                                dataLabels: {
                                    enabled: true,
                                }
                            }
                        },
                        series: [{
                            name: 'Phòng',
                            colorByPoint: true,
                            data: result
                        }],
                    });
                } else {
                    $("body").noti({
                        type: 'warning',
                        content: 'Không có dữ liệu'
                    });
                    $("#report_by_price_section").hide();
                }
            });
        });
    });
EA;
$this->registerJs($js, yii\web\View::POS_READY,'inventory-report');
?>
<div class="">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#synthetic" aria-controls="synthetic" role="tab" data-toggle="tab">Báo cáo tồn kho</a></li>
        <li role="presentation"><a href="#room_by_price" aria-controls="room_by_price" role="tab" data-toggle="tab">Báo cáo tồn kho theo giá tiền</a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="synthetic">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <button class="btn btn-primary" id="btn_generate_synthetic" title="Báo cáo tổng hợp" style="margin-top: 22px">Báo cáo</button>
                    </div>
                </div>
            </div>
            <div id="report_synthetic_section">
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="room_by_price">
            <div class="row" style="margin-top: 10px">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="txt_date_from">Từ ngày</label>
                        <input type="text" class="form-control custom-datepicker" name="date_from" id="txt_date_from" readonly>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="txt_date_to">Đến ngày</label>
                        <input type="text" class="form-control custom-datepicker" name="date_to" id="txt_date_to" readonly>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="radio">
                        <label>
                            <input type="checkbox" id="chk_check_all" value="1"> Tất cả
                        </label>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <button class="btn btn-primary" id="btn_generate_room_by_price" title="Báo cáo tổng hợp" style="margin-top: 22px">Báo cáo</button>
                    </div>
                </div>
            </div>
            <div id="report_by_price_section" style="width: 100%">
                <div id="report_by_price_chart_section"></div>
            </div>
        </div>
    </div>
</div>