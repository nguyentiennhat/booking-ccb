<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\order\Order;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use kartik\date\DatePicker;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel common\models\order\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('yii', 'Orders');
$this->params['breadcrumbs'][] = $this->title;
$url = Url::to(['send-email']);
$js =<<<ET
    $('body').on('click', '.btn-send-mail', function () {
        let id = $(this).data('id');
        let self = $(this);
        self.parents('tr').addClass('selected');
        bootbox.confirm({
            size: 'small',
            message: 'Bạn có muốn gửi mail xác nhận?',
            callback: function (result) {
                if (result) {
                    $.blockUI();
                    $.post("$url", {id: id}, function (result) {
                        if (result == 'success') {
                            body.noti({
                                type: 'success',
                                content: 'Success'
                            })
                        }
                    });
                }
                self.parents('tr').removeClass('selected');
            }
        });
    });
ET;
$this->registerJs($js, yii\web\View::POS_READY);
$dataProvider->setSort([
        'defaultOrder' => [
            'created_date' => SORT_DESC
        ]
    ]);
?>
<div class="order-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?><?php
    Modal::begin([
        'header' => '<h2>Export</h2>',
        'toggleButton' => ['label' => 'Export', 'class' => 'btn btn-success'],
    ]);?>

    
    <?php $form = ActiveForm::begin(['action' => 'default/export']); ?>
    
    <?= $form->field($model, 'fromDate')->widget(DatePicker::className(),[
      'pluginOptions' => [
        'format' => 'd-m-yyyy',
         'todayHighlight' => true
      ]
    ]) ?>
    <?= $form->field($model, 'toDate')->widget(DatePicker::className(),[
      'pluginOptions' => [
        'format' => 'd-m-yyyy',
         'todayHighlight' => true
      ]
    ]) ?>
    <?php echo $form->field($model, 'status')->dropDownList([
            Order::STATUS_DELETED => "Deleted",
            Order::STATUS_PENDING => "Pending",
            Order::STATUS_PAID => "Paid",
            Order::STATUS_EXPIRED => "Expired",
            Order::STATUS_EXPIRED_PAID => "Expired-Paid",
            Order::STATUS_CANCELED => "Cancelled",
    ],['prompt'=>'Payment status']) ?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('yii', 'Export'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <?php Modal::end(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => null,
        'columns' => [
            
            [
                'label' => 'Code',
                'format' => 'html',
                'value' => function ($model) {
                    return Html::a($model->code, Url::to('default/view?id=' . $model->id),['target' => '_blank']);
                }
            ],
            [
                'label' => 'Customer',
                'value' => function ($model) {
                    return $model->customer->fullname;
                }
            ],
            [
                'label' => 'Customer Email',
                'value' => function ($model) {
                    return $model->customer->email;
                }
            ],
            [
                'label' => 'Customer Phone',
                'value' => function ($model) {
                    return $model->customer->phone;
                }
            ],
            'quantity:decimal',
            'price:decimal',
            'checkin:date',
            'checkout:date',
            'created_date:datetime',
            'updated_date:datetime',
            //'updated_by',
            //'fee_payment',
            //'status',
            //'payment_status',
            //'hash_code',
            [
                'label' => 'Status',
                'value' => function ($model) {
                    switch ($model->payment_status){
                        case Order::STATUS_PAID:
                            return 'Paid';
                        case Order::STATUS_CANCELED:
                            return 'Cancelled';
                        case Order::STATUS_DELETED:
                            return 'Deleted';
                        case Order::STATUS_EXPIRED:
                            return 'Expired';
                        case Order::STATUS_EXPIRED_PAID:
                            return 'Expired-Paid';
                        case Order::STATUS_PENDING:
                            return 'Pendding';
                        case Order::STATUS_NOT_PAY:
                            return 'Not Pay';
                        default:
                            return 'Unknow';
                    }
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{myButton}',  // the default buttons + your custom button
                'buttons' => [
                    'myButton' => function($url, $model, $key) {     // render your custom button
                        return Html::a('Send Mail', 'javascript:void(0)', ['class' => 'btn btn-primary btn-send-mail','data-id' => $model->id]);
                    }
                ]
            ]
        ],
    ]); ?>
</div>
