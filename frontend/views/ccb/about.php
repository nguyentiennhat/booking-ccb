<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = Yii::t('yii','Hi ') . $customer->fullname;
?>
<div class="site-about row">
    <div class="col-lg-6 col-lg-offset-3 content">
    <h1><?=Yii::t('yii','Hi ')?><?= Html::encode($customer->fullname) ?></h1>
    <p><?=Yii::t('yii', 'Thank you for booking on our website, we sent a verify link to your email <b>{username}</b> to active your session. The link is valid in 10 mins.',[
        'username' => $customer->email
    ])?></p>
</div>
</div>
