<?php

namespace common\models\plan;

use Yii;
use common\models\plan\base\PlanBase;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;
use common\models\User;

class Plan extends PlanBase
{
    const DELETED_STATUS = -1;
    const INACTIVE_STATUS = 0;
    const ACTIVE_STATUS = 1;
    const RUNNING_STATUS = 2;
    const FINISHED_STATUS = 2;

    public function rules() {
        $rules = [
            [['creator'],'safe']
        ];
        return ArrayHelper::merge(parent::rules(), $rules);
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_date',
                'updatedAtAttribute' => 'modified_date',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => SluggableBehavior::className(),
                'attribute' => ['name'],
                'slugAttribute' => 'link',
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('yii', 'ID'),
            'note' => Yii::t('yii', 'Note'),
            'start_date' => Yii::t('yii', 'Start Date'),
            'end_date' => Yii::t('yii', 'End Date'),
            'created_by' => Yii::t('yii', 'Created By'),
            'created_date' => Yii::t('yii', 'Created Date'),
            'modified_by' => Yii::t('yii', 'Modified By'),
            'modified_date' => Yii::t('yii', 'Modified Date'),
            'status' => Yii::t('yii', 'Status'),
        ];
    }

    public function getCreator(){
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
    
    /**
     * @inheritdoc
     * @return PlanQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PlanQuery(get_called_class());
    }
}
