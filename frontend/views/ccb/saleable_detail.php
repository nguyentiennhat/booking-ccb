<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;
use dosamigos\selectize\SelectizeDropDownList;

$this->title = Yii::t('yii','Booking detail');
$message = Yii::$app->session->getFlash('message');
?>
<div class="site-contact row">
    <div class="col-lg-6 col-lg-offset-3 content">
    <h2><?= Yii::t('yii','Hi ') . $customer_name ?></h2>

    <p>
        <?=Yii::t('yii','Please enter following infomation')?>
    </p>
<?php if(isset($message)): ?>
        <p class="message">
            <?= $message ?>
        </p>
        <?php endif;?>
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'planhashcode')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'saleablehashcode')->hiddenInput()->label(false) ?>
<?php Yii::trace($start_date);Yii::trace($end_date); ?>
    <?= $form->field($model, 'checkindate')->widget(DatePicker::className(),[
      'pluginOptions' => [
        'format' => 'd-m-yyyy',
         'todayHighlight' => true,
            "startDate" => (string)$start_date,
            "endDate" => (string)$end_date,
            'autoclose' => true
      ]
    ])->label(Yii::t('yii', 'Check-in Date')) ?>
        <?php if($is_dynamic_checkout) { ?>
    <?= $form->field($model, 'checkoutdate')->widget(DatePicker::className(),[
      'pluginOptions' => [
        'format' => 'd-m-yyyy',
         'todayHighlight' => true,
            "startDate" => (string)$start_date,
            "endDate" => (string)$end_date,
            'autoclose' => true
      ]
    ])->label(Yii::t('yii', 'Check-out Date')) ?>
        <?php } ?>
    <?= $form->field($model, 'quantity')->widget(SelectizeDropDownList::className(),[
        'items' => $quantity,
    ])->label(Yii::t('yii', 'Quantity'))  ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('yii', 'Continue'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div></div>