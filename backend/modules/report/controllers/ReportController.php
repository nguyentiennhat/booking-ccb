<?php

namespace backend\modules\report\controllers;

use backend\utilities\controller\Controller;
use backend\utilities\helpers\ArrayHelper;
use backend\utilities\helpers\TimeHelper;
use Carbon\Carbon;
use common\models\inventory\Inventory;
use common\models\order\Order;
use Yii;

class ReportController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    //Báo cáo kho
    public function actionInventoryReport()
    {
        return $this->render('inventoryReport');
    }

    public function actionGenerateSyntheticReport()
    {
        $dates  = ArrayHelper::flatten(Inventory::find()->groupBy(['stay_date'])->select('stay_date')->createCommand()->queryAll());
        $prices = ArrayHelper::flatten(Inventory::find()->groupBy(['price'])->select('price')->createCommand()->queryAll());
        $datas  = [];
        foreach ($dates as $date) {
            $tempDatas   = [];
            $rooms       = Inventory::find()->where(['stay_date' => $date, 'status' => 0])->groupBy(['price', 'stay_date'])
                                    ->select('count(*) as total, price, stay_date')->createCommand()->queryAll();
            $roomTotals  = Inventory::find()->where(['stay_date' => $date])
                                    ->groupBy(['price', 'stay_date'])
                                    ->select('count(*) as total, price, stay_date')
                                    ->createCommand()->queryAll();
            $tempPrice   = ArrayHelper::getColumn($rooms, 'price');
            $emptyPrices = array_diff($prices, $tempPrice);
            foreach ($emptyPrices as $emptyPrice) {
                $rooms[] = [
                    'total'     => 0,
                    'price'     => $emptyPrice,
                    'stay_date' => $date
                ];
            }
            ArrayHelper::multisort($rooms, 'price');
            foreach ($roomTotals as $key => $roomTotal) {
                $tempDatas[] = [
                    'data'      => $rooms[$key]['total'] . "/{$roomTotal['total']}",
                    'stay_date' => $date,
                    'price'     => $rooms[$key]['price']
                ];
            }
            if ($roomTotals[0]['stay_date'] == 1504285200 || $roomTotals[0]['stay_date'] == 1504371600) {
                $tempPrices = [999000, 1199000, 1299000, 1399000];
                $lastTwo = array_splice( $tempDatas, 6, 2);
                $lastTwo[0]['price'] = '1499000';
                $lastTwo[1]['price'] = '1699000';
                unset($tempDatas[6], $tempDatas[7]);
                for ($i = 0; $i < 4; $i++ ) {
                    $tempDatas[] = [
                        'data'      => 0,
                        'stay_date' => $date,
                        'price'     => $tempPrices[$i]
                    ];
                }
                $tempDatas = array_merge($tempDatas, $lastTwo);
                ArrayHelper::multisort($tempDatas, 'price');
            }
            $datas[] = $tempDatas;
        }

        return $this->renderAjax('_synthetic', ['datas' => $datas, 'prices' => $prices]);
    }

    public function actionGenerateByPriceReport()
    {
        $datas = [];
        $rooms = Inventory::find()->where(['status' => 0])->groupBy(['price'])->select('count(*) as total, price')->createCommand()->queryAll();
        $date = \yii\helpers\ArrayHelper::index(Inventory::find()->where(['status' => 0])->distinct(['price', 'stay_date'])->select('count(*) as total, price')->createCommand()->queryAll(),'price');

        foreach ($rooms as $room) {
            $datas[] = [
                'name' => $room['price'],
                'y'    => (int)($room['total'] / count($date[$room['price']]) )
            ];
        }

        return $this->asJson($datas);
    }

    public function actionGenerateByDateReport()
    {
        $prices = ArrayHelper::flatten(Inventory::find()->groupBy(['price'])->select('price')->createCommand()->queryAll());
        $dateFrom = Yii::$app->request->post('dateFrom');
        $dateTo   = Yii::$app->request->post('dateTo');
        $checkAll = Yii::$app->request->post('checkAll');

        $datas = [];
        $rooms = Inventory::find()->where(['status' => 0]);

        if ($checkAll == 0) {
            if ($dateTo != '' || $dateFrom != '') {
                if ($dateTo != '' && $dateFrom == '') {
                    $dateFrom = strtotime($dateFrom . '00:00:00');

                    $rooms = $rooms->andFilterWhere(['stay_date' => $dateFrom]);
                } else if ($dateTo == '' && $dateFrom != '') {
                    $dateTo = strtotime($dateTo . '00:00:00');

                    $rooms = $rooms->andFilterWhere(['stay_date' => $dateTo]);
                } else {
                    $dateFrom = strtotime($dateFrom . '00:00:00');
                    $dateTo   = strtotime($dateTo . '00:00:00');

                    $rooms = $rooms->andFilterWhere(['between', 'stay_date', $dateFrom, $dateTo]);
                }
            } else {
                $rooms = $rooms->andFilterWhere(['stay_date' => strtotime(date('d.m.Y') . '00:00:00')]);
            }
        }
        $rooms = $rooms->groupBy(['price'])->select('count(*) as total, price')->createCommand()->queryAll();
        $tempPrice   = ArrayHelper::getColumn($rooms, 'price');
        $emptyPrices = array_diff($prices, $tempPrice);
        foreach ($emptyPrices as $emptyPrice) {
            $datas[] = [
                'name' => $emptyPrice,
                'y'    => 0
            ];
        }
        /** @var Inventory[] $rooms */
        foreach ($rooms as $room) {
            $datas[] = [
                'name' => $room['price'],
                'y'    => (int)$room['total']
            ];
        }

        return empty($datas) ? 'empty' : $this->asJson($datas);
    }

    //Báo cáo doanh thu
    public function actionRevenueReport()
    {
        return $this->render('revenueReport');
    }

    public function actionGenerateRevenueReport()
    {
        $dateFrom   = Yii::$app->request->post('dateFrom');
        $dateTo     = Yii::$app->request->post('dateTo');
        $presetDate = Yii::$app->request->post('presetDate', '');

        $periods = TimeHelper::getDatesFromRange($dateFrom, $dateTo, 'd-m-Y');

        $orders = Order::find()->where(['payment_status' => 1]);
        $datas  = $lineDatas = [];

        if ($dateTo != '' || $dateFrom != '') {
            if ($dateTo == '' && $dateFrom != '') {
                $dateTempFrom = strtotime($dateFrom . '00:00:00');
                $dateTempTo   = strtotime($dateFrom . '23:59:59');

            } else if ($dateTo != '' && $dateFrom == '') {
                $dateTempFrom = strtotime($dateTo . '00:00:00');
                $dateTempTo   = strtotime($dateTo . '23:59:59');

            } else {
                $dateTempFrom = strtotime($dateFrom . '00:00:00');
                $dateTempTo   = strtotime($dateTo . '23:59:59');
            }

            $orders = $orders->joinWith(['payment'])
                             ->andFilterWhere(['>=', 'created_date', $dateTempFrom])
                             ->andFilterWhere(['<=', 'created_date', $dateTempTo])
                    ->andFilterWhere(['vpc_TxnResponseCode' => 100])
                             ->groupBy(['DATE(dr_datetime)'])
                             ->select('sum(price) as price, DATE(dr_datetime) as dr_datetime')
                             ->createCommand()->queryAll();

            if ($dateTo != '' && $dateFrom != '') {
                $orderDates = ArrayHelper::getColumn($orders, 'dr_datetime');
                $orderDates = array_map(function ($date) {
                    return Yii::$app->formatter->asDate($date);
                }, $orderDates);
                $diffDates  = array_diff($periods, $orderDates);
                foreach ($orderDates as $key => $date) {
                    Yii::trace($date);
                    Yii::trace(Yii::$app->formatter->asDate($date));
                    $datas[] = [
                        'name' => Yii::$app->formatter->asDate($date),
                        'y'    => (int)$orders[array_search($date, $orderDates, false)]['price']
                    ];
                }
                foreach ($diffDates as $diffDate) {
                    $datas[] = [
                        'name' => Yii::$app->formatter->asDate($diffDate),
                        'y'    => 0
                    ];
                }
            } else {
                foreach ($orders as $key => $order) {
                    $datas[] = [
                        'name' => Yii::$app->formatter->asDate($order['created_date']),
                        'y'    => (int)$order['price']
                    ];
                }
            }
        } else {
            $monday          = Carbon::now()->startOfWeek();
            $sunday          = Carbon::now()->endOfWeek();
            $firstDayOfMonth = Carbon::now()->startOfMonth();
            $lastDayOfMonth  = Carbon::now()->lastOfMonth();
            if ($presetDate == 1) {
//                $dateFrom = date('d-m-Y', strtotime('-7 day', strtotime(date('d.m.Y'))));
//                $dateFrom = date('d-m-Y', $monday->getTimestamp());
                $dateFrom = $monday->toDateString();
                $dateTo   = $sunday->toDateString();
            } else {
//                $dateFrom = date('d-m-Y', strtotime('-30 day', strtotime(date('d.m.Y'))));
                $dateFrom = $firstDayOfMonth->toDateString();
                $dateTo   = $lastDayOfMonth->toDateString();
            }

            $periods = TimeHelper::getDatesFromRange($dateFrom, date('d-m-Y'), 'd.m.Y');

            $periods = array_map(function ($date) {
                return strtotime($date);
            }, $periods);

            $dateFrom = strtotime($dateFrom . '00:00:00');
            $dateTo   = strtotime($dateTo . '23:59:59');

            $orders = $orders->joinWith(['payment'])
                             ->andFilterWhere(['>=', 'created_date', $dateFrom])
                             ->andFilterWhere(['<=', 'created_date', $dateTo])
                    ->andFilterWhere(['vpc_TxnResponseCode' => 100])
                             ->groupBy(['DATE(dr_datetime)'])
                             ->select('sum(price) as price, DATE(dr_datetime) as dr_datetime, orders.id order_id')
                             ->createCommand()->queryAll();

            $orderDates = ArrayHelper::getColumn($orders, 'dr_datetime');

            $orderDates = array_map(function ($date) {
                return strtotime($date);
            }, $orderDates);
            $diffDates  = array_diff($periods, $orderDates);
            foreach ($orderDates as $key => $date) {
                    Yii::trace($date);
                    Yii::trace(Yii::$app->formatter->asDate($date));
                $datas[] = [
                    'name' => Yii::$app->formatter->asDate($date),
                    'y'    => (int)$orders[array_search($date, $orderDates, false)]['price']
                ];
            }
            foreach ($diffDates as $diffDate) {
                $datas[] = [
                    'name' => Yii::$app->formatter->asDate($diffDate),
                    'y'    => 0
                ];
            }
        }
        ArrayHelper::multisort($datas, 'name');
        ArrayHelper::multisort($lineDatas, 'name');

        $lineDatas = ArrayHelper::getColumn($lineDatas, 'y');

        return empty($datas) ? 'empty' : $this->asJson([
            $datas,
            $lineDatas
        ]);
    }

    public function actionGenerateTransactionReport()
    {
        $dateFrom = Yii::$app->request->post('dateFrom');
        $dateTo   = Yii::$app->request->post('dateTo');

        if ($dateTo == '' && $dateFrom != '') {
            $dateTempFrom = strtotime($dateFrom . '00:00:00');
            $dateTempTo   = strtotime($dateFrom . '23:59:59');

        } else if ($dateTo != '' && $dateFrom == '') {
            $dateTempFrom = strtotime($dateTo . '00:00:00');
            $dateTempTo   = strtotime($dateTo . '23:59:59');
        } else {
            $dateTempFrom = strtotime($dateFrom . '00:00:00');
            $dateTempTo   = strtotime($dateTo . '23:59:59');
        }
        $totalOrder = Order::find()
                             ->where(['payment_status' => [Order::STATUS_PENDING,Order::STATUS_PAID,Order::STATUS_EXPIRED,Order::STATUS_EXPIRED_PAID,Order::STATUS_CANCELED]])
                             ->andFilterWhere(['>=', 'created_date', $dateTempFrom])
                             ->andFilterWhere(['<=', 'created_date', $dateTempTo])->count();
        if ($totalOrder > 0) {
            $totalSuccess = Order::find()
                                  ->where(['payment_status' => Order::STATUS_PAID])
                                  ->andFilterWhere(['>=', 'created_date', $dateTempFrom])
                                  ->andFilterWhere(['<=', 'created_date', $dateTempTo])
                                  ->count();
            $totalSuccess = (isset($totalSuccess)?$totalSuccess:0);
            $totalPending    = Order::find()
                                  ->where(['payment_status' => Order::STATUS_PENDING])
                                  ->andFilterWhere(['>=', 'created_date', $dateTempFrom])
                                  ->andFilterWhere(['<=', 'created_date', $dateTempTo])
                                  ->count();
            $totalPending = (isset($totalPending)?$totalPending:0);
            $totalExpired = Order::find()
                                  ->where(['payment_status' => Order::STATUS_EXPIRED])
                                  ->andFilterWhere(['>=', 'created_date', $dateTempFrom])
                                  ->andFilterWhere(['<=', 'created_date', $dateTempTo])
                                  ->count();
            $totalExpired = (isset($totalExpired)?$totalExpired:0);
            $totalPaidExpired    = Order::find()
                                  ->where(['payment_status' => Order::STATUS_EXPIRED_PAID])
                                  ->andFilterWhere(['>=', 'created_date', $dateTempFrom])
                                  ->andFilterWhere(['<=', 'created_date', $dateTempTo])
                                  ->count();
            $totalPaidExpired = (isset($totalPaidExpired)?$totalPaidExpired:0);
            $totalCancelled    = Order::find()
                                  ->where(['payment_status' => Order::STATUS_CANCELED])
                                  ->andFilterWhere(['>=', 'created_date', $dateTempFrom])
                                  ->andFilterWhere(['<=', 'created_date', $dateTempTo])
                                  ->count();
            $totalCancelled = (isset($totalCancelled)?$totalCancelled:0);
            $datas        = [
                [
                    'name' => 'Giao dịch thành công',
                    'y'    => ($totalSuccess / $totalOrder) * 100
                ],
                [
                    'name' => 'Giao dịch quá hạn',
                    'y'    => ($totalExpired / $totalOrder) * 100
                ],
                [
                    'name' => 'Giao dịch thanh toán quá hạn',
                    'y'    => ($totalPaidExpired / $totalOrder) * 100
                ],
                [
                    'name' => 'Giao dịch chờ thanh toán',
                    'y'    => ($totalPending / $totalOrder) * 100
                ],
                [
                    'name' => 'Giao dịch đã hủy',
                    'y'    => ($totalCancelled / $totalOrder) * 100
                ],
            ];

            return $this->asJson([
                $datas,
                [$totalOrder, $totalSuccess, $totalPending, $totalExpired, $totalPaidExpired, $totalCancelled]
            ]);
        }

        return 'empty';
    }
}
