<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;

//AppAsset::register($this);
$language    = 'vi';

// get a session variable. The following usages are equivalent:
if (isset(Yii::$app->request->cookies['language']->value)) {
    $language = Yii::$app->request->cookies['language']->value;
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en-us">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
  <title>Cocobus</title>
  <link rel="shortcut icon" href="img/favicon.ico">

  <!-- GOOGLE FONTS-->
  <link href='http://fonts.googleapis.com/css?family=Roboto+Slab' rel='stylesheet' type='text/css'>
  <link href="https://fonts.googleapis.com/css?family=Space+Mono" rel="stylesheet">

  <!-- STYLESHEETS-->
  <link href="/styles/cocobus/css/bootstrap.min.css" rel="stylesheet">
  <link href="/styles/cocobus/css/font-awesome.min.css" rel="stylesheet">
  <link href='/styles/cocobus/css/owl.carousel.css' rel="stylesheet">
  <link href='/styles/cocobus/css/style.css' rel="stylesheet">
  <link href='/styles/cocobus/css/responsive.css' rel="stylesheet"> 
  <link rel="stylesheet" href="/styles/cocobus/css/animate.css">
  <link rel="stylesheet" href="/styles/cocobus/css/feast.css">
  <link rel="stylesheet" href="/styles/cocobus/css/cocobos.css">
  <script src="/styles/cocobus/js/jquery-1.9.1.min.js" type="text/javascript"></script> <script>
  ;(function() {
    'use strict';


    $(activate);


    function activate() {

      $('.nav-tabs').scrollingTabs();

    }
  }());
</script>

    <!--[if lte IE 9]>
      <script src="js/respond.min.js" type="text/javascript"></script>
      <![endif]-->
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    </head>
    <body class="enable-fixed-header">
<?php $this->beginBody() ?>
    <!-- Start Header -->
    <header id="header" class="triangle">

      <!-- Start Banner -->
      <div id="banner">
        <div class="banner-bg">
          <div class="banner-bg-item"><img src="/styles/cocobus/img/cocobay/CocoBusBanner.jpg" alt=""></div>
        </div>

        <!-- Start Header-Inner -->
        <div class="header-inner cleafix">
          <!-- Start Header-Logo -->
          <div class="header-logo">
            <a href="https://www.cocobay.vn">
              <img  style="margin-top: 60px;"  src="/styles/cocobus/img/cocobay/logo-1.png" alt="logo" class="toplogo">
            </a>
          </div>
          <!-- End Header-Logo -->
          <!-- Start Header-Tool-Bar -->
          <div class="header-tool-bar tops">
            <div class="container">
              <div class="row">

                <!-- Start Header-Left -->
                <div class="col-lg-4 col-md-5 col-xs-12 header-left">

                  <!-- Start Header-Contact -->
                  <ul class="custom-list header-contact">
                    <li>
                      <i class="yellow-nav">Hotline</i>
                      +84 23 6395 4666
                    </li>
                    <li>
                      <i  class="yellow-nav" style="padding-left: 25px;">Follow us</i>
                      
                      <ul class="header-social custom-list">
                        <li class="social-mobi"><a href="https://www.facebook.com/CocobayOfficial/" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                        <li class="social-mobi"><a href="https://www.youtube.com/channel/UCxjGy9lqQnHSyd50lVjsFCw" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
                        <li class="social-mobi"><a href="https://www.instagram.com/cocobayofficial/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                      </ul> 
                    </li>
                  </ul>
                  <!-- End Header-Contact -->

                </div>
                <!-- End Header-Left -->

                <!-- Start Header-Right -->
                <div class="col-lg-4 col-md-5 col-sm-12 col-xs-12 col-lg-offset-4 col-md-offset-2 header-right">

                  <ul class="custom-list header-contact">
                    <!-- Start Header-Language -->
                    <li><div class="header-language">
                      <button class="header-btn">
                        <i class="fa fa-globe"></i>
                        EN
                      </button>
                      <nav class="header-form">
                        <ul class="custom-list">
                          <li class="active"><a href="#">VI</a></li>
                        </ul>
                      </nav>
                    </div></li></ul>
                    <!-- End Header-Language -->

                  </div>
                  <!-- End Header-Right -->

                </div>
              </div>
            </div>
            <!-- End Header-tool-bar -->

            <!-- Start Header-Nav -->
            <div class="header-nav">
            </div>
            <!-- End Header-Nav -->

          </div>
          <!-- End Header-Inner -->

          <div class="css-table">
            <div class="css-table-cell cell-search">
            </div>
          </div>

        </div>
        <!-- End Banner -->

      </header>
      <!-- End Header -->

      <!-- Our stay4 -->
      <section class="feast2">
        <div class="container-fluid">
          <div class="row">
            <!-- Start stay4 -->
            <div class="col-sm-12 bg-stay4 ">
              <img src="/styles/cocobus/img/cocobay/ccb-feast2/restaurant.png"/ class="wow fadeInDown" style="padding-top:60px;">
              <h1 class="title wow fadeInDown"><span style="background: -webkit-linear-gradient(#673AB7, #e91e63de);-webkit-background-clip: text;-webkit-text-fill-color: transparent;">COCO BUS</span> <span style="background: -webkit-linear-gradient(#673AB7, #e91e63de);-webkit-background-clip: text;-webkit-text-fill-color: transparent;">TOUR</span></h1>
              <img class="below-p wow fadeInDown" src="/styles/cocobus/img/cocobay/gach-title.PNG"/>
            </div>
          </div>
        </div>
      </section>

      <!-- Start Banner -->

      <div>
       <div id="my-popup" style="margin-top: 0;" class="wow slideInDown" data-wow-duration="2s">
        <div class="row">
         <div class="col-sm-12" style="text-align: center;">
          <h3 class="title-projects">BOOK BUS</h3>
          <p class="sub-titlte-projects">Please fill your information into fields below</p>
          <img class="below-h1" src="/styles/cocobus/img/cocobay/gach-title-white.png"/>
        </div>
		<?= $content ?>
    </div>
  </div>
</form>
</div>
</div>
<!-- End Banner -->



  <!-- Start Footer -->
  <footer id="footer">
          <!-- Start Footer-Menu -->
          <div class="footer-copyrights">
            <div class="container">
              <div class="row">
                <div class="col-lg-12">
                  <a href="#" class="link-footer">About Empire Group</a> | <a href="#" class="link-footer">Privacy Policy</a> | <a href="#" class="link-footer">Terms Of Use</a> | <a href="#" class="link-footer">Terms & Conditions</a>
                </div>
              </div>
            </div>
          </div>
          <!-- End Footer-Menu -->

        </footer>
        <!-- End Footer -->

        <a href="#" class="btn" id="back-to-top"><i class="fa fa-arrow-up"></i></a>

        <!-- SCRIPTS-->
        <script src="/styles/cocobus/js/jquery-1.9.1.min.js" type="text/javascript"></script><script src="/styles/cocobus/js/slick.min.js" type="text/javascript" charset="utf-8"></script>
        <script src="/styles/cocobus/js/bootstrap.min.js"></script>
        <script src="/styles/cocobus/js/owl.carousel.min.js"></script>
        <script src="/styles/cocobus/js/jquery.vide.min.js"></script>
        <script src="/styles/cocobus/js/jquery-ui-1.10.4.custom.min.js"></script>
        <script src="/styles/cocobus/js/script.js"></script>
        <script src="/styles/cocobus/js/jquery.ba-outside-events.min.js"></script>
        <script src="/styles/cocobus/js/jquery.magnific-popup.min.js"></script>
        <script src="/styles/cocobus/js/jquery.placeholder.js"></script>
        <script type="text/javascript" src="/styles/cocobus/js/jquery.flexisel.js"></script>

        <script type="text/javascript" src="/styles/cocobus/js/scroll.js"></script>
        <script src="/styles/cocobus/js/wow.js"></script>
        <script>
          wow = new WOW(
          {
            animateClass: 'animated',
            offset:       100,
            callback:     function(box) {
              console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
            }
          }
          );
          wow.init();
        </script> <script>$("#open-popup").magnificPopup({type:"inline",callbacks:{open:function(){$.magnificPopup.instance.close=function(){confirm("Are you sure?")&&$.magnificPopup.proto.close.call(this)}}}});</script>
		<script>$("#open-popup1").magnificPopup({type:"inline",callbacks:{open:function(){$.magnificPopup.instance.close=function(){confirm("Are you sure?")&&$.magnificPopup.proto.close.call(this)}}}});</script>
        <script>var nav=$("div.header-tool-bar.tops");$(window).scroll(function(){$(window).scrollTop()>50?nav.removeClass("tops"):nav.addClass("tops")});var nav1=$("img.toplogo");$(window).scroll(function(){$(window).scrollTop()>50?nav1.removeClass("toplogo"):nav1.addClass("toplogo martop")});</script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>