<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\order\Order;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model common\models\order\OrderSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'code') ?>

    <?php  echo $form->field($model, 'customer_email')->textInput() ?>

    <?php // echo $form->field($model, 'price') ?>

    <?php // echo $form->field($model, 'fee_payment') ?>

    <?php echo $form->field($model, 'payment_status')->dropDownList([
            Order::STATUS_DELETED => "Deleted",
            Order::STATUS_PENDING => "Pending",
            Order::STATUS_PAID => "Paid",
            Order::STATUS_EXPIRED => "Expired",
            Order::STATUS_EXPIRED_PAID => "Expired-Paid",
            Order::STATUS_CANCELED => "Cancelled",
    ],['prompt'=>'Payment status']) ?>

    <?php // echo $form->field($model, 'payment_status') ?>

    <?php // echo $form->field($model, 'hash_code') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('yii', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('yii', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
