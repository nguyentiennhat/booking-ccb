<?php

namespace common\models\inventory\type;

use Yii;
use common\models\inventory\type\base\InventoryTypeBase;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;

class InventoryType extends InventoryTypeBase
{
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => 'update_at',
            ],
        ];
    }    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('yii', 'ID'),
            'name' => Yii::t('yii', 'Name'),
            'note' => Yii::t('yii', 'Note'),
        ];
    }

    /**
     * @inheritdoc
     * @return InventoryTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new InventoryTypeQuery(get_called_class());
    }
}
