<?php
namespace backend\utilities\filters;

use backend\models\User;
use Yii;
use yii\base\ActionFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;

class PermissionAccess extends ActionFilter
{
    public function events()
    {
        return [Controller::EVENT_BEFORE_ACTION => 'beforeAction'];
    }

    public function beforeAction($event)
    {
        /** @var User $user */
        if (Yii::$app->user->isGuest) {
            return parent::beforeAction($event);
        } else {
            if (Yii::$app->permission->can($this->owner->id, $event->id)) {
                return parent::beforeAction($event);
            } else {
                Url::remember(Url::to([$this->owner->id . '/']));
                throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
            }
        }
    }
}