<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\plan\Plan;

/* @var $this yii\web\View */
/* @var $model common\models\plan\Plan */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('yii', 'Plans'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="plan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('yii', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('yii', ($model->status == Plan::RUNNING_STATUS)?'Pause':'Start'), ['schedule', 'id' => $model->id], [
            'class' => ($model->status == Plan::RUNNING_STATUS)?'btn btn-danger':'btn btn-success',
            'data' => [
                'confirm' => Yii::t('yii', 'Are you sure you want to change this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'note:ntext',
            'start_date',
            'end_date',
            'created_by',
            'created_date',
            'modified_by',
            'modified_date',
            'status',
            'code_name',
            'link',
            'hash_code',
        ],
    ]) ?>

    <?= Html::a(Yii::t('yii', 'Delete'), ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
            'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
    ]) ?>
</div>
