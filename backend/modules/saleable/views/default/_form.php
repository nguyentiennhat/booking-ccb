<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use unclead\multipleinput\MultipleInput;
use common\models\inventory\type\InventoryType;
use \yii\helpers\ArrayHelper;
use dosamigos\ckeditor\CKEditor;
use common\models\plan\Plan;

/* @var $this yii\web\View */
/* @var $model common\models\saleable\Saleable */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="saleable-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'name_en')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'name_vi')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'note_vi')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'standard'
    ]) ?>
    <?= $form->field($model, 'note_en')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'standard'
    ]) ?>
    <?= $form->field($model, 'term_vi')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'standard'
    ]) ?>
    <?= $form->field($model, 'term_en')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'standard'
    ]) ?>
    
    <?= $form->field($model, 'status')->dropDownList([0=>'Inactive',1=>'Active']) ?>
    <?= $form->field($model, 'plan_id')->dropDownList(ArrayHelper::map(Plan::find()->createCommand()->queryAll(), 'id', 'name')) ?>
    <?= $form->field($model, 'transaction_code')->textInput() ?>
    <?= $form->field($model, 'price')->textInput() ?>
    <?= $form->field($model, 'child_price')->textInput() ?>
    <?= $form->field($model, 'local_price')->textInput() ?>
    <?= $form->field($model, 'code_name')->textInput() ?>
    <?= $form->field($model, 'allow_select_price')->checkbox() ?>
    <?= $form->field($model, 'is_check_inventory')->checkbox(['label' => 'Non Check Inventory']) ?>
    <?= $form->field($model, 'is_dynamic_checkout')->checkbox() ?>
    <?= $form->field($model, 'is_selectable_inventory')->checkbox() ?>
    <?= $form->field($model, 'layout')->textInput() ?>
<?php
    echo $form->field($model, 'inventories')->widget(MultipleInput::className(), [
        'max'               => 6,
        'min'               => 1, // should be at least 2 rows
        'columns' => [
            [
                'name'  => 'id',
                'type'  => 'dropDownList',
                'title' => 'Type',
                'value' => function($data) {
                    Yii::trace($data);
                    return $data['inventory_type_id'];
                },
                'items' => ArrayHelper::map(InventoryType::find()->createCommand()->queryAll(), 'id', 'name')
            ],
            [
                'name'  => 'quantity',
                'title' => 'Quantity',
                'enableError' => true,
                'value' => function($data) {
                    Yii::trace($data);
                    return $data['quantity'];
                },
                'options' => [
                    'class' => 'input-priority'
                ]
            ],
            [
                'name'  => 'period',
                'title' => 'Period',
                'enableError' => true,
                'value' => function($data) {
                    Yii::trace($data);
                    return $data['period'];
                },
                'options' => [
                    'class' => 'input-priority'
                ]
            ]
        ]
    ])
    ->label(false);
?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('yii', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
