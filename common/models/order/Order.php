<?php

namespace common\models\order;

use Yii;
use common\models\order\base\OrderBase;
use common\models\customer\Customer;
use yii\behaviors\TimestampBehavior;
use common\models\payment\Payment;

/**
 * @property Customer $customer
 * @property Payment $payment
 */
class Order extends OrderBase
{
    const STATUS_DELETED = -1;
    const STATUS_PENDING = 0;
    const STATUS_PAID = 1;
    const STATUS_EXPIRED = 2;
    const STATUS_EXPIRED_PAID = 3;
    const STATUS_CANCELED = 4;
    const STATUS_NOT_PAY = 5;

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_date',
                'updatedAtAttribute' => 'updated_date',
            ],
        ];
    }
        /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('yii', 'ID'),
            'customer_id' => Yii::t('yii', 'Customer ID'),
            'code' => Yii::t('yii', 'Code'),
            'created_date' => Yii::t('yii', 'Created Date'),
            'updated_date' => Yii::t('yii', 'Updated Date'),
            'updated_by' => Yii::t('yii', 'Updated By'),
            'price' => Yii::t('yii', 'Price'),
            'fee_payment' => Yii::t('yii', 'Fee Payment'),
            'status' => Yii::t('yii', 'Status'),
            'payment_status' => Yii::t('yii', 'Payment Status'),
        ];
    }

    public function getCustomer(){
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }
    
    /**
     * @inheritdoc
     * @return OrderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OrderQuery(get_called_class());
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasMany(Payment::className(), ['order_id' => 'id']);
    }

}
