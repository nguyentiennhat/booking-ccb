<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\models;
use yii\base\Model;
/**
 * Description of PaymentTypeForm
 *
 * @author theredlab
 */
class PaymentTypeForm extends Model{
    public $payment_type;
    
    public function rules() {
        return [
            [['payment_type'], 'required'],
            [['payment_type'], 'integer'],
        ];
    }
}
