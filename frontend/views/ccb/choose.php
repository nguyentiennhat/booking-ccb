<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('yii', 'Hi ') . $customer->fullname;

$css =<<<EOT
table.choosen-table{
    display: table;
    width: 100%;
    line-height: 20px;
}
table.choosen-table .choosen-head{}
table.choosen-table .choosen-head tr{}
table.choosen-table .choosen-head tr td{}
.choosen-row{
   border-bottom: 1px solid #ededed
   }
.choosen-col{}
.choosen-body{}  
.choosen-body tr{}  
.choosen-body tr td{} 
.choosen-body tr td .form-group{
    margin-bottom: 0} 
.choosen-body tr td .form-group .radio{
    margin: 0} 
.choosen-body tr td .radio input[type="radio"]{
        margin-left: 0;
        display: inline-block;
        position: relative;
        margin-top: 10px;
}        
EOT;

$this->registerCss($css);

?>
<div class="site-about row">
    <div class="col-lg-6 col-lg-offset-3 content">
        <h2><?= Html::encode($this->title) ?></h2>
        <p><?=$term?></p>
        <h4 class="text-white">
            <?= Yii::t('yii', 'Please choose one of these option below') ?>
        </h4>
        <div class="col-md-12">
            <?php  $form = ActiveForm::begin(); ?>
            
            <table class="choosen-table form-group">
                <thead class="choosen-head">
                    <tr class="choosen-row">
                        <th class="choosen-col"><?=Yii::t('yii', 'Date')?></th>
                        <?php foreach ($template as $price => $count) {
                            
                            if(isset($name[$price]) && !empty($name[$price])){
                                ?><th class="choosen-col"><?= $name[$price] . '<br>(' . number_format($price) ?> VND)</th><?php
                            } else {
                                ?><th class="choosen-col"><?= number_format($price)?> VND</th><?php
                            }
                            
                        }?>
                    </tr>
                </thead>
                <tbody class="choosen-body">
                    <?php foreach($data as $day => $each):?>
                    
                    <tr class="choosen-row">
                        <td class="choosen-col"><?= date('d-m-Y', strtotime('-1 day', $day)) ?></td>
                        <?php foreach($each as $price => $available):?>
                            <td class="choosen-col">
                                <?php if($available){ ?>
                                    <?= $form->field($model, 'date_datas['.$day.']')->radio(['value' => $price, 'uncheck' => null])->label(false) ?>
                                    <?php } else { echo Yii::t('yii', 'Sold Out'); } ?>
                            </td>
                        <?php endforeach; ?>
                            </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="form-group">
                <?= Html::a(Yii::t('yii','Back'), \yii\helpers\Url::to(['book', 'code' => $code, 'event' => $event, 'type' => $type]),['class' => 'btn btn-primary'])?>
                <?= Html::submitButton(Yii::t('yii', 'Confirm'), ['class' => 'btn btn-success']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
