<?php

namespace common\models\saleable;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\saleable\Saleable;

/**
 * SaleableSearch represents the model behind the search form of `common\models\saleable\Saleable`.
 */
class SaleableSearch extends Saleable
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'plan_id', 'status', 'created_by', 'created_at', 'updated_at', 'transaction_code', 'is_dynamic_price', 'is_dynamic_checkout'], 'integer'],
            [['name', 'name_vi', 'link', 'hash_code', 'code_name'], 'safe'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Saleable::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'plan_id' => $this->plan_id,
            'status' => $this->status,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'transaction_code' => $this->transaction_code,
            'price' => $this->price,
            'is_dynamic_price' => $this->is_dynamic_price,
            'is_dynamic_checkout' => $this->is_dynamic_checkout,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'name_vi', $this->name_vi])
            ->andFilterWhere(['like', 'link', $this->link])
            ->andFilterWhere(['like', 'hash_code', $this->hash_code])
            ->andFilterWhere(['like', 'code_name', $this->code_name]);

        return $dataProvider;
    }
}
