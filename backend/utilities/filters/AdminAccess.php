<?php
namespace backend\utilities\filters;

use Yii;
use yii\base\ActionFilter;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;

class AdminAccess extends ActionFilter
{
    public function events()
    {
        return [Controller::EVENT_BEFORE_ACTION => 'beforeAction'];
    }

    public function beforeAction($event)
    {
        if (Yii::$app->user->isGuest) {
            return Yii::$app->getResponse()->redirect(['site/login']);
        } else {
            if (Yii::$app->permission->isAdmin()) {
                return parent::beforeAction($event);
            } else {
                throw new ForbiddenHttpException(Yii::t('yii', 'Method Not Allowed'));
            }
        }
    }
}