<?php

namespace common\models\order\detail\base;

use Yii;

/**
 * This is the model class for table "order_detail".
 *
 * @property int $id
 * @property int $status -1: đã xóa, 1: kích hoạt
 * @property int $inventory_id
 * @property int $orders_id
 * @property int $price
 * @property int $date_in
 * @property int $date_out
 */
class OrderDetailBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_detail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'inventory_id', 'orders_id', 'price', 'date_in', 'date_out'], 'integer'],
            [['inventory_id', 'orders_id', 'date_in'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Status',
            'inventory_id' => 'Inventory ID',
            'orders_id' => 'Orders ID',
            'price' => 'Price',
            'date_in' => 'Date In',
            'date_out' => 'Date Out',
        ];
    }
}
