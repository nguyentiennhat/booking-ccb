<?php

namespace common\models\plan\base;

use Yii;

/**
 * This is the model class for table "plan".
 *
 * @property int $id
 * @property string $name
 * @property string $note
 * @property string $start_date
 * @property string $end_date
 * @property int $created_by
 * @property string $created_date
 * @property int $modified_by
 * @property string $modified_date
 * @property int $status -1:đã xóa, 0: không kích hoạt, 1: kích hoạt
 * @property string $link
 * @property string $hash_code
 * @property int $transaction_code
 * @property string $code_name
 */
class PlanBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'plan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'start_date', 'end_date', 'created_by', 'modified_by', 'link', 'hash_code', 'code_name'], 'required'],
            [['note'], 'string'],
            [['start_date', 'end_date', 'created_date', 'modified_date'], 'safe'],
            [['created_by', 'modified_by', 'status', 'transaction_code'], 'integer'],
            [['name'], 'string', 'max' => 128],
            [['link', 'hash_code'], 'string', 'max' => 256],
            [['code_name'], 'string', 'max' => 8],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'note' => 'Note',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
            'modified_by' => 'Modified By',
            'modified_date' => 'Modified Date',
            'status' => 'Status',
            'link' => 'Link',
            'hash_code' => 'Hash Code',
            'transaction_code' => 'Transaction Code',
            'code_name' => 'Code Name',
        ];
    }
}
