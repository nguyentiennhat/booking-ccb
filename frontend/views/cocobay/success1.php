<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\web\View;
use yii\helpers\Url;

$this->title = Yii::t('yii', 'Hi ') . $customer_name;

$style = "ul.bankList li,ul.cardList li{cursor:pointer;float:left;text-align:center;list-style:none;}input[type=radio]{display:inline!important;margin:5px auto}ul.bankList{clear:both;height:202px;width:636px}ul.bankList li{list-style-position:outside;list-style-type:none;margin-right:0;padding:5px 2px;width:90px}.list-content li{list-style:none;margin:0 0 10px}.list-content li .boxContent{display:'';width:636px;border:1px solid #ccc;padding:10px}.list-content li.active .boxContent{display:block}.list-content li .boxContent ul{height:380px}i.ABB,i.ACB,i.AGB,i.AMREX,i.BAB,i.BIDV,i.BVB,i.DAB,i.EXB,i.GPB,i.HDB,i.ICB,i.JCB,i.LVB,i.MASTE,i.MB,i.MSB,i.NAB,i.NVB,i.OCB,i.OJB,i.PGB,i.SCB,i.SEA,i.SGB,i.SHB,i.TCB,i.TPB,i.VAB,i.VCB,i.VIB,i.VISA,i.VPB{width:80px;height:30px;display:block;background:url(https://www.nganluong.vn/checkout/webskins/skins/checkout/images/bank_logo.png) no-repeat}i.MASTE{background-position:0 -31px}i.AMREX{background-position:0 -62px}i.JCB{background-position:0 -93px}i.VCB{background-position:0 -124px}i.TCB{background-position:0 -155px}i.MB{background-position:0 -186px}i.VIB{background-position:0 -217px}i.ICB{background-position:0 -248px}i.EXB{background-position:0 -279px}i.ACB{background-position:0 -310px}i.HDB{background-position:0 -341px}i.MSB{background-position:0 -372px}i.NVB{background-position:0 -403px}i.DAB{background-position:0 -434px}i.SHB{background-position:0 -465px}i.OJB{background-position:0 -496px}i.SEA{background-position:0 -527px}i.TPB{background-position:0 -558px}i.PGB{background-position:0 -589px}i.BIDV{background-position:0 -620px}i.AGB{background-position:0 -651px}i.SCB{background-position:0 -682px}i.VPB{background-position:0 -713px}i.VAB{background-position:0 -744px}i.GPB{background-position:0 -775px}i.SGB{background-position:0 -806px}i.NAB{background-position:0 -837px}i.BAB{background-position:0 -868px}i.ABB{background-position:0 -958px}i.OCB{background-position:0 -997px}i.BVB{background-position:0 -1049px}i.LVB{background-position:0 -1086px}ul.cardList li{margin-right:0;padding:5px 4px;width:90px}@media (max-width:375px){ul.cardList li{width:80px}}";

$this->registerCss($style);

$this->registerJs(
    "$('body').on('click','#confirm', function() {
        let paymentOption = $('input[name=\"bankcode\"]:checked').val(); 
        $.post('" . Url::to(['/pay/domestic-payment', 'code' => $code, 'event' => $event, 'type' => $type]) . "', 
            {paymentOption: paymentOption}, 
            function(result) {
                if (result.indexOf('http') > -1) {
                    location.href = result;
                } else {
                    console.log(result);
                    alert(result);
                }
            }
        );
    });",
    View::POS_READY,
    'my-button-handler'
);

?>
<div class="site-contactrow">
    <div class="col-lg-6 col-lg-offset-3 content">
    
        <div class="form-group">
<div><label><?= Html::encode($this->title) ?></label></div>
<div><label><?= Yii::t('yii', 'Please complete the payment within 30 minutes') ?></label></div>
     <div>       <label><?= Yii::t('yii', 'Order: ') ?></label> <?= $order->code ?></div>
          <div>  <label><?= Yii::t('yii', 'Adult: ') ?></label> <?= $adult ?> x <?= number_format($adult_price)?></div>
            <div><label><?= Yii::t('yii', 'Children: ') ?></label> <?= $children ?> x <?= number_format($children_price)?></div>
            <div><label><?= Yii::t('yii', 'Price: ') ?></label> <?= number_format($cybersourceParams['amount'] - $order->fee_payment) . ' VND' ?></div>
        </div>
    <?php if(false) { ?>
    <div class="form-group">
        <label><?= Yii::t('yii', 'Transaction fee: ') ?></label> <?= number_format($order->fee_payment) . ' VND' ?>
    </div>
    <?php } ?>
    <?php
    Modal::begin([
        'header' => '<h2>' . Yii::t('yii', 'Select bank') . '</h2>',
        'toggleButton' => [
            'label' => Yii::t('yii', 'Pay'),
                'class' => 'btn btn-success'
            
        ],
        'footer' => Html::submitButton(Yii::t('yii', 'Confirm'),['class' => 'btn btn-success', 'id' => 'confirm'])
    ]);
    ?>
    
    <ul class="cardList clearfix">
        <li class="bank-online-methods ">
            <label for="vcb_ck_on"> <i class="VCB" title="TechComBank – Ngân hàng kỹ thương Việt Nam"></i>
                <input id="vcb_ck_on" type="radio" value="VCB" name="bankcode"> </label>
        </li>
        <li class="bank-online-methods ">
            <label for="tcb_ck_on"> <i class="TCB" title="Ngân hàng Nam Á"></i>
                <input id="tcb_ck_on" type="radio" value="TCB" name="bankcode"> </label>
        </li>
        <li class="bank-online-methods ">
            <label for="bnt_atm_agb_ck_on"> <i class="AGB" title="Ngân hàng Nông nghiệp &amp; Phát triển nông thôn"></i>
                <input id="bnt_atm_agb_ck_on" type="radio" value="AGB" name="bankcode"> </label>
        </li>
        <li class="bank-online-methods ">
            <label for="bidv_ck_on"> <i class="BIDV" title="Ngân hàng TMCP Đầu tư &amp; Phát triển Việt Nam"></i>
                <input id="bidv_ck_on" type="radio" value="BIDV" name="bankcode"> </label>
        </li>
        <li class="bank-online-methods ">
            <label for="sml_atm_scb_ck_on"> <i class="SCB" title="Ngân hàng Sài Gòn Thương tín"></i>
                <input id="sml_atm_scb_ck_on" type="radio" value="SCB" name="bankcode"> </label>
        </li>
        <li class="bank-online-methods ">
            <label for="sml_atm_exb_ck_on"> <i class="EXB" title="Ngân hàng Xuất Nhập Khẩu"></i>
                <input id="sml_atm_exb_ck_on" type="radio" value="EXB" name="bankcode"> </label>
        </li>
        <li class="bank-online-methods ">
            <label for="bnt_atm_pgb_ck_on"> <i class="PGB" title="Ngân hàng Xăng dầu Petrolimex"></i>
                <input id="bnt_atm_pgb_ck_on" type="radio" value="PGB" name="bankcode"> </label>
        </li>
        <li class="bank-online-methods ">
            <label for="bnt_atm_gpb_ck_on"> <i class="GPB" title="Ngân hàng TMCP Dầu khí Toàn Cầu"></i>
                <input id="bnt_atm_gpb_ck_on" type="radio" value="GPB" name="bankcode"> </label>
        </li>
        <li class="bank-online-methods ">
            <label for="nab_ck_on"> <i class="NAB" title="Ngân hàng Nam Á"></i>
                <input id="nab_ck_on" type="radio" value="NAB" name="bankcode"> </label>
        </li>
        <li class="bank-online-methods ">
            <label for="bnt_atm_sgb_ck_on"> <i class="SGB" title="Ngân hàng Sài Gòn Công Thương"></i>
                <input id="bnt_atm_sgb_ck_on" type="radio" value="SGB" name="bankcode"> </label>
        </li>
        <li class="bank-online-methods ">
            <label for="vnbc_ck_on"> <i class="ABB" title="Ngân hàng An Bình"></i>
                <input id="vnbc_ck_on" type="radio" value="ABB" name="bankcode"> </label>
        </li>
        <li class="bank-online-methods ">
            <label for="vib_ck_on"> <i class="VIB" title="VIB - Ngân Hàng Quốc Tế"></i>
                <input id="vib_ck_on" type="radio" value="VIB" name="bankcode"> </label>
        </li>
        <li class="bank-online-methods ">
            <label for="mb_ck_on"> <i class="MB" title="MB - Ngân hàng TMCP Quân Đội"></i>
                <input id="mb_ck_on" type="radio" value="MB" name="bankcode"> </label>
        </li>
        <li class="bank-online-methods ">
            <label for="MSB_ck_on"> <i class="MSB" title="MaritimeBank – Ngân hàng TMCP Hàng Hải Việt Nam"></i>
                <input id="MSB_ck_on" type="radio" value="MSB" name="bankcode"> </label>
        </li>
        <li class="bank-online-methods ">
            <label for="ojb_ck_on"> <i class="OJB" title="Ngân hàng TMCP Đại Dương (OceanBank)"></i>
                <input id="ojb_ck_on" type="radio" value="OJB" name="bankcode"> </label>
        </li>
        <li class="bank-online-methods ">
            <label for="sml_atm_bab_ck_on"> <i class="BAB" title="Ngân hàng Bắc Á"></i>
                <input id="sml_atm_bab_ck_on" type="radio" value="BAB" name="bankcode"> </label>
        </li>
        <li class="bank-online-methods ">
            <label for="icb_ck_on"> <i class="ICB" title="VietinBank - Ngân hàng TMCP Công thương Việt Nam"></i>
                <input id="icb_ck_on" type="radio" value="ICB" name="bankcode"> </label>
        </li>
        <li class="bank-online-methods ">
            <label for="shb_ck_on"> <i class="SHB" title="SHB - Ngân hàng TMCP Sài Gòn - Hà Nội"></i>
                <input id="shb_ck_on" type="radio" value="SHB" name="bankcode"> </label>
        </li>
        <li class="bank-online-methods ">
            <label for="VPB_ck_on"> <i class="VPB" title="VP Bank - Ngân hàng Việt Nam Thịnh Vượng"></i>
                <input id="VPB_ck_on" type="radio" value="VPB" name="bankcode"> </label>
        </li>
        <li class="bank-online-methods ">
            <label for="DAB_ck_on"> <i class="DAB" title="Dong A Bank - Ngân hàng TMCP Đông Á"></i>
                <input id="DAB_ck_on" type="radio" value="DAB" name="bankcode"> </label>
        </li>
        <li class="bank-online-methods ">
            <label for="TPB_ck_on"> <i class="TPB" title="TienphongBank - Ngân hàng Tiên phong"></i>
                <input id="TPB_ck_on" type="radio" value="TPB" name="bankcode"> </label>
        </li>
        <li class="bank-online-methods ">
            <label for="ACB_ck_on"> <i class="ACB" title="ACB - Ngân hàng Á Châu"></i>
                <input id="ACB_ck_on" type="radio" value="ACB" name="bankcode"> </label>
        </li>
        <li class="bank-online-methods ">
            <label for="OCB_ck_on"> <i class="OCB" title="OCB - Ngân hàng Phương Đông"></i>
                <input id="OCB_ck_on" type="radio" value="OCB" name="bankcode"> </label>
        </li>
        <li class="bank-online-methods ">
            <label for="HDB_ck_on"> <i class="HDB" title="HDBank - Ngân hàng TMCP Phát Triển TP Hồ Chí Minh"></i>
                <input id="HDB_ck_on" type="radio" value="HDB" name="bankcode"> </label>
        </li>
        <li class="bank-online-methods ">
            <label for="NVB_ck_on"> <i class="NVB" title="NaviBank - Ngân hàng TMCP Quốc dân"></i>
                <input id="NVB_ck_on" type="radio" value="NVB" name="bankcode"> </label>
        </li>
        <li class="bank-online-methods ">
            <label for="VAB_ck_on"> <i class="VAB" title="VietABank - Ngân hàng Việt Á"></i>
                <input id="VAB_ck_on" type="radio" value="VAB" name="bankcode"> </label>
        </li>
        <li class="bank-online-methods ">
            <label for="LVB_ck_on"> <i class="LVB" title="LienVietPostBank - Ngân hàng Bưu điện Liên Việt"></i>
                <input id="LVB_ck_on" type="radio" value="LVB" name="bankcode"> </label>
        </li>
        <li class="bank-online-methods ">
            <label for="BVB_ck_on"> <i class="BVB" title="BaoVietBank - Ngân hàng Bảo Việt"></i>
                <input id="BVB_ck_on" type="radio" value="BVB" name="bankcode"> </label>
        </li>
    </ul>
    <?php
   
    Modal::end();
    ?>
</div>
</div>