<?php

namespace common\models\plan;

/**
 * This is the ActiveQuery class for [[Plan]].
 *
 * @see Plan
 */
class PlanQuery extends \yii\db\ActiveQuery
{
    public function isRunning()
    {
        return $this->andWhere(['status' => Plan::RUNNING_STATUS]);
    }

    /**
     * @inheritdoc
     * @return Plan[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Plan|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
