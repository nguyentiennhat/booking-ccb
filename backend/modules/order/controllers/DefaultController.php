<?php

namespace backend\modules\order\controllers;

use Yii;
use common\models\order\Order;
use common\models\order\OrderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\utilities\helpers\Mail;
use common\models\saleable\Saleable;
use common\models\customer\Customer;
use moonland\phpexcel\Excel;
use backend\modules\order\models\ExportForm;

/**
 * DefaultController implements the CRUD actions for Order model.
 */
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = new ExportForm();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model
        ]);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionExport() {
        $form = new ExportForm();

        $form->load(Yii::$app->request->post());

        $dateFrom = strtotime(date('Y-m-d', strtotime($form->fromDate)). ' 00:00:00');
        $dateTo = strtotime(date('Y-m-d', strtotime($form->toDate)). ' 24:00:00');
        
        $query = [];
        
        if(isset($form->status) && !empty($form->status)){
            $query['payment_status'] = $form->status;
        }
        
        $datas = Order::find()->where(['between', 'created_date', $dateFrom, $dateTo ])->andFilterWhere($query)->all();

        
        
        $dataExport = [];
        foreach ($datas as $order) {
            //$code = (isset($order->payment) && !empty($order->payment))?$order->payment->vpc_TxnResponseCode:'Not Pay';
            $dataExport[] = [
                'code' => $order->code,
                'customer' => $order->customer->fullname,
                'customer_email' => $order->customer->email,
                'customer_phone' => $order->customer->phone,
                'quantity' => $order->quantity,
                'price' => number_format($order->price),
                'checkin' => date('d-m-Y',$order->checkin),
                'checkout' => date('d-m-Y',$order->checkout),
                //'vpc_TxnResponseCode' => $code
            ];
        }

        Excel::export([
            'models' => $dataExport,
            'columns' => [
                'code',
                'customer',
                'customer_email',
                'customer_phone',
                'quantity',
                'price',
                'checkin',
                'checkout',
                //'vpc_TxnResponseCode'
                ],
            'headers' => [
                'code' => 'Order Code',
                'customer' => 'Customer Name',
                'customer_email' => 'Customer Email',
                'customer_phone' => 'Customer Phone',
                'quantity' => 'Quantity',
                'price' => 'Total Amount',
                'checkin' => 'Check-in',
                'checkout' => 'Checkout',
                //'vpc_TxnResponseCode' => 'Payment Code'
            ],
        ]);
    }

    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Order();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Order model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionSendEmail(){
        
        $orderId = Yii::$app->request->post('id');
        /** @var Orders $order */
        $order = Order::find()->where(['id' => $orderId])->one();

        /** @var Customer $customer */
        $customer     = Customer::find()->where(['id' => $order->customer_id])->one();
        $saleable = Saleable::find()->where(['id' => $order->saleable_id])->one();
        
        $mail = new Mail([
            'subject' => 'Xác nhận thanh toán',
            'mailTo' => $customer->email,
            'content' => ''
        ]);
        $mail->send(['html' => 'booking-confirm-' . Yii::$app->language], [
            'term_condition' => $saleable->term_vi,
            'customer_name' => $customer->fullname,
            'order_code' => $order->code,
            'package_name' => $saleable->name_vi,
            'checkindate' => date('d/m/Y', $order->checkin),
            'checkoutdate' => date('d/m/Y', $order->checkout),
            'amount' => $order->price,
            'quantity' => $order->quantity,
        ]);
        return 'success';
    }
    
    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('yii', 'The requested page does not exist.'));
    }
}
