<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use common\models\customer\Customer;
use common\models\order\Order;
use common\models\payment\Payment;
use common\models\inventory\Inventory;
use common\models\payments\VIBCheckOut;
use common\models\plan\Plan;
use common\models\saleable\Saleable;
use yii\web\NotFoundHttpException;
use yii\helpers\Url;
use common\models\order\detail\OrderDetail;
use frontend\models\PaymentResult;
use backend\utilities\helpers\Mail;

/**
 * Description of PayController
 *
 * @author theredlab
 */
class PayController extends Controller {

    public $enableCsrfValidation = false;

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function init() {
        parent::init();
        $cookies = Yii::$app->request->cookies;
        $language = $cookies->getValue('language', 'vi');
        Yii::$app->language = $language;
    }
    public function actionDomesticPayment() {
        $customer_hash_code = Yii::$app->request->get('code', '');
        $event_link = Yii::$app->request->get('event', '');
        $saleable_link = Yii::$app->request->get('type', '');

        $plan = Plan::find()->where(['link' => $event_link])->isRunning()->one();
        $saleable = Saleable::find()->where(['link' => $saleable_link])->active()->one();
        $customer = Customer::find()->where(['hash_code' => trim($customer_hash_code)])->one();

        if (!(isset($customer) && isset($plan) && isset($saleable) && $saleable->plan->hash_code == $plan->hash_code)) {
            throw new NotFoundHttpException('Please check your link');
        }

        $paymentOption = Yii::$app->request->post('paymentOption');
        $order = Yii::$app->session->get($customer_hash_code . '_order', '');

        $payment = Payment::find()->where(['order_id' => $order->id])->one();

        $payment->updateAttributes([
            'payment_type' => $paymentOption
        ]);

        $vibCheckout = new VIBCheckOut();
        $params['order_id'] = 'BKCCB' . $order->id . $order->id;
        $params['order_code'] = $order->code;
        $params['order_description'] = 'Đơn đặt hàng';
        $params['order_total_amount'] = $order->price + $order->fee_payment;
        $params['order_discount_amount'] = 0;
        $params['order_fee_ship'] = 0;
        $params['order_return_url'] = Url::to('pay/payment-domestic-result?order_id=' . $order->hash_code . '&code=' . $customer_hash_code . '&type=' . $saleable_link,true);
        $params['order_cancel_url'] = Url::to('pay/cancel-payment?order_id=' . $order->hash_code . '&code=' . $customer_hash_code . '&type=' . $saleable_link,true);
        $params['order_time_limit'] = date('d/m/Y,H:i', time() + 7 * 24 * 3600);
        $params['order_payment_option'] = $paymentOption;
        $params['order_payment_fee_for_sender'] = 0;
        $params['sender_fullname'] = $customer->fullname;
        $params['sender_email'] = $customer->email;
        $params['sender_mobile'] = $customer->phone;
        $params['sender_address'] = 'Tp HCM';
        $result = $vibCheckout->sendOrder($params);
        if (is_array($result) && $result['result_code'] == '0') {
            return $result['result_data_decode']['payment_url'];
        }

        return $vibCheckout->getErrorMessage($result['result_code']); //['result_code'] . '---' . $vibCheckout->merchant_id;
    }

    public function actionPaymentDomesticResult() {
        $paymentResult = new PaymentResult();

        $orderId = Yii::$app->request->get('order_id');
        $code = Yii::$app->request->get('code');
        $saleable_link = Yii::$app->request->get('type', '');

        $saleable = Saleable::find()->where(['link' => $saleable_link])->active()->one();
        
        $customer = Customer::find()->where([
            'hash_code' => $code
        ])->one();
        $order = Order::find()->where([
            'hash_code' => $orderId,
            'customer_id' => $customer->id
        ])->one();
        
        if (!(isset($customer) && isset($saleable) && isset($order))) {
            throw new NotFoundHttpException('Please check your link');
        }
        $vibCheckout = new VIBCheckOut();
        $result = $vibCheckout->checkOrder([
            'order_id' => 'BKCCB' . $order->id . $order->id,
            'function' => 'checkOrder'
        ]);
        $billStaus = $result['result_data_decode']['bill_status'];
        $orderCode = $result['result_data_decode']['order_code'];
        $transactionId = $result['result_data_decode']['bill_id'];
        $message = '';
        $transStatus = '';
        if ($result['result_code'] == 0) {
            
            /** @var OrdersDetail $inventoryIds */
            $inventoryIds = OrderDetail::find()->where(['orders_id' => $order->id])->select(['inventory_id'])->createCommand()->queryColumn();
            /** @var Payment $payment */
            $payment = Payment::find()->where(['order_id' => $order->id])->one();
            //Nếu đơn hàng thanh toán thành công
            if ($billStaus == 2) {
                switch ($order->payment_status):
                case Order::STATUS_PENDING:
                    Inventory::updateAll(['status' => Inventory::STATUS_SOLD, 'sold_date' => time()], ['id' => $inventoryIds]);
                    $order->updateAttributes(['payment_status' => Order::STATUS_PAID]);
                    $payment->updateAttributes([
                        'vpc_TxnResponseCode' => $result['result_code'],
                        'vpc_TransactionNo' => $transactionId,
                        'vpc_Message' => 'Đơn hàng thanh toán thành công',
                        'dr_datetime' => time()
                    ]);

                    $mail = new Mail([
                        'subject' => Yii::t('yii','Booking Confirmation'),
                        'mailTo' => $customer->email,
                        'content' => ''
                    ]);
                    $mail->send(['html' => 'booking-confirm-' . Yii::$app->language], [
                        'term_condition' => $saleable->{'term_' . Yii::$app->language},
                        'customer_name' => $customer->fullname,
                        'order_code' => $orderCode,
                        'package_name' => $saleable->{'name_' . Yii::$app->language},
                        'checkindate' => date('d/m/Y', $order->checkin),
                        'checkoutdate' => date('d/m/Y', $order->checkout),
                        'amount' => $order->price,
                        'quantity' => $order->quantity,
                    ]);
                    if (Yii::$app->language == 'vi') {
                        $transStatus = "Đơn hàng của Quý khách <span style='color: red'>{$order->code}</span> đã được thanh toán thành công.";
                    } else {
                        $transStatus = "Your order <span style='color: red'>{$order->code}</span> has been successfully paid.";
                    }
                    break;
                case Order::STATUS_EXPIRED:
                        $order->updateAttributes([
                            'payment_status' => Order::STATUS_EXPIRED_PAID,
                        ]);

                        $payment->updateAttributes([
                            'vpc_TxnResponseCode' => $result['result_code'],
                            'vpc_TransactionNo' => $transactionId,
                            'vpc_Message' => 'Đơn hàng thanh toán quá hạn',
                            'dr_datetime' => time()
                        ]);

                        $amount = number_format($order->price);

                        $mail = new Mail([
                            'content' => '',
                            'subject' => Yii::t('yii','Booking Confirmation'),
                            'mailTo' => $customer->email
                        ]);
                        $mail->send(['html' => 'mail-info'], [
                            'customerName' => $customer->fullname,
                    'saleable_name' => $saleable->name,
                            'message' => "Chúng tôi đã nhận được thanh toán của quý khách cho đơn hàng {$order->code} số tiền {$amount} VND. Tuy nhiên đơn hàng này đã chuyển trạng thái Huỷ/Hết hạn. Chúng tôi sẽ refund lại số tiền trên vào tài khoản quý khách."
                        ]);

                        Inventory::updateAll(['status' => Inventory::STATUS_AVAILABLE, 'sold_date' => null], ['id' => $inventoryIds]);


                        $order->updateAttributes(['payment_status' => 5]);
                        if (Yii::$app->language == 'vi') {
                            $transStatus = "Đơn hàng đã bị hủy do quá hạn thanh toán. Chúng tôi sẽ hoàn lại số tiền của quý khách cho đơn hàng này.";
                        } else {
                            $transStatus = "Your order has been canceled. We will refund the money for you later.";
                        }
                    break;
                    default :
                endswitch;
            } else {
                if ($result['result_code'] == 3) {
                    $message = 'Có lỗi khi thanh toán ở hệ thống ngân hàng thanh toán';
                }
                if ($result['result_code'] == 4) {
                    $message = 'Đơn hàng hết hạn thanh toán';
                }
                if($order->payment_status == Order::STATUS_PENDING){
                    Inventory::updateAll(['inventory_status' => Inventory::STATUS_AVAILABLE], ['id' => $inventoryIds]);
                    //Nếu thanh toán không thành công và don hàng dang la cho thanh toán => status fail
                    $order->updateAttributes(['payment_status' => Order::STATUS_NOT_PAY]);
                    $payment->updateAttributes([
                        'vpc_TxnResponseCode' => $result['result_code'],
                        'vpc_TransactionNo' => $transactionId,
                        'vpc_Message' => 'Đơn hàng thanh toán thất bại',
                        'dr_datetime' => time()
                    ]);
                    $mail = new Mail([
                        'content' => '',
                        'subject' => Yii::t('yii','Booking Confirmation'),
                        'mailTo' => $customer->email
                    ]);
                    $mail->send(['html' => 'mail-info'], [
                        'customerName' => $customer->fullname,
                        'saleable_name' => $saleable->name,
                        'message' => 'Đơn hàng ' . $order->code . ' thanh toán thất bại. Vui lòng đặt lại hoặc liên hệ để được hướng dẫn'
                    ]);
                    $transStatus = 'Đơn hàng ' . $order->code . ' thanh toán thất bại. Vui lòng đặt lại hoặc liên hệ để được hướng dẫn';
                }
            }
        } 
        $paymentResult->transStatus = $transStatus;
        $paymentResult->transactionNo = $transactionId;
        $paymentResult->amount = $order->price;
        $paymentResult->orderInfo = $orderCode;
        $paymentResult->message = $message;
        $paymentResult->paymentFee = $order->fee_payment;
        Yii::$app->session->removeAll();
        return $this->render('payment_result', ['paymentResult' => $paymentResult,'customer_name' => $customer->fullname]);
    }

    public function actionCancelPayment() {

        $orderId = Yii::$app->request->get('order_id');
        $code = Yii::$app->request->get('code');
        
        $customer = Customer::find()->where([
            'hash_code' => $code
        ])->one();
        $order = Order::find()->where([
            'hash_code' => $orderId,
            'customer_id' => $customer->id
        ])->one();
        
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $message = 'Đơn hàng đã bị hủy vào ngày ' . Yii::$app->formatter->asDatetime(time());
            /** @var Payment $payment */
            $payment = Payment::find()->where(['order_id' => $order->id])->one();
            $payment->updateAttributes([
                'vpc_TxnResponseCode' => -2,
                'vpc_TransactionNo' => 'No Value',
                'vpc_Message' => $message,
                'dr_datetime' => time()
            ]);
            /** @var Customer $customer */
            $order->updateAttributes([
                'payment_status' => Order::STATUS_CANCELED,
            ]);
            $inventoryIds = OrderDetail::find()->where(['orders_id' => $order->id])->select(['inventory_id'])->createCommand()->queryColumn();
            Inventory::updateAll(['status' => Inventory::STATUS_AVAILABLE], ['id' => $inventoryIds]);
            $transaction->commit();
            Yii::$app->session->removeAll();

            return $this->redirect(['cancelled', 'order_id' => $order->hash_code, 'customer_name' => $customer->hash_code]);
        } catch (Exception $e) {
            $transaction->rollBack();
            return $e->getMessage();
        }
    }
    
    public function actionCancelled($order_id, $customer_name){
        
        $customer = Customer::find()->where(['hash_code' => trim($customer_name)])->one();
        $order = Order::find()->where(['hash_code' => trim($order_id)])->one();

        if (!(isset($customer) && isset($order) && $customer->id == $order->customer_id)) {
            throw new NotFoundHttpException('Please check your link');
        }
        return $this->render('cancelled',[
            'order_code' => $order->code,
            'customer_name' => $customer->fullname
        ]);
    }

    public function actionPaymentResultWs() {
        if (Yii::$app->request->isPost) {
            // Define Variables
            $orderCode = Yii::$app->request->post('req_reference_number', Yii::t('yii', 'No Value Returned'));
            $message = Yii::$app->request->post('message', Yii::t('yii', 'No Value Returned'));
            $decision = Yii::$app->request->post('decision', Yii::t('yii', 'No Value Returned'));
            $code = Yii::$app->request->post('payer_authentication_reason_code', -1);
            $transactionId = Yii::$app->request->post('transaction_id', Yii::t('yii', 'No Value'));
            $amount = Yii::$app->request->post('req_amount', Yii::t('yii', 'No Value Returned'));
            $signature = Yii::$app->request->post('signature', Yii::t('yii', 'No Value Returned'));

            /** @var Orders $order */
            $order = Order::find()->where(['code' => $orderCode])->one();
            $customer = Customer::find()->where(['id' => $order->customer_id])->one();
            $orderInValid = Yii::$app->db->createCommand("SELECT order_id
                                        FROM payment
                                        WHERE do_datetime < DATE_SUB(NOW(), INTERVAL 5 MINUTE)
                                               AND vpc_TxnResponseCode is null
                                               AND order_id = $order->id")->queryOne();
            /** @var OrderDetail $inventoryIds */
            $inventoryIds = OrderDetail::find()->where(['orders_id' => $order->id])->select(['inventory_id'])->createCommand()->queryColumn();
            //nếu trang thanh toán chấp nhận giao dịch
            if ($decision == 'ACCEPT') {
                /** @var Payment $opsvPayment */
                $opsvPayment = Payment::find()->where(['vpc_OrderInfo' => $orderCode])->one();
                //nếu thanh toán thành công
                if ($code == 100) {
                    if ($order->payment_status == 6 || $order->payment_status == Order::STATUS_EXPIRED) {
                        if ($order->payment_status == 6) {
                            $order->updateAttributes([
                                'payment_status' => 7, 'updated_date' => date('d.m.Y H:i:s'),
                            ]);
                        } elseif ($order->payment_status == Order::STATUS_EXPIRED) {
                            $order->updateAttributes([
                                'payment_status' => Order::STATUS_EXPIRED_PAID, 'updated_date' => date('d.m.Y H:i:s'),
                            ]);
                        }
                        if (empty($opsvPayment->vpc_TxnResponseCode) && $opsvPayment->vpc_TxnResponseCode != $code) {
                            $opsvPayment->updateAttributes([
                                'dr_datetime' => date('Y-m-d H:i:s'),
                                'vpc_TxnResponseCode' => $code,
                                'vpc_TransactionNo' => $transactionId,
                                'vpc_Message' => 'Thanh toán quá hạn',
                                'vpc_SecureHash' => $signature,
                            ]);
                        }
                        $amount = number_format($amount);

                        $mail = new Mail([
                            'subject' => Yii::t('yii','Booking Confirmation'),
                            'mailTo' => $customer->email,
                            'content' => ''
                        ]);
                        $mail->send(['html' => 'mail-info'], [
                            'customerName' => $customer->fullname,
                            'message' => "Chúng tôi đã nhận được thanh toán của quý khách cho đơn hàng {$order->code} số tiền {$amount} VND. Tuy nhiên đơn hàng này đã chuyển trạng thái Huỷ/Hết hạn. Chúng tôi sẽ hoàn lại số tiền trên vào tài khoản quý khách."
                        ]);

                        Inventory::updateAll(['status' => Inventory::STATUS_AVAILABLE], ['id' => $inventoryIds]);
                    } else {
                        Inventory::updateAll(['status' => Inventory::STATUS_SOLD], ['id' => $inventoryIds]);
                        $order->updateAttributes([
                            'payment_status' => Order::STATUS_PAID, 'updated_date' => date('d.m.Y H:i:s'),
                        ]);
                        if (empty($opsvPayment->vpc_TxnResponseCode) && $opsvPayment->vpc_TxnResponseCode != $code) {
                            $opsvPayment->updateAttributes([
                                'dr_datetime' => date('Y-m-d H:i:s'),
                                'vpc_TxnResponseCode' => $code,
                                'vpc_TransactionNo' => $transactionId,
                                'vpc_Message' => $message,
                                'vpc_SecureHash' => $signature,
                            ]);
                        }
                        
                        $saleable = Saleable::find()->where([
                            'id' => $order->saleable_id
                        ])->one();
                        

                        $mail = new Mail([
                            'subject' => Yii::t('yii','Booking Confirmation'),
                            'mailTo' => $customer->email,
                            'content' => ''
                        ]);
                    $mail->send(['html' => 'booking-confirm-' . Yii::$app->language], [
                        'term_condition' => $saleable->{'term_' . Yii::$app->language},
                        'customer_name' => $customer->fullname,
                        'order_code' => $orderCode,
                        'package_name' => $saleable->{'name_' . Yii::$app->language},
                        'checkindate' => date('d/m/Y', $order->checkin),
                        'checkoutdate' => date('d/m/Y', $order->checkout),
                        'amount' => $order->price,
                        'quantity' => $order->quantity,
                    ]);
                    }
                } else {
                    Inventory::updateAll(['status' => Inventory::STATUS_AVAILABLE], ['id' => $inventoryIds]);
                    //Neu order đã quá hạn nhưng đã thanh toán
                    if (!empty($orderInValid) && $code == 100 && $order->payment_status == 0) {
                        $order->updateAttributes([
                            'payment_status' => 5,
                        ]);
                    }
                    //Nếu thanh toán không thành công và don hàng dang la cho thanh toán => status fail
                    if ($code != 100 && $order->payment_status == 0) {
                        $order->updateAttributes([
                            'payment_status' => 3,
                        ]);
                    }
                    //Lưu cac don hàng da thanh toán thanh công ben viettinbank nhưng order đã quá hạn bên hệ thống => trả lại tiền cho KH
                    $paymentExpire = new PaymentExpire();
                    $paymentExpire->customer_id = $customer->id;
                    $paymentExpire->orders_id = $order->id;
                    $paymentExpire->opsv_payment_id = $opsvPayment->id;
                    $paymentExpire->amount = $amount;
                    $paymentExpire->cyber_customer_name = $customer->fullname;
                    $paymentExpire->cyber_customer_email = $opsvPayment->vpc_Customer_Email;
                    $paymentExpire->cyber_customer_phone = $opsvPayment->vpc_Customer_Phone;
                    $paymentExpire->cyber_status = $code . '-' . $order->payment_status;
                    $paymentExpire->cyber_transaction_id = $transactionId;
                    $paymentExpire->save(false);

                    $mail = new Mail([
                        'subject' => Yii::t('yii','Booking Confirmation'),
                        'mailTo' => $customer->email,
                        'content' => ''
                    ]);
                    $mail->send(['html' => 'mail-info'], [
                        'customerName' => $customer->fullname,
                        'message' => 'Đơn hàng ' . $order->code . ' thanh toán thất bại. Vui lòng đặt phòng lại hoặc liên hệ để được hướng dẫn',
                    ]);
                }
            } elseif ($decision == 'CANCEL') {
                Inventory::updateAll(['status' => Inventory::STATUS_AVAILABLE], ['id' => $inventoryIds]);
                $order->updateAttributes(['payment_status' => 3]);
                /** @var Payment $opsvPayment */
                $opsvPayment = Payment::find()->where(['vpc_OrderInfo' => $orderCode])->one();
                if (empty($opsvPayment->vpc_TxnResponseCode) && $opsvPayment->vpc_TxnResponseCode != $code) {
                    $opsvPayment->updateAttributes([
                        'dr_datetime' => date('d.m.Y H:i:s'),
                        'vpc_TxnResponseCode' => $code,
                        'vpc_TransactionNo' => $transactionId,
                        'vpc_Message' => $message,
                        'vpc_SecureHash' => $signature,
                    ]);
                }
                $mail = new Mail([
                    'subject' => Yii::t('yii','Booking Confirmation'),
                    'mailTo' => $customer->email,
                    'content' => ''
                ]);
                $mail->send(['html' => 'mail-info'], [
                    'customerName' => $customer->fullname,
                    'message' => 'Đơn hàng ' . $order->code . ' đã được hủy.'
                ]);
            }
        }
        return 0;
        //throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));
    }

    /**
     * Trang nhận kết quả trả về
     * @return string
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\base\InvalidParamException
     * @throws \yii\db\Exception
     */
    public function actionPaymentResult() {
        
        if (Yii::$app->request->isPost){
            // Define Variables
            $orderCode     = Yii::$app->request->post('req_reference_number', Yii::t('yii', 'No Value Returned'));
            $message       = Yii::$app->request->post('message', Yii::t('yii', 'No Value Returned'));
            $decision      = Yii::$app->request->post('decision', Yii::t('yii', 'No Value Returned'));
            $code          = Yii::$app->request->post('payer_authentication_reason_code', -1);
            $transactionId = Yii::$app->request->post('transaction_id', Yii::t('yii', 'No Value Returned'));

            $amount = Yii::$app->request->post('req_amount', Yii::t('yii', 'No Value Returned'));

            $paymentResult = new PaymentResult();
            /** @var Orders $order */
            $order = Order::find()->where(['code' => $orderCode])->one();
            //nếu trang thanh toán xac nhận thành công
            if ($decision == 'ACCEPT') {
                if ($code == 100 && $order != null) {
                    $transStatus                = Yii::t('yii', 'Thank you for booking with us. We have sent you an email confirmation.');
                    $paymentResult->transStatus = $transStatus;
                } else {
                    $transStatus = Yii::t('yii', 'Fail transaction');
                }
            } else {
                $transStatus = $message;
            }

            $paymentResult->message       = $message;
            $paymentResult->transStatus   = $transStatus;
            $paymentResult->transactionNo = $transactionId;
            $paymentResult->amount        = $amount;
            $paymentResult->orderInfo     = $orderCode;

            return $this->render('payment_result', ['paymentResult' => $paymentResult,'customer_name' => ""]);
        }
        return $this->render('info');
    }

    public function actionCheckDomesticPayment() {
        
        $orderId = Yii::$app->request->get('orderId', '');
        /** @var Orders $orders */
        if ($orderId == null || $orderId == '') {
            return -2;
        }

        $vibCheckout = new VIBCheckOut();
        $params['order_id'] = 'BKCCB' . $orderId. $orderId;
        $result = $vibCheckout->checkOrder($params);
        if (is_array($result) && $result['result_code'] == '0' && $result['result_data_decode']['bill_status'] == 2) {
            $orderCode = $result['result_data_decode']['order_code'];
            $transactionId = $result['result_data_decode']['bill_id'];
            /** @var Orders $order */
            $order = Order::find()->where(['id' => $orderId])->one();
            /** @var OrdersDetail $inventoryIds */
            $inventoryIds = OrderDetail::find()->where(['orders_id' => $orderId])->select(['inventory_id'])->createCommand()->queryColumn();
            /** @var Payment $payment */
            $payment = Payment::find()->where(['order_id' => $orderId])->one();
            $customerId = $order->customer_id;
            $customer = Customer::find()->where(['id' => $customerId])->one();
            //Nếu đơn hàng thanh toán thành công
            if ($order->payment_status == 0) {
                Inventory::updateAll(['status' => 1, 'sold_date' => time()], ['id' => $inventoryIds]);
                $order->updateAttributes(['payment_status' => 1,
                    'updated_date' => date('d.m.Y H:i:s')]);
                $payment->updateAttributes([
                    'vpc_TxnResponseCode' => $result['result_code'],
                    'vpc_TransactionNo' => $transactionId,
                    'vpc_Message' => 'Đơn hàng thanh toán thành công',
                    'dr_datetime' => date('Y-m-d H:i:s')
                ]);

                
                $saleable = Saleable::find()->where([
                    'id' => $order->saleable_id
                ])->one();

                $mail = new Mail([
                    'subject' => Yii::t('yii','Booking Confirmation'),
                    'mailTo' => $customer->email,
                    'content' => ''
                ]);

                    $mail->send(['html' => 'booking-confirm-' . Yii::$app->language], [
                        'term_condition' => $saleable->{'term_' . Yii::$app->language},
                        'customer_name' => $customer->fullname,
                        'order_code' => $orderCode,
                        'package_name' => $saleable->{'name_' . Yii::$app->language},
                        'checkindate' => date('d/m/Y', $order->checkin),
                        'checkoutdate' => date('d/m/Y', $order->checkout),
                        'amount' => $order->price,
                        'quantity' => $order->quantity,
                    ]);
                return "Your reservation code <span style='color: red'>{$order->code}</span> has been successfully paid.";
            }
        }
        return -2;
    }

}
