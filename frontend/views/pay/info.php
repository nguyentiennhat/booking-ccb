<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = Yii::t('yii','Thank you');
?>
<div class="site-about row">
    <div class="col-lg-6 col-lg-offset-3 content">
    <p><?=Yii::t('yii', 'Your transaction is processing. Thank you for booking with us.<br>To update with the latest events from Cocobay, please visit www.cocobay.vn')?></p>
</div>
</div>
