<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\inventory\type\InventoryType;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\saleable\Saleable */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('yii', 'Saleables'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="saleable-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('yii', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('yii', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
			'price',
			'child_price',
			'local_price',
            'name_en',
            'name_vi',
            'note_vi:html',
            'note_en:html',
            'term_vi:html',
            'term_en:html',
            [
                'label' => 'Link',
                'value' => Url::to('?event=' . $model->plan->link . '&type=' . $model->link,true)
            ],
            [
                'label' => 'Plan',
                'value' => $model->plan->name
            ],
            'code_name',
			[
				'label' => 'Is Dynamic Price',
				'value' => $model->is_dynamic_price?'True':'False'
			],
			[
				'label' => 'Is Dynamic Checkout',
				'value' => $model->is_dynamic_checkout?'True':'False'
			],
			[
				'label' => 'Is Not Check Inventory',
				'value' => $model->is_check_inventory?'True':'False'
			],
			[
				'label' => 'Is Selectable Inventory Type',
				'value' => $model->is_selectable_inventory?'True':'False'
			],
			'layout',
            'status',
            'created_by',
            'created_at:datetime',
        ],
    ]) ?>
    
    <?php
    foreach ($model->inventories as $inventory){
        echo DetailView::widget([
            'model' => $inventory,
            'attributes' => [
                [
                    'label' => 'Type',
                    'value' => InventoryType::findOne(['id' => $inventory['inventory_type_id']])->name
                ],
                'quantity',
                'period',
            ],
        ]);
    }
    ?>

</div>
