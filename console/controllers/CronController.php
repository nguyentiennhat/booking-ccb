<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace console\controllers;

use fedemotta\cronjob\models\CronJob;
use yii\console\Controller;
use common\models\inventory\Inventory;
use yii\helpers\ArrayHelper;
use common\models\order\Order;
use common\models\payments\VIBCheckOut;
use common\models\order\detail\OrderDetail;

/**
 * Description of InventoryCronController
 *
 * @author theredlab
 */
class CronController extends Controller {

    /**
     * Run SomeModel::some_method for a period of time
     * @param string $from
     * @param string $to
     * @return int exit code
     */
    public function actionInit($from, $to) {
        $dates = CronJob::getDateRange($from, $to);
        $command = CronJob::run($this->id, $this->action->id, 0, CronJob::countDateRange($dates));
        if ($command === false) {
            return Controller::EXIT_CODE_ERROR;
        } else {
            foreach ($dates as $date) {
                echo $date;
            }
            $command->finish();
            return Controller::EXIT_CODE_NORMAL;
        }
    }

    public function actionUpdateExpiredInventory(){        
        $expired_orders = Order::find()->select(Order::tableName().'.id')->join('LEFT JOIN', \common\models\payment\Payment::tableName(), Order::tableName().'.id = order_id')->where([
            'payment_status' => Order::STATUS_PENDING,
            'do_datetime' => strtotime("-40 minutes"),
            'vpc_TxnResponseCode' => null,
            'payment_type' => 1
        ])->createCommand()->queryAll();
        
        foreach ($expired_orders as $expired_order){
            echo '['.date("Y-m-d H:i:s")."]: ". $expired_order['id'] ." is checking\n";

            $response = checkDomesticPayment($expired_order['id']);
            if($response == -2){
                
                $order = Order::find()->where([
                    'id' => $expired_order['id']
                ])->one();

                $order->updateAttributes([
                    'status' => Order::STATUS_EXPIRED,
                    'payment_status' => Order::STATUS_EXPIRED,
                ]);
                
                echo('Expired\n');
                $order_details = OrderDetail::find()->where([
                    'order_id' => $expired_order['id']
                ])->createCommand()->queryAll();
                
                $inventories = Inventory::find()->where([
                    'id' => ArrayHelper::getColumn($order_details, 'inventory_id')
                ])->all();
                
                foreach ($inventories as $inventory){
                    $inventory->updateAttributes([
                        'status' => Inventory::STATUS_AVAILABLE,
                    ]);
                }
                
                $payment = \common\models\payment\Payment::find()->where([])->one();
                $payment->updateAttributes([
                    'vpc_TxnResponseCode' => 0,
                    'dr_datetime' => time(), 
                    'vpc_message'=>'System: be expired'
                ]);
            }
        }
                
        $expired_orders = Order::find()->select(Order::tableName().'.id')->join('LEFT JOIN', \common\models\payment\Payment::tableName(), Order::tableName().'.id = order_id')->where([
            'payment_status' => Order::STATUS_PENDING,
            'do_datetime' => strtotime("-40 minutes"),
            'vpc_TxnResponseCode' => null,
            'payment_type' => 0
        ])->createCommand()->queryAll();
        
        foreach ($expired_orders as $expired_order){
            echo '['.date("Y-m-d H:i:s")."]: ". $expired_order['id'] ." is checking\n";

            $order = Order::find()->where([
                'id' => $expired_order['id']
            ])->one();
            
            $order->updateAttributes([
                'status' => Order::STATUS_EXPIRED,
                'payment_status' => Order::STATUS_EXPIRED,
            ]);
            
            echo('Expired\n');
            $order_details = OrderDetail::find()->where([
                'order_id' => $expired_order['id']
            ])->createCommand()->queryAll();

            $inventories = Inventory::find()->where([
                'id' => ArrayHelper::getColumn($order_details, 'inventory_id')
            ])->all();

            foreach ($inventories as $inventory){
                $inventory->updateAttributes([
                    'status' => Inventory::STATUS_AVAILABLE,
                ]);
            }

            $payment = \common\models\payment\Payment::find()->where([])->one();
            $payment->updateAttributes([
                'vpc_TxnResponseCode' => 0,
                'dr_datetime' => time(), 
                'vpc_message'=>'System: be expired'
            ]);

        }
    }

    public function checkDomesticPayment($orderId) {
        /** @var Orders $orders */
        if ($orderId == null || $orderId == '') {
            return -2;
        }
        $vibCheckout = new VIBCheckOut();
        $params['order_id'] = 'STLDMT' . $orderId;
        $result = $vibCheckout->checkOrder($params);
        if (is_array($result) && $result['result_code'] == '0' && $result['result_data_decode']['bill_status'] == 2) {
            $orderCode = $result['result_data_decode']['order_code'];
            $transactionId = $result['result_data_decode']['bill_id'];
            /** @var Orders $order */
            $order = Orders::find()->where(['id' => $orderId])->one();
            /** @var OrdersDetail $inventoryIds */
            $inventoryIds = OrderDetail::find()->where(['orders_id' => $orderId])->select(['inventory_id'])->createCommand()->queryColumn();
            /** @var Payment $payment */
            $payment = OpsvPayment::find()->where(['order_id' => $orderId])->one();
            list(,,, $customerId) = explode('-', $orderCode);
            $customer = Customer::find()->where(['id' => $customerId])->one();
            //Nếu đơn hàng thanh toán thành công
            if ($order->payment_status == 0) {
                Inventory::updateAll(['status' => 1, 'sold_date' => time()], ['id' => $inventoryIds]);
                $order->updateAttributes(['payment_status' => 1,
                    'updated_date' => date('d.m.Y H:i:s')]);
                $this->updateVoucher($order->voucher_code, 1);
                $payment->updateAttributes([
                    'vpc_TxnResponseCode' => $result['result_code'],
                    'vpc_TransactionNo' => $transactionId,
                    'vpc_Message' => 'Đơn hàng thanh toán thành công',
                    'dr_datetime' => date('Y-m-d H:i:s')
                ]);

                $orderDetails = Yii::$app->db->createCommand("select count(*) as number, name, date_in, order_type_id "
                                . "from order_detail join ordertype on order_type_id = ordertype.id "
                                . "where orders_id = {$order->id} group by order_type_id")->queryAll();
                /** @var Inventory[] $inventorys */
                $orderUpgrades = Yii::$app->db->createCommand("select upgrade_package.price, upgrade_package.name "
                                . "from order_upgrade join upgrade_package on upgrade_package.id = order_upgrade.upgrade_package_id "
                                . "where order_upgrade.order_id = {$order->id}")->queryAll();


                $mail = new Mail([
                    'subject' => 'Xác nhận thanh toán',
                    'mailTo' => $customer->email,
                    'content' => ''
                ]);

                $mail->send(['html' => $this->getEmailTemplate($orderDetails[0]['order_type_id'])], [
                    'customerName' => $customer->fullname,
                    'email' => $customer->email,
                    'phone' => $customer->phone,
                    'amount' => $order->total_price,
                    'orderCode' => $orderCode,
                    'confimationNumber' => $order->id,
                    'orderDetails' => $orderDetails,
                    'orderUpgrades' => $orderUpgrades
                ]);
                $transStatus = "Your reservation code <span style='color: red'>{$order->code}</span> has been successfully paid.";
            }
            return $transStatus;
        }
        return -2;
    }

    public function actionUpdateProcessingInventory(){
        $inventories = Inventory::find()->where([
            'status' => Inventory::STATUS_PROCESSING,
        ])->andFilterWhere(['<','modified_date',strtotime("-20 minutes")])->createCommand()->queryAll();
        echo count($inventories);
        if(count($inventories) > 0){
            echo Inventory::updateAll([
                'status' => Inventory::STATUS_AVAILABLE
            ], ['id' => ArrayHelper::getColumn($inventories, 'id')]);
        } else {
            echo 'No inventory found';
        }
    }
    
    /**
     * Run SomeModel::some_method for today only as the default action
     * @return int exit code
     */
    public function actionIndex() {
        return $this->actionInit(date("Y-m-d"), date("Y-m-d"));
    }

    /**
     * Run SomeModel::some_method for yesterday
     * @return int exit code
     */
    public function actionYesterday() {
        return $this->actionInit(date("Y-m-d", strtotime("-1 days")), date("Y-m-d", strtotime("-1 days")));
    }

}
