<?php
/**
 * Created by PhpStorm.
 * User: Team
 * Date: 7/16/2016
 * Time: 11:14 AM
 */

namespace backend\assets;

use yii\web\AssetBundle;

class DatatablesAsset extends AssetBundle
{
    public $basePath = '@webroot/js/plugins/datatables';
    public $baseUrl = '@web/js/plugins/datatables';
    public $css = [
        'plugins/bootstrap/dataTables.bootstrap.min.css',
        'extensions/FixedColumns/css/fixedColumns.dataTables.min.css',
        'extensions/FixedColumns/css/fixedColumns.bootstrap.min.css',
    ];
    public $js = [
        'jquery.dataTables.min.js',
        'plugins/bootstrap/dataTables.bootstrap.min.js',
        'extensions/dataTables.conditionalPaging.js',
        'extensions/FixedColumns/js/dataTables.fixedColumns.min.js'
    ];
}