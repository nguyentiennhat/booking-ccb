<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

$baseUrl = '';
return [
    'id' => 'app-frontend',
    'homeUrl'             => $baseUrl.'/',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'timeZone'            => 'Asia/Ho_Chi_Minh',
    'components' => [
        'request' => [
            'baseUrl' => $baseUrl,
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'ccb/error',
        ],
        'urlManager' => ['baseUrl' => $baseUrl,
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
				[
                    'class' => 'yii\web\GroupUrlRule',
                    'rules' => [
                        '/'               => 'ccb/index',
                    ],
                ],
                '<controller>'                                   => '<controller>/index',
                '<controller:[a-z-]+>/<action:[a-z-]+>/<id:\d+>' => '<controller>/<action>',
            ],
        ],
        'reCaptcha' => [
            'name' => 'reCaptcha',
            'class' => 'himiklab\yii2\recaptcha\ReCaptcha',
            'siteKey' => '6LfPhEEUAAAAAOnWReUvW3bWm8kiM11te2Mt8Hfi',
            'secret' => '6LfPhEEUAAAAAP_i-58fE0lqiL4wIc82-qrYDD80',
        ],
        'i18n'         => [
            'translations' => [
                'yii' => [
                    'class'   => 'yii\i18n\PhpMessageSource',
                    'fileMap' => [
                        'yii' => 'yii.php'
                    ]
                ]
            ],
        ],
    ],
    'params' => $params,
    'language' => 'vi',
];
