<?php

namespace common\models\saleable;

/**
 * This is the ActiveQuery class for [[Saleable]].
 *
 * @see Saleable
 */
class SaleableQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere('[[status]]=1');
    }

    /**
     * @inheritdoc
     * @return Saleable[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Saleable|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
