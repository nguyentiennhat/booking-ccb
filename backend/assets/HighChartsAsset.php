<?php

namespace backend\assets;

use yii\web\AssetBundle;
use backend\assets\AppAsset;
/**
 * Team asset bundle.
 */
class HighChartsAsset extends AssetBundle
{
    public $basePath = '@webroot/js/plugins/highcharts';
    public $baseUrl = '@web/js/plugins/highcharts';

    public $js = [
        'highcharts.js',
    ];
    public $css= [
    ];
    public $depends = [
        AppAsset::class
    ];
}
