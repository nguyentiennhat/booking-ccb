<?php

namespace common\models\payment\base;

use Yii;

/**
 * This is the model class for table "payment".
 *
 * @property int $id
 * @property int $order_id
 * @property int $type 0: ngan luong - 1: noidia - 2: quocte
 * @property string $do_datetime
 * @property int $timecode
 * @property string $vpc_MerchTxnRef
 * @property string $vpc_Amount
 * @property string $vpc_TicketNo
 * @property string $vpc_OrderInfo
 * @property string $vpc_Customer_Phone
 * @property string $vpc_Customer_Email
 * @property int $vpc_Customer_Id
 * @property string $vpc_Customer_Name
 * @property string $dr_datetime
 * @property int $vpc_TxnResponseCode 100: Success - 1: failed
 * @property string $vpc_TransactionNo
 * @property string $vpc_Message
 * @property string $vpc_SecureHash
 * @property string $token_nl
 * @property int $payment_type
 * @property string $note
 */
class PaymentBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'type', 'do_datetime', 'timecode', 'vpc_MerchTxnRef', 'vpc_Amount', 'vpc_TicketNo', 'vpc_OrderInfo', 'vpc_Customer_Phone', 'vpc_Customer_Email', 'vpc_Customer_Id', 'vpc_Customer_Name'], 'required'],
            [['order_id', 'type', 'timecode', 'vpc_Customer_Id', 'vpc_TxnResponseCode', 'payment_type'], 'integer'],
            [['do_datetime', 'dr_datetime'], 'safe'],
            [['note'], 'string'],
            [['vpc_MerchTxnRef'], 'string', 'max' => 34],
            [['vpc_Amount'], 'string', 'max' => 21],
            [['vpc_TicketNo'], 'string', 'max' => 15],
            [['vpc_OrderInfo'], 'string', 'max' => 32],
            [['vpc_Customer_Phone'], 'string', 'max' => 16],
            [['vpc_Customer_Email'], 'string', 'max' => 100],
            [['vpc_Customer_Name'], 'string', 'max' => 500],
            [['vpc_TransactionNo'], 'string', 'max' => 12],
            [['vpc_Message'], 'string', 'max' => 1000],
            [['vpc_SecureHash'], 'string', 'max' => 200],
            [['token_nl'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'type' => 'Type',
            'do_datetime' => 'Do Datetime',
            'timecode' => 'Timecode',
            'vpc_MerchTxnRef' => 'Vpc  Merch Txn Ref',
            'vpc_Amount' => 'Vpc  Amount',
            'vpc_TicketNo' => 'Vpc  Ticket No',
            'vpc_OrderInfo' => 'Vpc  Order Info',
            'vpc_Customer_Phone' => 'Vpc  Customer  Phone',
            'vpc_Customer_Email' => 'Vpc  Customer  Email',
            'vpc_Customer_Id' => 'Vpc  Customer  ID',
            'vpc_Customer_Name' => 'Vpc  Customer  Name',
            'dr_datetime' => 'Dr Datetime',
            'vpc_TxnResponseCode' => 'Vpc  Txn Response Code',
            'vpc_TransactionNo' => 'Vpc  Transaction No',
            'vpc_Message' => 'Vpc  Message',
            'vpc_SecureHash' => 'Vpc  Secure Hash',
            'token_nl' => 'Token Nl',
            'payment_type' => 'Payment Type',
            'note' => 'Note',
        ];
    }
}
