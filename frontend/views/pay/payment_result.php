<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = Yii::t('yii','Hi ') . $customer_name;
?>
<div class="site-about row">
    <div class="col-lg-6 col-lg-offset-3 content">
    <h1><?=Yii::t('yii','Hi ')?><?= Html::encode($customer_name) ?></h1>
    <h4 class="text-white">
        <?= Yii::t( 'yii', $paymentResult->transStatus ) ?>
    </h4>
    <div class="col-md-12">
<?php if (!empty($paymentResult->message) && false){ ?> 
        <div class="row">
            <label><?= Yii::t( 'yii', 'Notification from the port' ) ?></label> <?php echo $paymentResult->message ?>
        </div>
<?php } ?>
    </div>
</div>
</div>
