<?php

namespace common\models\inventory\type\base;

use Yii;

/**
 * This is the model class for table "inventory_type".
 *
 * @property int $id
 * @property string $name
 * @property double $price
 * @property double $child_price
 * @property double $local_price
 * @property string $note
 * @property int $created_at
 * @property int $update_at
 * @property int $transaction_code
 * @property string $code_name
 */
class InventoryTypeBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inventory_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['price', 'child_price', 'local_price'], 'number'],
            [['note'], 'string'],
            [['created_at', 'update_at', 'transaction_code'], 'integer'],
            [['code_name'], 'required'],
            [['name'], 'string', 'max' => 100],
            [['code_name'], 'string', 'max' => 8],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'price' => 'Price',
			'child_price' => 'Price for child',
			'local_price' => 'Price for local',
            'note' => 'Note',
            'created_at' => 'Created At',
            'update_at' => 'Update At',
            'transaction_code' => 'Transaction Code',
            'code_name' => 'Code Name',
        ];
    }
}
