<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model frontend\models\Booking */
/* @var $form ActiveForm */

$js=<<<EOT
$("#booking1").submit( function(e){
	e.preventDefault();
    e.stopImmediatePropagation();
	var form = $(this);
	$.ajax({
		url    : '/cocobay/?event=coco-bus-tour&type=cocobus-ticket',
		type   : 'POST',
		data   : form.serialize(),
		success: function (response) 
		{                  
		   console.log(response);
		},
		error  : function (e) 
		{
			console.log(e);
		}
	});
    return false;        
  })
EOT;

$this->registerJs($js);

?>

	<!-- Modal -->
	<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-hidden="true">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title" id="term">Term and conditions</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <div class="modal-body">
			<?=$term?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
    <?php $form = ActiveForm::begin(['id'=>'booking']); ?>
        <div class="col-sm-6">
           <div class="form-group">
            <label class="form-label" for="name">Your Name</label>
            <?= $form->field($model, 'name')->label(false) ?>
          </div>
          <div class="form-group">
          <label class="form-label" for="name">Your Email</label>
          <?= $form->field($model, 'email')->label(false) ?>
         </div>
         <div class="form-group">
            <label class="form-label" for="pick">Local Residence</label>
            <?= $form->field($model, 'local')->DropDownList(['0' => 'No', '1' => 'Yes'], ['onchange'=>'optionres()'])->label(false) ?>
           <div id="optionres"></div>
           <script>
            function optionres() {
                if (document.getElementById("booking-local").value == 1){
                document.getElementById("optionres").innerHTML = "* Please show your photo ID when using ticket" ;}
                else {
                document.getElementById("optionres").innerHTML = "" ;
                }
            }
            </script>
        </div>
      </div>
      <div class="col-sm-6 mob">
        <div class="form-group">
         <label class="form-label" for="phone">Your Phone</label>
         <?= $form->field($model, 'phone')->label(false) ?>
       </div>
        <div class="form-group">
       <div class="col-sm-4 col-xs-12 active-input marbtt" style="padding: 0px;">
            <label class="form-label" for="name">Adult</label>
            <?= $form->field($model, 'adult', ['options' => ['class' => '']])->textInput([
                                 'type' => 'number'
                            ])->label(false) ?>
         </div>
       <div class="col-sm-4 col-xs-12 active-input marbtt" style="padding: 0px;">
            <label class="form-label" for="name">Children</label>
            <?= $form->field($model, 'children', ['options' => ['class' => '']])->textInput([
                                 'type' => 'number','placeholder' => 'Free under 6 years old'
                            ])->label(false) ?>
         </div>
		 <?php if(!empty($types)){ ?>
         <div class="col-sm-4 col-xs-12 active-input marbtt" style="padding: 0px;">
           <label class="form-label mob" for="note">Type of Ticket</label>
		   <?= $form->field($model, 'type', ['options' => ['class' => '']])->dropDownList($types)->label(false)  ?>
         </div>
		 <?php } ?>
       </div>                        
         <?= $form->field($model, 'note')->label("Note (Optional)", ['class' => 'form-label']) ?>
       </div>
	   
        <div class="col-sm-12">
           <label class="form-label mob" for="payment_type">Payment Type</label>
			<?= $form->field($model, 'payment_type')->radioList($payment_list, ['class' => 'form-content'])->label(false) ?>
		</div>
        <div class="col-sm-12">
			<?= $form->field($model, 'accept_term')->checkBox([
				'label' => 'I accept the <a href="javascript:void(0)"  data-toggle="modal" data-target="#modal">Terms and Conditions</a>'
			], ['class' => 'form-content'])->label(false) ?>
		</div>
       <div class="col-sm-12 center">
        <button class="btn subcribtn" onclick="var magnificPopup = $.magnificPopup.instance; magnificPopup.close();">SUBMIT</button>
      </div>
    <?php ActiveForm::end();?>
