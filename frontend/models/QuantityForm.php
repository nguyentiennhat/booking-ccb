<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\models;

use yii\base\Model;
use Yii;
/**
 * Description of QuantityForm
 *
 * @author theredlab
 */
class QuantityForm extends Model{
    
    const SCR_DYNAMIC_CHECKOUT = 'dynamiccheckout';

        public $quantity;
    public $checkindate;
    public $checkoutdate;
    public $planhashcode;
    public $saleablehashcode;
    
    public function rules() {
        return [
            [['planhashcode','saleablehashcode'],'required'],
            [['quantity'],'required',
                'message'=>Yii::t('yii','Please enter a value for quantity.')],
            [['checkindate'],'required',
                'message'=> Yii::t('yii','Please enter a value for check-in date.')],
            [['checkoutdate'], 'safe'],
            [['checkoutdate'], 'required', 'on' => [self::SCR_DYNAMIC_CHECKOUT]],
        ];
    }
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCR_DYNAMIC_CHECKOUT] = ['planhashcode','saleablehashcode','quantity','checkindate','checkoutdate'];
        return $scenarios;
    }
}
