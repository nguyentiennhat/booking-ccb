<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\order\models;
use yii\base\Model;
/**
 * Description of ExportForm
 *
 * @author theredlab
 */
class ExportForm extends Model{
    public $fromDate;
    public $toDate;
    public $status;
    
    public function rules() {
        return [
            [['fromDate','toDate','status'],'safe'],
            [['status'],'integer']
        ];
    }
}
