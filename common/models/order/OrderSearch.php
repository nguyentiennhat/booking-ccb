<?php

namespace common\models\order;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\order\Order;
use common\models\customer\Customer;

/**
 * OrderSearch represents the model behind the search form of `common\models\order\Order`.
 */
class OrderSearch extends Order
{
    public $customer_email;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_date', 'updated_date', 'updated_by', 'fee_payment', 'status', 'payment_status'], 'integer'],
            [['code', 'hash_code'], 'safe'],
            [['price'], 'number'],
            [['customer_email'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find()->join('LEFT JOIN', Customer::tableName(), Customer::tableName() . '.id = customer_id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'created_date' => $this->created_date,
            'updated_date' => $this->updated_date,
            'updated_by' => $this->updated_by,
            'price' => $this->price,
            'fee_payment' => $this->fee_payment,
            'status' => $this->status,
            'payment_status' => $this->payment_status,
            'email' => $this->customer_email,
        ]);

        $query->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'hash_code', $this->hash_code]);

        return $dataProvider;
    }
}
