<?php

namespace common\models\inventory;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\inventory\Inventory;

/**
 * InventorySearch represents the model behind the search form of `common\models\inventory\Inventory`.
 */
class InventorySearch extends Inventory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'inv_type_id', 'price', 'created_date', 'modified_date', 'created_by', 'modified_by', 'sold_date', 'customer_id', 'orders_id', 'status'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Inventory::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'inv_type_id' => $this->inv_type_id,
            'price' => $this->price,
            'created_date' => $this->created_date,
            'modified_date' => $this->modified_date,
            'created_by' => $this->created_by,
            'modified_by' => $this->modified_by,
            'sold_date' => $this->sold_date,
            'customer_id' => $this->customer_id,
            'orders_id' => $this->orders_id,
            'status' => $this->status,
        ]);

        return $dataProvider;
    }
}
