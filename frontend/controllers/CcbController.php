<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\Cookie;
use common\models\customer\Customer;
use yii\web\NotFoundHttpException;
use common\models\plan\Plan;
use common\models\saleable\Saleable;
use backend\utilities\helpers\Mail;
use yii\helpers\Url;
use frontend\models\QuantityForm;
use common\models\inventory\Inventory;
use frontend\models\PaymentTypeForm;
use yii\helpers\ArrayHelper;
use common\models\order\Order;
use common\models\order\detail\OrderDetail;
use backend\utilities\helpers\ClientHelper;
use common\models\payments\Cybersource;
use common\models\payment\Payment;
use Swift_TransportException;
use backend\utilities\helpers\TimeHelper;

/**
 * Site controller
 */
class CcbController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function beforeAction($action) {
        

        if (Yii::$app->request->isGet && Yii::$app->controller->action->id != 'error' && Yii::$app->controller->action->id != 'info' && false) {
            $customer_hash_code = Yii::$app->request->get('code', '');
            $event_link = Yii::$app->request->get('event', '');
            $saleable_link = Yii::$app->request->get('type', '');

            if (empty($event_link) || empty($saleable_link)) {
                throw new NotFoundHttpException('Please check your link');
            }

            if (empty($customer_hash_code)) {
                if (Yii::$app->controller->action->id != 'index') {
                    throw new NotFoundHttpException('Please check your link');
                }
            } else {

                $inventory_ids = Yii::$app->session->get($customer_hash_code . '_inventory_ids', []);
                $quantity_form = Yii::$app->session->get($customer_hash_code . '_form', '');

                $start_time = Yii::$app->session->get($customer_hash_code . '_start_time', '');
                if ((Yii::$app->controller->action->id == 'confirm-order' && empty($inventory_ids) && empty($quantity_form)) || (Yii::$app->controller->action->id != 'success' && !empty($start_time) && $start_time < strtotime("-60 minutes"))) {

                    Inventory::updateAll([
                        'status' => Inventory::STATUS_AVAILABLE
                            ], ['id' => $inventory_ids, 'status' => Inventory::STATUS_PROCESSING]);

                    Yii::$app->session->removeAll();
                    Yii::$app->session->setFlash('message', 'Your session is expired');
                    return $this->redirect(['index', 'event' => $event_link, 'type' => $saleable_link]);
                }
            }
        }
        return parent::beforeAction($action);
    }

    public function init() {
        parent::init();
        $cookies = Yii::$app->request->cookies;
        $language = $cookies->getValue('language', 'vi');
        Yii::$app->language = $language;
    }

    public function actionChangeLang() {
        $lang = Yii::$app->request->post('lang', ''); //en
        // Yii::$app->language = $lang;
        $cookie = new Cookie([
            'name' => 'language',
            'value' => $lang
        ]);
        \Yii::$app->getResponse()->getCookies()->add($cookie);
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex() {
        $event_link = Yii::$app->request->get('event', '');
        $saleable_link = Yii::$app->request->get('type', '');
        $plan = Plan::find()->where(['link' => $event_link])->isRunning()->one();
        $saleable = Saleable::find()->where(['link' => $saleable_link])->active()->one();
        $customer = new Customer();
        if (isset($plan) && isset($saleable) && isset($saleable->plan) && $saleable->plan->hash_code == $plan->hash_code) {
            if (Yii::$app->request->isPost) {
                //Customer[reCaptcha]
                //if($customer->getr)
                $email = Yii::$app->request->post('Customer')['email'];
                $email = Yii::$app->request->post('Customer')['email'];
                $old_customer = Customer::find()->where(['email' => trim($email)])->one();

                if (isset($old_customer)) {
                    $customer = $old_customer;
                }

                $customer->hash_code = Yii::$app->getSecurity()->generateRandomString(24);
                $customer->otp_date = time();
                $customer->is_verified = 0;
                $customer->verified_date = null;

                if ($customer->load(Yii::$app->request->post()) && $customer->save()) {
                    Yii::$app->session->set('customer_id', $customer->id);
                    /**
                     * I have wrote this but the client said they do not want customer to have too much steps before they can buy something, please confirm before you un-comment this
                    $mail = new Mail([
                        'subject' => $plan->name,
                        'mailTo' => $customer->email,
                        'content' => ''
                    ]);
                    try {
                        $mail->send(['html' => 'otp-confirm'], [
                            'customerName' => $customer->fullname,
                            'otp' => Url::to('/ccb/confirm?code=' . $customer->hash_code . '&event=' . $event_link . '&type=' . $saleable_link, true)
                        ]);
                    } catch (Swift_TransportException $e) {
                        throw new NotFoundHttpException('Our server is busing at the moment, please come back later.');
                    }
                    return $this->redirect(['ccb/info', 'event' => $event_link, 'type' => $saleable_link]);
                     * 
                     */
                    return $this->redirect(Url::to('/ccb/confirm?code=' . $customer->hash_code . '&event=' . $event_link . '&type=' . $saleable_link, true));
                }
            }
            Yii::trace($customer->getErrors());
            return $this->render('index', [
                        'customer' => $customer,
                        'name' => $saleable->{'name_' . Yii::$app->language}
            ]);
        }
        throw new NotFoundHttpException('Please check your link');
    }

    public function actionInfo() {
        $customer_id = Yii::$app->session->get('customer_id', '');
        $customer = Customer::findOne(['id' => $customer_id]);
        if (isset($customer)) {
            return $this->render('about', [
                        'customer' => $customer
            ]);
        }
        throw new NotFoundHttpException('Please check your link');
    }

    public function actionConfirm() {
        $customer_hash_code = Yii::$app->request->get('code', '');
        $event_link = Yii::$app->request->get('event', '');
        $saleable_link = Yii::$app->request->get('type', '');


        $plan = Plan::find()->where(['link' => $event_link])->isRunning()->one();
        $saleable = Saleable::find()->where(['link' => $saleable_link])->active()->one();
        $customer = Customer::find()->where(['hash_code' => trim($customer_hash_code)])->one();

        if (!(isset($customer) && isset($plan) && isset($saleable) && $saleable->plan->hash_code == $plan->hash_code)) {
            throw new NotFoundHttpException('Please check your link');
        }

        $time = ($customer->is_verified == 1) ? true : $customer->otp_date + 3600 > time();
        if ($time) {

            Yii::$app->session->set($customer_hash_code . 'plan_hash_code', $plan->hash_code);
            Yii::$app->session->set($customer_hash_code . 'saleable_hash_code', $saleable->hash_code);

            $customer->updateAttributes([
                'is_verified' => 1,
                'verified_date' => time(),
                    //'hash_code' => Yii::$app->getSecurity()->generateRandomString(24)
            ]);

            return $this->redirect(['book', 'code' => $customer_hash_code, 'event' => $event_link, 'type' => $saleable_link]);
        }
        return $this->render('expired', [
                    'customer' => $customer
        ]);
    }

    public function actionBook() {
        $customer_hash_code = Yii::$app->request->get('code', '');
        $event_link = Yii::$app->request->get('event', '');
        $saleable_link = Yii::$app->request->get('type', '');
        $customer = Customer::find()->where(['hash_code' => trim($customer_hash_code)])->one();

        $quantityForm = new QuantityForm();

        $plan = Plan::find()->where(['link' => $event_link])->isRunning()->one();
        $saleable = Saleable::find()->where(['link' => $saleable_link])->active()->one();

        if (!(isset($customer) && isset($plan) && isset($saleable) && $saleable->plan->hash_code == $plan->hash_code)) {
            throw new NotFoundHttpException('Please check your link');
        }
        
        if($saleable->is_dynamic_checkout){
            $quantityForm->setScenario(QuantityForm::SCR_DYNAMIC_CHECKOUT);
        }
        
        //$ids = Yii::$app->session->get($customer_hash_code . '_inventory_ids', []);
        //Inventory::updateAll(['status' => Inventory::STATUS_AVAILABLE], ['id' => $ids, 'status' => Inventory::STATUS_PROCESSING, 'customer_id' => $customer->id]);
        Yii::$app->session->set($customer_hash_code . '_inventory_ids', []);
        Yii::$app->session->set($customer_hash_code . '_form', null);
        Yii::$app->session->set($customer_hash_code . '_start_time', null);
        
        if (Yii::$app->request->isPost && $quantityForm->load(Yii::$app->request->post())) {

            Yii::trace(ArrayHelper::toArray($quantityForm));
            $toDate = null;
            $fromDate = $quantityForm->checkindate;

            if($saleable->is_dynamic_checkout){
                $toDate = $quantityForm->checkoutdate;
            } else {
                $toDate = $saleable->getCheckoutDate($quantityForm->checkindate);
            }
            
            Yii::trace($fromDate);
            Yii::trace($plan->start_date);
            Yii::trace($toDate);
            Yii::trace($plan->end_date);
            
            if (date('Y-m-d H:i:s', strtotime($fromDate)) >= $plan->start_date && date('Y-m-d H:i:s', strtotime($toDate)) <= $plan->end_date && date('Y-m-d H:i:s', strtotime($fromDate)) <= date('Y-m-d H:i:s', strtotime($toDate))) {
                
                Yii::$app->session->set($customer_hash_code . '_form', $quantityForm);
                
                if($saleable->allow_select_price){
                    return $this->redirect(['choose', 'code' => $customer_hash_code, 'event' => $event_link, 'type' => $saleable_link]);
                }
                $inventory_ids = [];
                //if the saleable does not require to check inventory, should avoid this part
                if(!($saleable->is_check_inventory)){
                    $inventory_ids = $saleable->getInventoryIds($fromDate, $toDate, is_numeric($quantityForm['quantity']) ? $quantityForm['quantity'] + 1 : 0);
                }
                Yii::trace($inventory_ids);
                //$updated_count = Inventory::updateAll(['status' => Inventory::STATUS_PROCESSING, 'customer_id' => $customer->id, 'modified_date' => time(), 'modified_by' => $customer->id], ['id' => $inventory_ids, 'status' => Inventory::STATUS_AVAILABLE]);

                //if (count($inventory_ids) > 0 && $updated_count == count($inventory_ids)) {

                    Yii::$app->session->set($customer_hash_code . '_inventory_ids', $inventory_ids);
                    Yii::$app->session->set($customer_hash_code . '_customer', $customer);
                    Yii::$app->session->set($customer_hash_code . '_start_time', time());

                    return $this->redirect(['confirm-order', 'code' => $customer_hash_code, 'event' => $event_link, 'type' => $saleable_link]);
                /**} else {
                    Inventory::updateAll(['status' => Inventory::STATUS_AVAILABLE], ['id' => $inventory_ids, 'status' => Inventory::STATUS_PROCESSING, 'customer_id' => $customer->id]);
                    Yii::trace($inventory_ids);
                    Yii::$app->session->setFlash('message', Yii::t('yii', 'Out of inventory'));
                }**/
            } else {
                Yii::$app->session->setFlash('message', Yii::t('yii', 'End of event'));
            }
        }
        
        $quantityForm->saleablehashcode = $saleable->hash_code;
        $quantityForm->planhashcode = $plan->hash_code;

        return $this->render('saleable_detail', [
                    'customer_name' => $customer->fullname,
                    'saleable_name' => $saleable->{'name_' . Yii::$app->language},
                    'model' => $quantityForm,
                    'is_dynamic_checkout' => $saleable->is_dynamic_checkout,
                    'quantity' => [1, 2, 3, 4, 5, 6, 7, 8],
                    'start_date' => (string)date('d-m-yyyy', strtotime($plan->start_date)),
                    'end_date' => (string)date('d-m-yyyy', strtotime($plan->end_date))
        ]);
    }
    
    public function actionChoose($code, $event, $type){
        $customer = Customer::find()->where(['hash_code' => trim($code)])->one();
        $plan = Plan::find()->where(['link' => $event])->isRunning()->one();
        $saleable = Saleable::find()->where(['link' => $type])->active()->one();

        if (!(isset($customer) && isset($plan) && isset($saleable) && $saleable->plan->hash_code == $plan->hash_code)) {
            throw new NotFoundHttpException('Please check your link');
        }
        
        if(!$saleable->allow_select_price){
            return $this->redirect(['book', 'code' => $code, 'event' => $event, 'type' => $type]);
        }
        
        $quantityForm = Yii::$app->session->get($code . '_form', new QuantityForm());
        
        //$ids = Yii::$app->session->get($code . '_inventory_ids', []);
        //Inventory::updateAll(['status' => Inventory::STATUS_AVAILABLE], ['id' => $ids, 'status' => Inventory::STATUS_PROCESSING, 'customer_id' => $customer->id]);
        Yii::$app->session->set($code . '_inventory_ids', []);
        Yii::$app->session->set($code . '_start_time', null);
        
        //check inventory is available within these time or not
        
        $toDate = null;
        $fromDate = date($quantityForm->checkindate);

        if($saleable->is_dynamic_checkout){
            $toDate = date($quantityForm->checkoutdate);
        } else {
            $toDate = $saleable->getCheckoutDate($fromDate);
        }
        
        
        $date_data = new \frontend\models\ChoosenForm();
        
        if(Yii::$app->request->isPost){
            $date_data->load(Yii::$app->request->post());
            $inv_type = $saleable->getInventoryJungleTable()[0];
            $date_to = date('m/d/Y', strtotime('+' . $inv_type['period'] . 
                    (($inv_type['period'] > 1)?' days':' day'), strtotime($fromDate)));
            
            $to_date = ($saleable->is_dynamic_checkout)?$toDate:$date_to;
            $periods = TimeHelper::getDatesFromRange($fromDate, $to_date, 'm/d/Y');
            $quantity = is_numeric($quantityForm['quantity']) ? $quantityForm['quantity'] + 1 : 0;
            $total_quantity = $quantity * $inv_type['quantity'];
            
            if (count($periods) > 1) {
                array_splice($periods, 0, 1);
            }
            
            $inventory_ids = [];
            
            if(count($date_data->date_datas) == count($periods)){
                foreach ($periods as $date) {
                    $inventories = Inventory::find()->select(['id'])
                        ->where(['inv_type_id' => $inv_type['inventory_type_id'],'stay_date' => strtotime($date),'price' => $date_data->date_datas[strtotime($date)]])
                        ->limit($total_quantity)
                        ->createCommand()->queryAll();
                    if(count($inventories) == $total_quantity){
                        foreach ($inventories as $inventory){
                            $inventory_ids[] = $inventory['id'];
                        }
                    } else {
                        Yii::$app->session->setFlash('message', Yii::t('yii', 'Your selected inventories were booked by others'));
                        return $this->redirect(['book', 'code' => $code, 'event' => $event, 'type' => $type]);
                    }
                }
                Yii::trace($inventory_ids);
                //$updated_count = Inventory::updateAll(['status' => Inventory::STATUS_PROCESSING, 'customer_id' => $customer->id, 'modified_date' => time(), 'modified_by' => $customer->id], ['id' => $inventory_ids, 'status' => Inventory::STATUS_AVAILABLE]);

                //if (count($inventory_ids) > 0 && $updated_count == count($inventory_ids)) {

                    Yii::$app->session->set($code . '_inventory_ids', $inventory_ids);
                    Yii::$app->session->set($code . '_customer', $customer);
                    Yii::$app->session->set($code . '_start_time', time());

                    return $this->redirect(['confirm-order', 'code' => $code, 'event' => $event, 'type' => $type]);
                /**} else {
                    Inventory::updateAll(['status' => Inventory::STATUS_AVAILABLE], ['id' => $inventory_ids, 'status' => Inventory::STATUS_PROCESSING, 'customer_id' => $customer->id]);
                    Yii::trace($inventory_ids);
                    Yii::$app->session->setFlash('message', Yii::t('yii', 'Your selected inventories were booked by others'));
                    return $this->redirect(['book', 'code' => $code, 'event' => $event, 'type' => $type]);
                }**/
            }
        }
        
            $name = [];
        foreach($saleable->getInventoryJungleTable() as $inventory_jungle_table){
            $date_to = date('m/d/Y', strtotime('+' . $inventory_jungle_table['period'] . 
                    (($inventory_jungle_table['period'] > 1)?' days':' day'), strtotime($fromDate)));
            
            $to_date = ($saleable->is_dynamic_checkout)?$toDate:$date_to;
            $periods = TimeHelper::getDatesFromRange($fromDate, $to_date, 'm/d/Y');
            $quantity = is_numeric($quantityForm['quantity']) ? $quantityForm['quantity'] + 1 : 0;
            $total_quantity = $inventory_jungle_table['quantity'] * $quantity;
            
            Yii::trace("Quantity Form: ");Yii::trace(ArrayHelper::toArray($quantityForm));
            Yii::trace("To date: ");Yii::trace($toDate);
            Yii::trace("From date: ");Yii::trace($fromDate);
            Yii::trace("Date to: ");Yii::trace($date_to);
            
            if (count($periods) > 1) {
                array_splice($periods, 0, 1);
            }
            
            $price_group = Inventory::find()->select(['COUNT(*) AS number','price','stay_date'])
                ->where(['inv_type_id' => $inventory_jungle_table['inventory_type_id'],'status' => Inventory::STATUS_AVAILABLE])->andWhere(['between','stay_date',strtotime($fromDate), strtotime($to_date)])
                ->groupBy(['price', 'stay_date'])
                ->createCommand()->queryAll();
            
            $prices = Inventory::find()->distinct()->select(['price'])
                ->where(['inv_type_id' => $inventory_jungle_table['inventory_type_id'],])
                    ->orderBy('price')
                ->createCommand()->queryAll();
            
            $price_name = Inventory::find()->distinct()->select(['price','name_vi', 'name_en'])
                ->where(['inv_type_id' => $inventory_jungle_table['inventory_type_id'],])
                    ->orderBy('price')
                ->createCommand()->queryAll();
            
            Yii::trace(ArrayHelper::toArray($prices));
            
            foreach ($price_name as $name_price){
                $name[$name_price['price']] = $name_price['name_' . Yii::$app->language] ;
            }
            
            $template = [];
            foreach ($prices as $price){
                $template[''.$price['price']] =  0;
            }
             
            Yii::trace($template);
            Yii::trace(ArrayHelper::toArray($price_group));
            Yii::trace(ArrayHelper::map(ArrayHelper::toArray($price_group),'price','number','stay_date'));
            
            $data_inventory = ArrayHelper::map(ArrayHelper::toArray($price_group),'price','number','stay_date');
            
            $data_list = [];
            
            foreach ($periods as $date) {
                
                $each_day = [];
                $inventory_today = 0;
                
                if(!isset($data_inventory[strtotime($date)])){
                    $data_inventory[strtotime($date)] = $template;
                    $inventory_today = $template;
                } else {
                    $inventory_today = $data_inventory[strtotime($date)];
                }
                
                $has_enought = false;
                
                foreach ($template as $price => $quantity_today){
                    $quantity_per_day = isset($inventory_today[$price])?$inventory_today[$price]:$quantity_today;
                    if($quantity_per_day >= $total_quantity){
                        $each_day[$price] = true;
                        $has_enought = true;
                    } else {
                        $each_day[$price] = false;
                    }
                }
                
                if(!$has_enought){
                    Yii::trace("On: ");
                    Yii::trace($date);
                    Yii::$app->session->setFlash('message', Yii::t('yii', 'Out of inventory'));
                    return $this->redirect(['book', 'code' => $code, 'event' => $event, 'type' => $type]);
                }
                
                $data_list[strtotime($date)] = $each_day;
                
                Yii::trace($data_list);
            }
        }
        return $this->render('choose',[
            'customer' => $customer,
            'model' => $date_data,
            'data' => $data_list,
            'template' => $template,
            'code' => $code, 'event' => $event, 'type' => $type
            , 'term' => $saleable->{'note_' . Yii::$app->language},
                    'name' => $name
        ]);
    }

    public function actionConfirmOrder($code, $event, $type) {
        $inventory_ids = Yii::$app->session->get($code . '_inventory_ids', []);
        //$customer = Yii::$app->session->get($code . '_customer',null);
        $quantity_form = Yii::$app->session->get($code . '_form', new QuantityForm());

        $customer = Customer::find()->where(['hash_code' => trim($code)])->one();
        $plan = Plan::find()->where(['link' => $event])->isRunning()->one();
        $saleable = Saleable::find()->where(['link' => $type])->active()->one();

        if (!(isset($customer) && isset($plan) && isset($saleable) && $saleable->plan->hash_code == $plan->hash_code)) {
            throw new NotFoundHttpException('Please check your link');
        }

        $payment_form = new PaymentTypeForm();
        $checkoutdate = (empty($quantity_form['checkoutdate'])) ? $saleable->getCheckoutDate($quantity_form['checkindate']) : $quantity_form['checkoutdate'];

        $price = 0;

        if ($saleable->price > 0) {
            $price = $saleable->price * ($quantity_form['quantity'] + 1);
        } else {
            $price = array_sum(ArrayHelper::getColumn(Inventory::find()->where(['id' => $inventory_ids])->createCommand()->queryAll(), 'price'));
        }
        /**
        if($saleable->is_dynamic_checkout){
            
            $quantityForm = Yii::$app->session->get($code . '_form', new QuantityForm());
            $fromDate = date($quantityForm->checkindate);

            $toDate = date($quantityForm->checkoutdate);

            $periods = TimeHelper::getDatesFromRange($fromDate, $toDate, 'm/d/Y');
            $price = $price * (count($periods) - 1);
        }*/
        //no more fee
        $payment_fee = 0;//2200 + ($price * 0.012);
        $payment_form->payment_type = 1;
        
        $order = new Order();
        $order->quantity = ($quantity_form['quantity'] + 1);
        $order->checkin = strtotime($quantity_form['checkindate'] . " 00:00:00");
        $order->checkout = (empty($checkoutdate)) ? strtotime($quantity_form['checkindate'] . " 00:00:00") : strtotime($checkoutdate . " 00:00:00");

        if (Yii::$app->request->isPost && $payment_form->load(Yii::$app->request->post())) {
            //if saleable does not require inventory, should avoid this update
            if(!($saleable->is_check_inventory)){
                $inventories = Inventory::find()->where([
                    'id' => $inventory_ids,
                    'status' => Inventory::STATUS_AVAILABLE
                ])->createCommand()->queryAll();

                if(count($inventories) < count($inventory_ids)){

                    $not_avail_inv = Inventory::find()->where([
                                'id' => $inventory_ids,
                            ])->andFilterWhere(['not',['status' => Inventory::STATUS_AVAILABLE]])->createCommand()->queryAll();

                    foreach($not_avail_inv as $key => $not_inv){
                        $alt_inv = Inventory::findOne([
                            'stay_date' => $not_inv['stay_date'],
                            'status' => Inventory::STATUS_AVAILABLE,
                            'price' => $not_inv['price']
                        ]);
                        if(isset($alt_inv)){
                            $inventories[] = $alt_inv;
                        } else {
                            Yii::$app->session->setFlash('message', Yii::t('yii', 'Your selected inventories were booked by others'));
                            return $this->redirect(['book', 'code' => $code, 'event' => $event, 'type' => $type]);
                        }
                    }

                }
            }
            $payment_type = ($payment_form->payment_type == 1) ? 1 : 0;

            if ($payment_type == 0) {
                $payment_fee = 0;//2200 + ($price * 0.025);
            }

            $order->customer_id = $customer->id;
            $order->price = $price;
            $order->fee_payment = $payment_fee;
            $order->status = Order::STATUS_PENDING;
            $order->hash_code = Yii::$app->security->generateRandomString(16);
            $order->saleable_id = $saleable->id;
            if ($order->save(false)) {
                $order->updateAttributes(['code' => $plan->code_name . '-' . $saleable->code_name . '-' . $customer->id . $order->id]);
                $detailDatas = [];
                
                //if saleable does not require inventory, should avoid this update
                if(!($saleable->is_check_inventory)){
                    foreach ($inventory_ids as $inventory_id) {
                        $detailDatas[] = [
                            $inventory_id,
                            $order->id,
                            strtotime($quantity_form['checkindate'] . " 00:00:00"),
                            (empty($checkoutdate)) ? strtotime($quantity_form['checkindate'] . " 00:00:00") : strtotime($quantity_form['checkoutdate'] . " 00:00:00")
                        ];
                    }

                    Inventory::updateAll([
                        'status' => Inventory::STATUS_HOLDING,
                        'customer_id' => $customer->id,
                        'sold_date' => time(),
                        'orders_id' => $order->id
                            ], [
                        'id' => $inventory_ids,
                    ]);
                } else {
                    $detailDatas[] = ['DUMMY',$order->id,strtotime($quantity_form['checkindate'] . " 00:00:00"),(empty($checkoutdate)) ? strtotime($quantity_form['checkindate'] . " 00:00:00") : strtotime($quantity_form['checkoutdate'] . " 00:00:00")];
                }
                Yii::$app->db->createCommand()
                        ->batchInsert(OrderDetail::tableName(), [
                            'inventory_id',
                            'orders_id',
                            'date_in',
                            'date_out'
                                ], $detailDatas)
                        ->execute();

                $ip = ClientHelper::getUserIP(false);
                $params = array(
                    'vpc_MerchTxnRef' => date('YmdHis') . "-$customer->id-$order->id",
                    'vpc_Amount' => $order->price,
                    'vpc_TicketNo' => $ip,
                    'vpc_OrderInfo' => $order->code,
                    'vpc_Customer_Name' => $customer->fullname,
                    'vpc_Customer_Phone' => $customer->phone,
                    'vpc_Customer_Email' => $customer->email,
                    'vpc_Customer_Id' => $customer->id,
                    'order_id' => $order->id,
                    'payment_type' => $payment_type
                );

                $cybersource = new Cybersource($params);
                $cybersource->pay($params);

                Yii::$app->session->set($code . '_order', $order);
                Yii::$app->session->set($code . '_payment', $payment_type);

                return $this->redirect(['success', 'code' => $code, 'event' => $event, 'type' => $type]);
            }
        }
        return $this->render('confirm_order', [
                    'customer_name' => $customer->fullname,
                    'email' => $customer->email,
                    'phone' => $customer->phone,
                    'event' => $plan->name,
                    'type' => $saleable->{'name_' . Yii::$app->language},
                    'price' => $price,
                    'model' => $payment_form,
                    'checkindate' => date('d-m-Y', $order->checkin),
                    'checkoutdate' => date('d-m-Y', $order->checkout),
                    'quantity' => $quantity_form['quantity'] + 1,
                    'term' => $saleable->{'term_' . Yii::$app->language},
                    'fee' => $payment_fee
        ]);
    }

    public function actionSuccess($code, $event, $type) {

        $customer = Customer::find()->where(['hash_code' => trim($code)])->one();
        $plan = Plan::find()->where(['link' => $event])->isRunning()->one();
        $saleable = Saleable::find()->where(['link' => $type])->active()->one();

        if (!(isset($customer) && isset($plan) && isset($saleable) && $saleable->plan->hash_code == $plan->hash_code)) {
            throw new NotFoundHttpException('Please check your link');
        }

        $order = Yii::$app->session->get($code . '_order', '');
        $payment_type = Yii::$app->session->get($code . '_payment', '');

        if (empty($order)) {
            Yii::$app->session->removeAll();
            throw new NotFoundHttpException('Your session is expired.');
        }

        $fullname = preg_split('/ +/', $customer->fullname);
        $surname = $fullname[0];
        unset($fullname[0]);
        $forename = implode(' ', $fullname);
        $cybersourceParams = [
            'access_key' => Cybersource::ACCESS_KEY,
            'profile_id' => Cybersource::PROFILE_ID,
            'transaction_uuid' => $this->generateUuidV4(),
            'signed_field_names' => Cybersource::SIGNED_FIELD_NAME,
            'unsigned_field_names' => '',
            'signed_date_time' => gmdate("Y-m-d\TH:i:s\Z"),
            'locale' => Yii::$app->language == 'en' ? 'en-us' : 'vi-vn',
            'transaction_type' => 'sale',
            'reference_number' => $order->code,
            'amount' => $order->price + $order->fee_payment,
            'currency' => 'VND',
            'bill_to_email' => $customer->email,
            'bill_to_surname' => $surname,
            'bill_to_forename' => $forename,
            'bill_to_phone' => $customer->phone,
            'bill_to_address_country' => 'VN',
            'bill_to_address_line1' => 'Tp Hồ Chí Minh',
            'bill_to_address_line2' => 'Tp Hồ Chí Minh',
            'bill_to_address_postal_code' => '700000',
            'bill_to_address_city' => 'Hồ Chí Minh',
            'bill_to_company_name' => 'Empire',
            'bill_to_address_state' => '51'
        ];

        $opsvPayment = Payment::find()->where(['order_id' => $order->id])->one();
        $opsvPayment->updateAttributes([
            'vpc_SecureHash' => Cybersource::generateSignature($cybersourceParams), 'payment_type' => $payment_type
        ]);

        return $this->render('success' . $payment_type, [
                    'order' => $order,
                    'customer_name' => $customer->fullname,
                    'cybersourceParams' => $cybersourceParams,
                    'paymentType' => $payment_type,
                    'code' => $code, 'event' => $event, 'type' => $type]);
    }

    /**
     * Generates a random UUID using the secure RNG.
     *
     * Returns Version 4 UUID format: xxxxxxxx-xxxx-4xxx-Yxxx-xxxxxxxxxxxx where x is
     * any random hex digit and Y is a random choice from 8, 9, a, or b.
     * @return string the UUID
     * @throws \yii\base\InvalidParamException
     * @throws \yii\base\Exception
     */
    public function generateUuidV4() {
        $bytes = Yii::$app->security->generateRandomKey(16);
        $bytes[6] = chr((ord($bytes[6]) & 0x0f) | 0x40);
        $bytes[8] = chr((ord($bytes[8]) & 0x3f) | 0x80);
        $chunks = str_split(bin2hex($bytes), 4);

        return "{$chunks[0]}{$chunks[1]}-{$chunks[2]}-{$chunks[3]}-{$chunks[4]}-{$chunks[5]}{$chunks[6]}{$chunks[7]}";
    }

}
