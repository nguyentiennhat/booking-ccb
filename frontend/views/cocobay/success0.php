<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;
use common\models\payments\Cybersource;

$this->title = Yii::t('yii', 'Hi ') . $customer_name;
?>
<div class="site-contactrow">
    <div class="col-lg-6 col-lg-offset-3 content">
    <form id="payment_confirmation" action="<?= Cybersource::PAYMENT_URL ?>" method="post">
        <input type="hidden" name="access_key" value="<?= Cybersource::ACCESS_KEY ?>">
        <input type="hidden" name="profile_id" value="<?= Cybersource::PROFILE_ID ?>">
        <input type="hidden" name="transaction_uuid" value="<?= $cybersourceParams['transaction_uuid'] ?>">
        <input type="hidden" name="signed_field_names" value="<?= Cybersource::SIGNED_FIELD_NAME ?>">
        <input type="hidden" name="unsigned_field_names">
        <input type="hidden" name="signed_date_time" value="<?= $cybersourceParams['signed_date_time'] ?>">
        <input type="hidden" name="locale" value="<?= Yii::$app->language == 'en' ? 'en-us' : 'vi-vn' ?>">
        <input type="hidden" name="transaction_type" value="<?= $cybersourceParams['transaction_type'] ?>">
        <input type="hidden" name="reference_number" value="<?= $cybersourceParams['reference_number'] ?>">
        <input type="hidden" name="currency" value="VND">
        <input type="hidden" name="bill_to_email" value="<?= $cybersourceParams['bill_to_email'] ?>">
        <input type="hidden" name="bill_to_surname" value="<?= $cybersourceParams['bill_to_surname'] ?>">
        <input type="hidden" name="bill_to_forename" value="<?= $cybersourceParams['bill_to_forename'] ?>">
        <input type="hidden" name="bill_to_phone" value="<?= $cybersourceParams['bill_to_phone'] ?>">
        <input type="hidden" name="bill_to_address_country" value="VN">
        <input type="hidden" name="bill_to_address_line1" value="Tp Hồ Chí Minh">
        <input type="hidden" name="bill_to_address_postal_code" value="700000">
        <input type="hidden" name="bill_to_address_city" value="Hồ Chí Minh">
        <input type="hidden" name="bill_to_company_name" value="Empire">
        <input type="hidden" name="bill_to_address_state" value="51">
        <input type="hidden" name="amount" value="<?=$cybersourceParams['amount']?>">
        <input type="hidden" name="signature" value="<?= Cybersource::generateSignature($cybersourceParams) ?>">
        
        <div class="form-group">
<div><label><?= Html::encode($this->title) ?></label></div>
<div><label><?= Yii::t('yii', 'Please complete the payment within 30 minutes') ?></label></div>
     <div>       <label><?= Yii::t('yii', 'Order: ') ?></label> <?= $order->code ?></div>
          <div>  <label><?= Yii::t('yii', 'Adult: ') ?></label> <?= $adult ?> x <?= number_format($adult_price)?></div>
            <div><label><?= Yii::t('yii', 'Children: ') ?></label> <?= $children ?> x <?= number_format($children_price)?></div>
            <div><label><?= Yii::t('yii', 'Price: ') ?></label> <?= number_format($cybersourceParams['amount'] - $order->fee_payment) . ' VND' ?></div>
        </div>
        <?php if(false) { ?>
        <div class="form-group">
            <label><?= Yii::t('yii', 'Transaction fee: ') ?></label> <?= number_format($order->fee_payment) . ' VND' ?>
        </div>
        <?php } ?>
        <button type="submit" class="btn btn-success"><?= Yii::t('yii', 'Pay') ?></button>
    </form>
</div>
</div>