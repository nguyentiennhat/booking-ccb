<?php

namespace common\models\customer\base;

use Yii;

/**
 * This is the model class for table "customer".
 *
 * @property int $id
 * @property string $fullname
 * @property string $phone
 * @property string $email
 * @property string $OTP
 * @property int $otp_date
 * @property int $created_date
 * @property int $is_verified 0: chưa xác nhận, 1: đã xác nhận
 * @property int $verified_date
 * @property string $hash_code
 */
class CustomerBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fullname', 'phone', 'email', 'hash_code'], 'required'],
            [['otp_date', 'created_date', 'is_verified', 'verified_date'], 'integer'],
            [['fullname', 'email'], 'string', 'max' => 300],
            [['phone'], 'string', 'max' => 20],
            [['OTP'], 'string', 'max' => 6],
            [['hash_code'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fullname' => 'Fullname',
            'phone' => 'Phone',
            'email' => 'Email',
            'OTP' => 'Otp',
            'otp_date' => 'Otp Date',
            'created_date' => 'Created Date',
            'is_verified' => 'Is Verified',
            'verified_date' => 'Verified Date',
            'hash_code' => 'Hash Code',
        ];
    }
}
