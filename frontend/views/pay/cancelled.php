<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = Yii::t('yii','Hi ') . $customer_name;
?>
<div class="site-about row">
    <div class="col-lg-6 col-lg-offset-3 content">
    <h1><?=Yii::t('yii','Hi ')?><?= Html::encode($customer_name) ?></h1>
    <p><?=Yii::t('yii', 'Your order <b>{order}</b> has been cancelled. Thank you.',[
        'order' => $order_code
    ])?></p>
</div>
</div>
