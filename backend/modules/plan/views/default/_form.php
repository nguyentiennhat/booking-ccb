<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use dosamigos\selectize\SelectizeDropDownList;

/* @var $this yii\web\View */
/* @var $model common\models\plan\Plan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="plan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'note')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'start_date')->widget(DatePicker::className(),[
      'pluginOptions' => [
         'todayHighlight' => true
      ]
    ]) ?>
    <?= $form->field($model, 'end_date')->widget(DatePicker::className(),[
      'pluginOptions' => [
         'todayHighlight' => true
      ]
    ]) ?>

    <?= $form->field($model, 'transaction_code')->textInput() ?>
    <?= $form->field($model, 'code_name')->textInput() ?>
    <?= $form->field($model, 'status')->widget(SelectizeDropDownList::className(),[
        'items' => ['0' => 'Inactive', '1' => 'Active'],
        'value' => 1
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('yii', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
