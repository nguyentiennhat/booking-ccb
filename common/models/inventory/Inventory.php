<?php

namespace common\models\inventory;

use Yii;
use common\models\inventory\base\InventoryBase;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class Inventory extends InventoryBase
{
    
    const STATUS_DELETED = -1;
    const STATUS_AVAILABLE = 0;
    const STATUS_HOLDING = 1;
    const STATUS_SOLD = 2;
    const STATUS_PROCESSING = 3;

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_date',
                'updatedAtAttribute' => 'modified_date',
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('yii', 'ID'),
            'inv_type_id' => Yii::t('yii', 'Inventory Type'),
            'price' => Yii::t('yii', 'Price'),
            'created_date' => Yii::t('yii', 'Created Date'),
            'modified_date' => Yii::t('yii', 'Modified Date'),
            'created_by' => Yii::t('yii', 'Created By'),
            'modified_by' => Yii::t('yii', 'Modified By'),
            'sold_date' => Yii::t('yii', 'Sold Date'),
            'customer_id' => Yii::t('yii', 'Customer ID'),
            'orders_id' => Yii::t('yii', 'Orders ID'),
            'status' => Yii::t('yii', 'Status'),
        ];
    }

    public static function getInventoryTypeColumn(){
        return 'inv_type_id';
    }
    /**
     * @inheritdoc
     * @return InventoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new InventoryQuery(get_called_class());
    }
}
