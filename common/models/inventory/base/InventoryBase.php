<?php

namespace common\models\inventory\base;

use Yii;

/**
 * This is the model class for table "inventory".
 *
 * @property int $id
 * @property int $inv_type_id 1: room, 2: ticket
 * @property int $price
 * @property int $stay_date
 * @property int $created_date
 * @property int $modified_date
 * @property int $created_by
 * @property int $modified_by
 * @property int $sold_date
 * @property int $customer_id
 * @property int $orders_id
 * @property int $status -1: deleted; 0: in stock; 1: sold; 2: Reserve; 3: Pending 
 * @property int $transaction_code
 * @property string $name_vi
 * @property string $name_en
 */
class InventoryBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inventory';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inv_type_id', 'price', 'stay_date', 'created_date', 'modified_date', 'created_by', 'modified_by', 'sold_date', 'customer_id', 'orders_id', 'status', 'transaction_code'], 'integer'],
            [['name_vi','name_en'], 'string'],
            [['stay_date'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'inv_type_id' => 'Inv Type ID',
            'price' => 'Price',
            'stay_date' => 'Stay Date',
            'created_date' => 'Created Date',
            'modified_date' => 'Modified Date',
            'created_by' => 'Created By',
            'modified_by' => 'Modified By',
            'sold_date' => 'Sold Date',
            'customer_id' => 'Customer ID',
            'orders_id' => 'Orders ID',
            'status' => 'Status',
            'transaction_code' => 'Transaction Code',
        ];
    }
}
