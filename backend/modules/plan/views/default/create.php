<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\plan\Plan */

$this->title = Yii::t('yii', 'Create Plan');
$this->params['breadcrumbs'][] = ['label' => Yii::t('yii', 'Plans'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="plan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
