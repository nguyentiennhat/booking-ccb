<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\inventory\type\InventoryType */

$this->title = Yii::t('yii', 'Create Inventory Type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('yii', 'Inventory Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inventory-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
