<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\web\View;

$this->title = Yii::t('yii', 'Your Order');
$this->registerJsFile(
        '@web/numeral/numeral.min.js', [View::POS_END, 'depends' => [\yii\web\JqueryAsset::className()]]);
/**
$this->registerJs("$('input[name=\"PaymentTypeForm[payment_type]\"]').click(function() {

            let amount = 0, charge = 0;

            if ($(this).val() == 1) {
                charge = 2200 + (" . $price . " * 0.012);
                amount = " . $price . " + charge;
            } else {
                charge = 2200 + (" . $price . " * 0.025);
                amount = " . $price . " + charge;
            }

            $('#fee').text(numeral(charge).format('0,0'));

        });");*/
?>

<div class="order-view row">
    <div class="col-lg-6 col-lg-offset-3 content">
        <h2><?= Html::encode($this->title) ?></h2>
        <h4 class="text-white">
            <?= Yii::t('yii', 'Customer Information') ?>
        </h4>
        <div class="col-md-12">

            <div class="row">
                <label><?= Yii::t('yii', 'Full Name: ') ?></label> <?= $customer_name ?>
            </div>
            <div class="row">
                <label><?= Yii::t('yii', 'Email: ') ?></label> <?= $email ?>
            </div>
            <div class="row">
                <label><?= Yii::t('yii', 'Phone: ') ?></label> <?= $phone ?>
            </div>
        </div>
        <h4 class="text-white">
            <?= Yii::t('yii', 'Booking Details') ?>
        </h4>
        <div class="col-md-12">
            <div class="row">
                <label><?= Yii::t('yii', 'Quantity: ') ?></label> <?= number_format($quantity) ?>
            </div>
            <div class="row">
                <label><?= Yii::t('yii', 'Total Price: ') ?></label> <span id="price" data-value="<?= $price ?>"><?= number_format($price) ?></span> VND
            </div>
            <?php if(false) { ?>
            <div class="row">
                <label><?= Yii::t('yii', 'Transaction fee: ') ?></label> <span id="fee" data-value="<?= $fee ?>"><?= number_format($fee) ?></span> VND
            </div>
            <?php } ?>
            <div class="row">
                <label><?= Yii::t('yii', 'Check-in Date: ') ?></label> <?= $checkindate ?>
            </div>
            <?php
            if (!empty($checkoutdate)) {
                ?>

                <div class="row">
                    <label><?= Yii::t('yii', 'Check-out Date: ') ?></label> <?= $checkoutdate ?>
                </div>
                <?php
            }
            ?>

            <?php $form = ActiveForm::begin(['options' => ['class' => 'row'],]); ?>
            <?=
            $form->field($model, 'payment_type')->radioList([
                0 => Yii::t('yii', 'International payments (Visa, Master, JCB Card)'),
                1 => Yii::t('yii', 'Domestic payment (ATM card with Online Banking)')
            ])->label(Yii::t('yii', 'Payment Type'))
            ?>
            <div class="text-white form-group">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" value="" id="cb_accept_term" required="true">
                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                        <?= Yii::t('yii', 'I have read and accept') ?> <?php
                        Modal::begin([
                            'header' => '<h2>' . Yii::t('yii', 'Condition & Promotion clause') . '</h2>',
                            'id' => 'termcondition',
                            'toggleButton' => [
                                'tag' => 'a',
                                'label' => Yii::t('yii', 'Condition & Promotion clause'),
                                'href' => 'javascript:void(0)',
                                'data-target' => '#termcondition'
                            ]
                        ]);

                        echo $term;

                        Modal::end();
                        ?>
                    </label>
                </div>
            </div>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('yii', 'Confirm'), ['class' => 'btn btn-success']) ?>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>