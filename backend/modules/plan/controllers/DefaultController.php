<?php

namespace backend\modules\plan\controllers;

use Yii;
use common\models\plan\Plan;
use common\models\plan\PlanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DefaultController implements the CRUD actions for Plan model.
 */
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Plan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PlanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Plan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Plan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Plan();
        if(Yii::$app->request->isPost){
            $model->hash_code = Yii::$app->getSecurity()->generateRandomString(24);
            $model->created_by = Yii::$app->user->identity->id;
            $model->modified_by = Yii::$app->user->identity->id;
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $model->start_date = date('Y-m-d H:i:s', strtotime(Yii::$app->request->post('Plan')['start_date']));
                $model->end_date = date('Y-m-d H:i:s', strtotime(Yii::$app->request->post('Plan')['end_date']));
                $model->save();
                return $this->redirect(['view', 'id' => $model->id]);
            }
            Yii::trace($model->getErrors());
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Plan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if(Yii::$app->request->isPost){
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $model->start_date = date('Y-m-d H:i:s', strtotime(Yii::$app->request->post('Plan')['start_date']));
                $model->end_date = date('Y-m-d H:i:s', strtotime(Yii::$app->request->post('Plan')['end_date']));
                Yii::trace(\yii\helpers\ArrayHelper::toArray($model));
                $model->save();
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Plan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionSchedule($id){
        $plan = $this->findModel($id);
        $status = ($plan->status == Plan::RUNNING_STATUS)?Plan::ACTIVE_STATUS: Plan::RUNNING_STATUS;
        $plan->updateAttributes(['status' => $status]);
        return $this->render('view', [
            'model' => $plan,
        ]);
    }
    
    /**
     * Finds the Plan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Plan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Plan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('yii', 'The requested page does not exist.'));
    }
}
