<?php
/**
 * Created by PhpStorm.
 * User: Team
 * Date: 3/27/2017
 * Time: 1:39 PM
 */

namespace common\models\payments;

use common\models\payment\Payment;

class Cybersource
{
//    const SECRET_KEY = 'ebe87a9012eb45b8bfd33391f3d2c0f03e64ed63683248f080bedf199dbde86fa9763e77b4314de8804df54f9d7ca694b53e0f7e32ea46ff9c0b352be2dde7794c6dfc36492f4562a5ff5da6053be5aff65a0b04974a441387af1de48e227a630a2a6e921fa44b4d8e495741055b62c9b27e669ad63b4461bbe3f56ce68685b5';
//    const PROFILE_ID = '2E96A2E7-ADC8-4E1C-BFC0-7AB66A393A2E';
//    const ACCESS_KEY = '98a70cba71603401862bcc7c6862e7f3';
//    const PAYMENT_URL = 'https://testsecureacceptance.cybersource.com/pay';
    const SIGNED_FIELD_NAME = 'access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency,bill_to_email,bill_to_phone,bill_to_address_country,bill_to_address_postal_code,bill_to_address_line1,bill_to_address_city,bill_to_surname,bill_to_forename';

    const SECRET_KEY = 'd3f8f17c85e74e4b8f614b3e17de7bccfda80612fe284c5e8a58a378c2b6e3c3e3f7c54c67934703b931f7612f0ab3a5bf4bd6491b09423abea51766c149df1ab5efda6c706943bb82a3100d3010959676b9ba92554241cd926b472e8e194e7559a71bf0edfd4781b91cae37bbfea7f1c2c42acd5456408ca33855733e059043';
    const PROFILE_ID = '71EAF160-1352-48A3-86F4-A43035C807C4';
    const ACCESS_KEY = '237859993e1b38da84fcb577427f7276';
    const PAYMENT_URL = 'https://secureacceptance.cybersource.com/pay';
//    const SIGNED_FIELD_NAME = 'access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency,bill_to_email,bill_to_phone,bill_to_address_country,bill_to_address_postal_code,bill_to_address_line1,bill_to_address_city,bill_to_surname,bill_to_forename,bill_to_company_name,bill_to_address_state';

    public $params;

    public function __construct($params)
    {
        $this->params = $params;
    }

    public static function generateSignature($cybersourceParams)
    {
        $signedFieldNames = explode(',', self::SIGNED_FIELD_NAME);
        $dataToSign       = [];
        foreach ($signedFieldNames as $field) {
            $dataToSign[] = $field . '=' . $cybersourceParams[$field];
        }

        $dataToSign = implode(",", $dataToSign);

        return base64_encode(hash_hmac('sha256', $dataToSign, self::SECRET_KEY, true));
    }

    public function pay($params)
    {
        return $this->insertOpsv($params);
    }

    private function insertOpsv($params)
    {
        $vpc_MerchTxnRef    = $params['vpc_MerchTxnRef'];
        $vpc_Amount         = $params['vpc_Amount'];
        $vpc_TicketNo       = $params['vpc_TicketNo'];
        $vpc_OrderInfo      = $params['vpc_OrderInfo'];
        $vpc_Customer_Phone = $params['vpc_Customer_Phone'];
        $vpc_Customer_Name  = $params['vpc_Customer_Name'];
        $vpc_Customer_Email = $params['vpc_Customer_Email'];
        $vpc_Customer_Id    = $params['vpc_Customer_Id'];
        $payment_type    = $params['payment_type'];

        $opsvPayment                     = new Payment();
        $opsvPayment->vpc_MerchTxnRef    = $vpc_MerchTxnRef;
        $opsvPayment->vpc_Amount         = $vpc_Amount;
        $opsvPayment->vpc_TicketNo       = $vpc_TicketNo;
        $opsvPayment->vpc_OrderInfo      = $vpc_OrderInfo;
        $opsvPayment->vpc_Customer_Phone = $vpc_Customer_Phone;
        $opsvPayment->vpc_Customer_Name  = $vpc_Customer_Name;
        $opsvPayment->vpc_Customer_Email = $vpc_Customer_Email;
        $opsvPayment->vpc_Customer_Id    = $vpc_Customer_Id;
        $opsvPayment->payment_type    = $payment_type;
//        $opsvPayment->vpc_SecureHash     = $this->generateSignature();
        $opsvPayment->order_id    = $params['order_id'];
        $opsvPayment->do_datetime = date('Y-m-d H:i:s');

        return $opsvPayment->save(false);
    }
}