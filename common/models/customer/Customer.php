<?php

namespace common\models\customer;

use Yii;
use common\models\customer\base\CustomerBase;
use yii\helpers\ArrayHelper;
use yii\behaviors\TimestampBehavior;

class Customer extends CustomerBase
{
    public $reCaptcha;
    public function rules() {
        $rules = [
          [['email'],'email'],
          //[['reCaptcha'], \himiklab\yii2\recaptcha\ReCaptchaValidator::className()]
        ];
        return ArrayHelper::merge(parent::rules(), $rules);
    }
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_date',
                'updatedAtAttribute' => null,
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('yii', 'ID'),
            'fullname' => Yii::t('yii', 'Fullname'),
            'phone' => Yii::t('yii', 'Phone'),
            'email' => Yii::t('yii', 'Email'),
            'OTP' => Yii::t('yii', 'Otp'),
            'otp_date' => Yii::t('yii', 'Otp Date'),
            'created_date' => Yii::t('yii', 'Created Date'),
            'is_verified' => Yii::t('yii', '0: chưa xác nhận, 1: đã xác nhận'),
            'verified_date' => Yii::t('yii', 'Verified Date'),
        ];
    }

    /**
     * @inheritdoc
     * @return CustomerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CustomerQuery(get_called_class());
    }
}
