<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\Cookie;
use common\models\customer\Customer;
use yii\web\NotFoundHttpException;
use common\models\plan\Plan;
use common\models\saleable\Saleable;
use yii\helpers\ArrayHelper;
use common\models\inventory\type\InventoryType;
use common\models\order\Order;
use common\models\order\detail\OrderDetail;
use backend\utilities\helpers\ClientHelper;
use common\models\payments\Cybersource;
use common\models\payment\Payment;
use Swift_TransportException;
use backend\utilities\helpers\TimeHelper;

class CocobayController extends Controller
{
    public function init() {
        parent::init();
        $cookies = Yii::$app->request->cookies;
        $language = $cookies->getValue('language', 'vi');
        Yii::$app->language = $language;
    }
	
    public function actionChangeLang() {
        $lang = Yii::$app->request->post('lang', ''); //en
        // Yii::$app->language = $lang;
        $cookie = new Cookie([
            'name' => 'language',
            'value' => $lang
        ]);
        \Yii::$app->getResponse()->getCookies()->add($cookie);
    }

    public function actionConfirm()
    {
        return $this->render('confirm');
    }

    public function actionIndex()
    {
        $event_link = Yii::$app->request->get('event', '');
        $saleable_link = Yii::$app->request->get('type', '');
        $plan = Plan::find()->where(['link' => $event_link])->isRunning()->one();
        $saleable = Saleable::find()->where(['link' => $saleable_link])->active()->one();
		
        if (isset($plan) && isset($saleable) && isset($saleable->plan) && $saleable->plan->hash_code == $plan->hash_code) {
			
			if(!empty($saleable->layout)){
				$this->layout = $saleable->layout;
				Yii::trace($this->layout);
			}
			
			$model = new \frontend\models\Booking();
			$types = [];
			$cookies = Yii::$app->request->cookies;
			$language = $cookies->getValue('language', 'vi');
			$term = $saleable->{"term_" . $language};
			$payment_list = [
                0 => Yii::t('yii', 'International payments (Visa, Master, JCB Card)'),
                1 => Yii::t('yii', 'Domestic payment (ATM card with Online Banking)')
			];
			
			if($saleable->is_selectable_inventory){
				$types_to_select = $saleable->getInventoryJungleTable();
				
				$inventory_type_ids = ArrayHelper::getColumn($types_to_select, 'inventory_type_id');
				
				$types = ArrayHelper::map(InventoryType::find()->where(['id' => $inventory_type_ids])->createCommand()->queryAll(),'id','name');
				
				Yii::trace($types);
			}
			
			if ($model->load(Yii::$app->request->post()) && $model->validate()) {
				
				//process customer
				
				$customer = new Customer();
				$old_customer = Customer::find()->where(['email' => trim($model->email)])->one();

				if (isset($old_customer)) {
					$customer = $old_customer;
				} else {
					$customer->email = trim($model->email);
					$customer->hash_code = Yii::$app->getSecurity()->generateRandomString(24);
					$customer->otp_date = time();
					$customer->is_verified = 0;
					$customer->verified_date = null;
				}
				$customer->fullname = trim($model->name);
				$customer->phone = trim($model->phone);
				
				if(!$customer->save()){
					return $this->render('_form', [
							'model' => $model
					]);
				}
				
				//calculate price
				//is selectable inventory type
				
				$price = 0;
				
				if($saleable->is_selectable_inventory){
					$unit_price = 0;
					$selected_type = InventoryType::findOne($model->type);
					$quantity_per = Yii::$app->db->createCommand(
					'select quantity from saleable_inventory_type  WHERE `saleable_id`=' . $saleable->id . ' and `inventory_type_id`=' . $selected_type->id)->queryOne()['quantity'];
					
					$adult_price = $child_price = $selected_type->price;
					
					if ($selected_type->local_price >= 0 && $model->local){
						$adult_price = $selected_type->local_price;
					}
					
					if ($selected_type->child_price >= 0){
						$child_price  = $selected_type->child_price;
					}
					
					Yii::trace($adult_price . "--" . $child_price );
					
					$price = ($adult_price * $model->adult + $child_price * $model->children) * $quantity_per;
				} else {
					//calculate price for all inventory types based on total price or inventory type prices
				}
				//create order
				$order = new Order();
				
				$order->quantity = $model->adult;
				$order->customer_id = $customer->id;
				$order->price = $price;
				$order->fee_payment = 0;
				$order->status = Order::STATUS_PENDING;
				$order->hash_code = Yii::$app->security->generateRandomString(16);
				$order->saleable_id = $saleable->id;
				
				if(!$order->save(false)){
					Yii::trace($order->getErrors());
					return $this->render('_form', [
							'model' => $model
					]);
				}
				
                $order->updateAttributes(['code' => $plan->code_name . '-' . $saleable->code_name . '-' . $customer->id . $order->id]);
                Yii::$app->session->set($customer->hash_code . '_order', $order);
				
				$detailDatas = [];
				$detailDatas[] = ['DUMMY',$order->id];
				
                Yii::$app->db->createCommand()
                        ->batchInsert(OrderDetail::tableName(), [
                            'inventory_id',
                            'orders_id',
                                ], $detailDatas)
                        ->execute();
				
                $ip = ClientHelper::getUserIP(false);
                $params = array(
                    'vpc_MerchTxnRef' => date('YmdHis') . "-$customer->id-$order->id",
                    'vpc_Amount' => $order->price,
                    'vpc_TicketNo' => $ip,
                    'vpc_OrderInfo' => $order->code,
                    'vpc_Customer_Name' => $customer->fullname,
                    'vpc_Customer_Phone' => $customer->phone,
                    'vpc_Customer_Email' => $customer->email,
                    'vpc_Customer_Id' => $customer->id,
                    'order_id' => $order->id,
                    'payment_type' => $model->payment_type
                );

                $cybersource = new Cybersource($params);
                $cybersource->pay($params);
				
				
				$fullname = preg_split('/ +/', $customer->fullname);
				$surname = $fullname[0];
				unset($fullname[0]);
				$forename = implode(' ', $fullname);
				$cybersourceParams = [
					'access_key' => Cybersource::ACCESS_KEY,
					'profile_id' => Cybersource::PROFILE_ID,
					'transaction_uuid' => $this->generateUuidV4(),
					'signed_field_names' => Cybersource::SIGNED_FIELD_NAME,
					'unsigned_field_names' => '',
					'signed_date_time' => gmdate("Y-m-d\TH:i:s\Z"),
					'locale' => Yii::$app->language == 'en' ? 'en-us' : 'vi-vn',
					'transaction_type' => 'sale',
					'reference_number' => $order->code,
					'amount' => $order->price + $order->fee_payment,
					'currency' => 'VND',
					'bill_to_email' => $customer->email,
					'bill_to_surname' => $surname,
					'bill_to_forename' => $forename,
					'bill_to_phone' => $customer->phone,
					'bill_to_address_country' => 'VN',
					'bill_to_address_line1' => 'Tp Hồ Chí Minh',
					'bill_to_address_line2' => 'Tp Hồ Chí Minh',
					'bill_to_address_postal_code' => '700000',
					'bill_to_address_city' => 'Hồ Chí Minh',
					'bill_to_company_name' => 'Empire',
					'bill_to_address_state' => '51'
				];

				$opsvPayment = Payment::find()->where(['order_id' => $order->id])->one();
				$opsvPayment->updateAttributes([
					'vpc_SecureHash' => Cybersource::generateSignature($cybersourceParams), 'payment_type' => $model->payment_type
				]);

				return $this->render('success' . $model->payment_type, [
							'order' => $order,
							'customer_name' => $customer->fullname,
'adult'=>$model->adult,
'children' =>$model->children,
'adult_price' => $adult_price,
'children_price' =>  $child_price,
							'cybersourceParams' => $cybersourceParams,
							'paymentType' => $model->payment_type,
							'code' => $customer->hash_code, 'event' => $event_link, 'type' => $saleable_link]);
			}
			
			return $this->render('_form', [
					'model' => $model,
					'types' => $types,
					'payment_list' => $payment_list,
					'term' => $term
			]);
		}
        throw new NotFoundHttpException('Please check your link');
    }

    public function actionSuccess()
    {
        return $this->render('success');
    }

    /**
     * Generates a random UUID using the secure RNG.
     *
     * Returns Version 4 UUID format: xxxxxxxx-xxxx-4xxx-Yxxx-xxxxxxxxxxxx where x is
     * any random hex digit and Y is a random choice from 8, 9, a, or b.
     * @return string the UUID
     * @throws \yii\base\InvalidParamException
     * @throws \yii\base\Exception
     */
    public function generateUuidV4() {
        $bytes = Yii::$app->security->generateRandomKey(16);
        $bytes[6] = chr((ord($bytes[6]) & 0x0f) | 0x40);
        $bytes[8] = chr((ord($bytes[8]) & 0x3f) | 0x80);
        $chunks = str_split(bin2hex($bytes), 4);

        return "{$chunks[0]}{$chunks[1]}-{$chunks[2]}-{$chunks[3]}-{$chunks[4]}-{$chunks[5]}{$chunks[6]}{$chunks[7]}";
    }

}
