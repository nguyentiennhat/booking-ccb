<?php

namespace common\models\payment;

use Yii;
use common\models\payment\base\PaymentBase;

class Payment extends PaymentBase
{

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('yii', 'ID'),
            'order_id' => Yii::t('yii', 'Order ID'),
            'type' => Yii::t('yii', '0: ngan luong - 1: noidia - 2: quocte'),
            'do_datetime' => Yii::t('yii', 'Do Datetime'),
            'timecode' => Yii::t('yii', 'Timecode'),
            'vpc_MerchTxnRef' => Yii::t('yii', 'Vpc  Merch Txn Ref'),
            'vpc_Amount' => Yii::t('yii', 'Vpc  Amount'),
            'vpc_TicketNo' => Yii::t('yii', 'Vpc  Ticket No'),
            'vpc_OrderInfo' => Yii::t('yii', 'Vpc  Order Info'),
            'vpc_Customer_Phone' => Yii::t('yii', 'Vpc  Customer  Phone'),
            'vpc_Customer_Email' => Yii::t('yii', 'Vpc  Customer  Email'),
            'vpc_Customer_Id' => Yii::t('yii', 'Vpc  Customer  ID'),
            'vpc_Customer_Name' => Yii::t('yii', 'Vpc  Customer  Name'),
            'dr_datetime' => Yii::t('yii', 'Dr Datetime'),
            'vpc_TxnResponseCode' => Yii::t('yii', '100: Success - 1: failed'),
            'vpc_TransactionNo' => Yii::t('yii', 'Vpc  Transaction No'),
            'vpc_Message' => Yii::t('yii', 'Vpc  Message'),
            'vpc_SecureHash' => Yii::t('yii', 'Vpc  Secure Hash'),
            'token_nl' => Yii::t('yii', 'Token Nl'),
            'payment_type' => Yii::t('yii', 'Payment Type'),
            'note' => Yii::t('yii', 'Note'),
        ];
    }

    /**
     * @inheritdoc
     * @return PaymentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PaymentQuery(get_called_class());
    }
}
